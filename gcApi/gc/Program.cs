using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace gc
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
            int processorCounter = Environment.ProcessorCount;
            bool success = ThreadPool.SetMaxThreads(processorCounter, processorCounter);
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
                    Host.CreateDefaultBuilder(args)
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.ConfigureKestrel(serverOptions => serverOptions.AddServerHeader = false)
                    .UseStartup<Startup>();
            }).ConfigureLogging(builder =>
            {
                builder.SetMinimumLevel(LogLevel.Error);
                builder.AddLog4Net("log4net.config");
            });

    }
}

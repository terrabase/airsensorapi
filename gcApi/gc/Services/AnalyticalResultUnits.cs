﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using gc.DataAccess.DataObjects;

namespace gc.Services
{
    public class MeasurementResultUnits
    {
        private static String lovClass = "ANALYTICAL_RESULT_UNITS";

        public static async Task<List<gc.Models.LovMemShadowTypes.MeasurementResultUnits>> Get(MySqlContext context, Int32 manufacturerId)
        {
            List<LovMem> lovParams = await gc.Services.Aurora.LovMemGet(context, manufacturerId, lovClass);
            List<gc.Models.LovMemShadowTypes.MeasurementResultUnits> list = new List<gc.Models.LovMemShadowTypes.MeasurementResultUnits>();

            foreach (LovMem lov in lovParams)
            {
                gc.Models.LovMemShadowTypes.MeasurementResultUnits item = new gc.Models.LovMemShadowTypes.MeasurementResultUnits();
                item.Unit = lov.Value;
                item.SystemDefined = lov.SystemParam;
                list.Add(item);
            }
            return list;
        }
        public static async Task<gc.Models.LovMemShadowTypes.MeasurementResultUnits> Get(MySqlContext context, Int32 manufacturerId, String id)
        {
            LovMem lov = await gc.Services.Aurora.LovMemGet(context,manufacturerId, lovClass, id);
            gc.Models.LovMemShadowTypes.MeasurementResultUnits item = new gc.Models.LovMemShadowTypes.MeasurementResultUnits();
            if (lov != null)
            {
                item.Unit = lov.Value;
                item.SystemDefined = lov.SystemParam;
                return item;
            }
            else
                return null;
        }
        public static async Task Put(MySqlContext context,Int32 manufacturerId, String id, gc.Models.LovMemShadowTypes.MeasurementResultUnits item)
        {
            await gc.Services.Aurora.LovMemPut(context,manufacturerId, id, lovClass, item.Unit);
        }
        public static async Task Post(MySqlContext context,Int32 manufacturerId, String username, gc.Models.LovMemShadowTypes.MeasurementResultUnits item)
        {
            await gc.Services.Aurora.LovMemPost(context,manufacturerId, username, lovClass, item.Unit);
        }
        public static async Task Delete(MySqlContext context,Int32 manufacturerId, String id)
        {
            await gc.Services.Aurora.LovMemDel(context,manufacturerId, lovClass, id);
        }
    }
}

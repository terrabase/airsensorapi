﻿//using MySql.Data.MySqlClient;
//using MySqlConnector;
using Pomelo.EntityFrameworkCore.MySql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using gc.DataAccess.DataObjects;

namespace gc.Services
{
    public class MeasurementParams
    {
        private static String lovClass= "ANALYTICAL_PARAMS";

        public static async Task<List<gc.Models.LovMemShadowTypes.MeasurementParameters>> Get(MySqlContext context, Int32 manufacturerId)
        {
            List<LovMem> lovParams = await gc.Services.Aurora.LovMemGet(context, manufacturerId, lovClass);
            List<gc.Models.LovMemShadowTypes.MeasurementParameters> list = new List<gc.Models.LovMemShadowTypes.MeasurementParameters>();

            foreach (LovMem lov in lovParams)
            {
                gc.Models.LovMemShadowTypes.MeasurementParameters item = new gc.Models.LovMemShadowTypes.MeasurementParameters();
                item.ParamId = lov.Value;
                item.Description = lov.Description;
                item.SystemDefined = lov.SystemParam;
                list.Add(item);
            }
            return list;
        }
        public static async Task<gc.Models.LovMemShadowTypes.MeasurementParameters> Get(MySqlContext context,Int32 manufacturerId,String id)
        {
            LovMem lov = await gc.Services.Aurora.LovMemGet(context,manufacturerId, lovClass,id);
            gc.Models.LovMemShadowTypes.MeasurementParameters item = new gc.Models.LovMemShadowTypes.MeasurementParameters();
            if (lov != null)
            {
                item.ParamId = lov.Value;
                item.Description = lov.Description;
                item.SystemDefined = lov.SystemParam;
                return item;
            }
            else
                return null;
        }

        public static async Task<gc.Models.LovMemShadowTypes.MeasurementParameters> GetDesc(MySqlContext context, Int32 manufacturerId, String description)
        {
            LovMem lov = await gc.Services.Aurora.LovMemGetDesc(context,manufacturerId, lovClass, description);
            gc.Models.LovMemShadowTypes.MeasurementParameters item = new gc.Models.LovMemShadowTypes.MeasurementParameters();
            if (lov != null)
            {
                item.ParamId = lov.Value;
                item.Description = lov.Description;
                item.SystemDefined = lov.SystemParam;
                return item;
            }
            else
                return null;
        }
        public static async Task Put(MySqlContext context, Int32 manufacturerId, String id, gc.Models.LovMemShadowTypes.MeasurementParameters item)
        {
            await gc.Services.Aurora.LovMemPut(context,manufacturerId, lovClass, id, item.ParamId, item.Description);
        }
        public static async Task Post(MySqlContext context,Int32 manufacturerId, String username, gc.Models.LovMemShadowTypes.MeasurementParameters item)
        {
            await gc.Services.Aurora.LovMemPost(context,manufacturerId, username,lovClass, item.ParamId, item.Description);
        }
        public static async Task Delete(MySqlContext context,Int32 manufacturerId,String id)
        {
            await gc.Services.Aurora.LovMemDel(context,manufacturerId, lovClass, id);
        }
    }
}

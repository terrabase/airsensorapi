﻿using Pomelo.EntityFrameworkCore.MySql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Dynamic;
using System.Data;
//using FastMember;
using System.IO;
using gc.Extensions;
using Microsoft.Extensions.Caching.Distributed;
using System.Reflection;
using Newtonsoft.Json;
using System.Collections;
using gc.DataAccess.DataObjects;
using Microsoft.EntityFrameworkCore;
using System.Configuration;
using MySqlConnector;
using Microsoft.Extensions.Configuration;
using Kendo.Mvc.UI;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using StackExchange.Redis;
using ServiceStack;

namespace gc.Services
{
    public static class Aurora
    {
        const String tmp_gc = "tmp_gc";
        const String tmp_gc_sys = "tmp_gc_sys";
        const String tmp_latlng = "tmp_latlng";
        const String tmp_wind = "tmp_wind";
        /*
         public static async IAsyncEnumerable<dynamic> QueryGenericData(MySqlConnection conn, String sql)
         {
             using (var cmd = new MySqlCommand())
             {
                 cmd.Connection = conn;
                 cmd.CommandText = sql;

                 using (MySqlDataReader reader = (MySqlDataReader)await cmd.ExecuteReaderAsync())
                 {
                     while (await reader.ReadAsync())
                     {

                         yield return SqlDataReaderToExpando(reader);
                     }
                 }
             }            
         }


         public static async IAsyncEnumerable<dynamic> QueryGenericDataFresh(MySqlConnection conn, String sql)
         {
             using (var cmd = new MySqlCommand())
             {
                 cmd.Connection = conn;
                 cmd.CommandText = sql;

                 using (MySqlDataReader reader = (MySqlDataReader)await cmd.ExecuteReaderAsync())
                 {
                     while (await reader.ReadAsync())
                     {
                         yield return SqlDataReaderToExpando(reader);
                     }
                 }
             }
         }

         private static dynamic SqlDataReaderToExpando(MySqlDataReader reader)
         {
             var expandoObject = new ExpandoObject() as IDictionary<string, object>;

             for (var i = 0; i < reader.FieldCount; i++)
                 expandoObject.Add(reader.GetName(i), reader[i]);

             return expandoObject;
         }



        private static T SqlDataReaderToExpando<T>(MySqlDataReader reader)
        {
            var expandoObject = new ExpandoObject() as IDictionary<string, object>;

            for (var i = 0; i < reader.FieldCount; i++)
                expandoObject.Add(reader.GetName(i), reader[i]);

            return (T)expandoObject;
        }
        */
        private static async Task FastSave<T>(List<T> list, String tableName)
        {
            var path = Path.GetTempPath();
            var fileName = tableName+"_auto_" + Guid.NewGuid().ToString() + ".txt";
            string csv = ServiceStack.Text.CsvSerializer.SerializeToCsv<T>(list);

            using (var sw = new StreamWriter(path + fileName, false))
            {
                await sw.WriteLineAsync(csv);
                await sw.WriteAsync(sw.NewLine);
                await sw.FlushAsync();

            }

        }
        public static async Task FastLoad()
        {
            IConfigurationBuilder builder = new ConfigurationBuilder();
            builder.AddJsonFile(Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json"));
            var root = builder.Build();
            var strConn = root.GetConnectionString("MySqlContext");


            using (var conn = new MySqlConnection(strConn))
            {
                try
                {
                    await conn.OpenAsync();

                    List<String> listOfFieldNames = typeof(gc.Models.v1.InternalMeasurement).GetProperties().Select(f => f.Name).ToList();
                    await FastLoad(conn, listOfFieldNames, tmp_gc);

                    listOfFieldNames = typeof(gc.Models.v1.InternalLatLng).GetProperties().Select(f => f.Name).ToList();
                    await FastLoad(conn, listOfFieldNames, tmp_latlng);

                    listOfFieldNames = typeof(gc.Models.v1.InternalWind).GetProperties().Select(f => f.Name).ToList();
                    await FastLoad(conn, listOfFieldNames, tmp_wind);

                    listOfFieldNames = typeof(gc.Models.v1.InternalSys).GetProperties().Select(f => f.Name).ToList();
                    await FastLoad(conn, listOfFieldNames, tmp_gc_sys);

                }
                finally
                {
                    await conn.CloseAsync();
                    await conn.DisposeAsync();
                }

            }
        }
        private static void MergeFiles(FileInfo[] files, string outputFilePath)
        {            
            foreach (FileInfo file in files)
            {
                using (var outputStream = File.AppendText(outputFilePath))
                {
                    outputStream.WriteLine(File.ReadAllText(file.FullName));
                }
            }
        }

        private static async Task FastLoad(MySqlConnection conn, List<String> columnNames, String tableName)
        {                        
            //get list of old "master" files that were loaded more than a day prior and remove them


            //get list of pending files for the table we wish to populate
            string filepath = Path.GetTempPath();
            string searchPattern = tableName+"_auto_*.txt";
            DirectoryInfo di = new DirectoryInfo(@filepath);
            FileInfo[] files = di.GetFiles(searchPattern);

            //merge all files into one for a single upload
            string mergedFileName = filepath + "master_" + tableName + "_" + Guid.NewGuid().ToString() + ".txt";
            MergeFiles(files, mergedFileName);

            //bulk upload single merged file
            var loader = new MySqlBulkLoader(conn)
            {
                FileName = mergedFileName,
                TableName = tableName,
                Timeout = 0,
                Local = true,
                FieldTerminator = ",",
                FieldQuotationCharacter = '"',
                NumberOfLinesToSkip = 1,
            };
            loader.Columns.Clear();
            foreach (string col in columnNames)
            {
                loader.Columns.Add(col);
            }
            int count = loader.Load();
            

            //remove small individual files that were merged
            foreach (FileInfo file in files)
            {
                if (System.IO.File.Exists(file.FullName))
                {
                    System.IO.File.Delete(file.FullName);
                }
            }

            //remove merged file
            if (System.IO.File.Exists(mergedFileName))
            {
                System.IO.File.Delete(mergedFileName);
            }               

        }
        public static async Task<List<gc.Models.v1.Json.TerraBaseSites>> TerraBaseSitesGetAll(MySqlContext context, Int32 manufacturerId)
        {

            List < gc.Models.v1.Json.TerraBaseSites > sites = new List<gc.Models.v1.Json.TerraBaseSites>();
            return sites;

        }

        public static async Task LovMemPost(MySqlContext context,Int32 manufacturerId, String username, String lovclass, String id,String description=null, String param1=null, String param2 = null, String param3 = null, 
            String param4 = null, String param5 = null)
        {
            LovMem mem = new LovMem();
            mem.Class = lovclass;
            mem.Value = id;
            mem.ManufacturerId = manufacturerId;
            mem.Description = description;
            mem.Param1 = param1;
            mem.Param2 = param2;
            mem.Param3 = param3;
            mem.Param4 = param4;
            mem.Param5 = param5;
            mem.SystemParam = "N";
            mem.CreatedBy = username;
            mem.CreatedDt = DateTime.UtcNow;
            await context.AddAsync(mem);
            await context.SaveChangesAsync();
        }
        public static async Task LovMemPut(MySqlContext context,Int32 manufacturerId, String lovclass, String id, String newvalue, String description=null, String param1=null, String param2 = null, String param3 = null,
    String param4 = null, String param5 = null)
        {
            LovMem mem = await context.LovMems.WhereAwait(async x => (
               x.Class.Equals(lovclass) &&
               x.SystemParam.Equals("N") &&
               x.ManufacturerId.Equals(manufacturerId) &&
               x.Value.Equals(id))).FirstOrDefaultAsync();
            context.Entry(mem).Reload();

            if (mem!=null)
            {
                mem.Value = newvalue;
                mem.Description = description;
                mem.Param1 = param1;
                mem.Param2 = param2;
                mem.Param3 = param3;
                mem.Param4 = param4;
                mem.Param5 = param5;

                context.Update(mem);
                await context.SaveChangesAsync();
            }

        }
        public static async Task LovMemDel(MySqlContext context, Int32 manufacturerId, String lovclass, String id)
        {
            LovMem mem = await context.LovMems.WhereAwait(async x => (
                x.Class.Equals(lovclass) &&
                x.SystemParam.Equals("N") &&
                x.ManufacturerId.Equals(manufacturerId) &&
                x.Value.Equals(id))).FirstOrDefaultAsync();
            context.Entry(mem).Reload();

            if (mem!=null)
            {
                context.LovMems.Remove(mem);
                await context.SaveChangesAsync();
            }
        }

        public static async Task<List<LovMem>> LovMemGet(MySqlContext context, Int32 manufacturerId, String lovclass)
        {
            List<LovMem> list = await context.LovMems.FromSqlInterpolated($"SELECT * FROM lov_mem WHERE Class={lovclass} AND (SystemParam='Y' || ManufacturerId={manufacturerId}) ")
                .OrderBy(x=>x.Value)
                .AsNoTracking()
                .ToListAsync();
            //List< LovMem> list = await context.LovMems.WhereAwait(async x => (
            //    x.Class.Equals(lovclass) &&
            //    (x.SystemParam.Equals("Y") || x.ManufacturerId.Equals(manufacturerId))))
            //    .OrderByAwait(async x=>x.Value)
            //    .ToListAsync();
            return list;
        }
        public static async Task<LovMem> LovMemGet(MySqlContext context, Int32 manufacturerId, String lovclass, String id)
        {
            LovMem mem = await context.LovMems.FromSqlInterpolated($"SELECT * FROM lov_mem WHERE Value={id} AND Class={lovclass} AND (SystemParam='Y' || ManufacturerId={manufacturerId}) ")
                .OrderBy(x => x.Value)
                .AsNoTracking()
                .FirstOrDefaultAsync();

            //LovMem mem = await context.LovMems.WhereAwait(async x => (
            //    x.Class.Equals(lovclass) &&
            //    (x.SystemParam.Equals("Y") || x.ManufacturerId.Equals(manufacturerId)) &&
            //    x.Value.Equals(id, StringComparison.OrdinalIgnoreCase))).FirstOrDefaultAsync();
            return mem;
        }
        public static async Task<LovMem> LovMemGetDesc(MySqlContext context, Int32 manufacturerId, String lovclass, String description)
        {
            LovMem lovItem = await context.LovMems.FromSqlInterpolated($"SELECT * FROM lov_mem WHERE Description={description} AND Class={lovclass} AND (SystemParam='Y' || ManufacturerId={manufacturerId}) ")
                .OrderBy(x => x.Value)
                .AsNoTracking()
                .FirstOrDefaultAsync();

            //List<LovMem> lovList = await LovMemGet(context, manufacturerId, lovclass);
            //LovMem lovItem = lovList.Where(x => (
            //                                x.Description.Equals(description, StringComparison.OrdinalIgnoreCase) &&
            //                                (x.ManufacturerId.Equals(manufacturerId) || x.SystemParam.Equals("Y"))))
            //                                .FirstOrDefault();
            return lovItem;
        }
        public static async Task<List<gc.Models.v1.Json.SensorDeviceShort>> GetDevices(MySqlContext context, Int32 ManufacturerId)
        {
            List<SysDevice> devices = await GetDevicesVerbose(context, ManufacturerId);

            List<gc.Models.v1.Json.SensorDeviceShort> deviceList = new List<Models.v1.Json.SensorDeviceShort>();
            
            foreach (SysDevice q in devices)
            {
                gc.Models.v1.Json.SensorDeviceShort device = new gc.Models.v1.Json.SensorDeviceShort();
                device.id = q.DeviceSerialNo;
                device.name = q.DeviceName;
                deviceList.Add(device);
            }
            return deviceList;

        }
        public static async Task<List<gc.Models.v1.Json.SensorDeviceShort>> GetDevices(MySqlContext context, Int32 ManufacturerId, String Ids)
        {
            //if user wants all devices, then query all devices
            if (Ids.ToLower() == "all")
            {
                return await GetDevices(context, ManufacturerId);
            }

            List<SysDevice> devices = await GetDevicesVerbose(context, ManufacturerId);
            List<gc.Models.v1.Json.SensorDeviceShort> deviceList = new List<Models.v1.Json.SensorDeviceShort>();


            //if user wants certain devices, then query certain devices
            List<String> IdList = Ids.Split(",").ToList();
            List<SysDevice> devices2 = devices.Where(x => IdList.Contains(x.DeviceSerialNo)).ToList();

            foreach (SysDevice q in devices2)
            {
                gc.Models.v1.Json.SensorDeviceShort device = new gc.Models.v1.Json.SensorDeviceShort();
                device.id = q.DeviceSerialNo;
                device.name = q.DeviceName;
                deviceList.Add(device);
            }
            return deviceList;




        }
        public static async Task<List<SysDevice>> GetDevicesVerbose(MySqlContext context, Int32 ManufacturerId)
        {
            List<SysDevice> devices = await context.SysDevices.FromSqlRaw("SELECT * FROM sys_device WHERE ManufacturerId="+ManufacturerId.ToString()+";").AsNoTracking().ToListAsync();

            return devices;
        }
        public static async Task<SysDevice> GetDevice(MySqlContext context, Int32 ManufacturerId, String DeviceSerialNo)
        {
            SysDevice device = await context.SysDevices.FromSqlInterpolated($"SELECT * FROM sys_device WHERE ManufacturerId={ManufacturerId} AND DeviceSerialNo={DeviceSerialNo} ")
                .AsNoTracking()
                .FirstOrDefaultAsync();
            return device;
        }
        public static async Task<String> GetDefaultDeviceCategory(MySqlContext context, Int32 ManufacturerId)
        {
            SysManufacturer manufacturer = await context.SysManufacturers.FromSqlInterpolated($"SELECT * FROM sys_manufacturer WHERE ManufacturerId={ManufacturerId} ")
                .AsNoTracking()
                .FirstOrDefaultAsync();

            return manufacturer.DefaultDeviceCategory;
        }
        public static async Task PostDevice(MySqlContext context, Int32 ManufacturerId, String DeviceSerialNo, String DeviceName, String DeviceCategory)
        {
            /*
            var device = await context
                  .SysDevices
                  .FromSqlInterpolated(
                    $"CALL usp_ins_sys_device({ManufacturerId}, {DeviceSerialNo},{DeviceName})")
                  .FirstOrDefaultAsync();
            */
            try
            {
                SysDevice device = new SysDevice();
                device.ManufacturerId = ManufacturerId;
                device.DeviceSerialNo = DeviceSerialNo;
                device.DeviceName = DeviceName;
                device.DeviceDescription = DeviceName;
                device.DeviceCategory = DeviceCategory;
                device.SensorType = "OUTDOOR";
                device.IsEnabled = "Y";
                await context.AddAsync(device);
                await context.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }
            
        }
        public static async Task PutDevice(MySqlContext context, Int32 ManufacturerId, String DeviceSerialNo, String DeviceName, String DeviceCategory)
        {
            /*
            var device = await context
              .SysDevices
              .FromSqlInterpolated(
                $"CALL usp_upd_sys_device({ManufacturerId}, {DeviceSerialNo},{DeviceName})")
              .FirstOrDefaultAsync();
            */
            
            SysDevice device = await context.SysDevices.WhereAwait(async x => (x.ManufacturerId.Equals(ManufacturerId) && x.DeviceSerialNo.Equals(DeviceSerialNo))).FirstOrDefaultAsync();
            context.Entry(device).Reload();
            if (device!=null )
            {
                device.DeviceName = DeviceName;
                device.DeviceDescription = DeviceName;
                device.DeviceCategory = DeviceCategory;
            }
            context.Update(device);
            await context.SaveChangesAsync();
        }
        public static async Task DeleteDevice(MySqlContext context, Int32 ManufacturerId, String DeviceSerialNo)
        {
            /*
            var device = await context
              .SysDevices
              .FromSqlInterpolated(
                $"CALL usp_del_sys_device({ManufacturerId}, {DeviceSerialNo})")
              .FirstOrDefaultAsync();
            */
            
            SysDevice device = await context.SysDevices.WhereAwait(async x => (x.ManufacturerId.Equals(ManufacturerId) && x.DeviceSerialNo.Equals(DeviceSerialNo))).FirstOrDefaultAsync();
            context.Entry(device).Reload();
            if (device!=null)
            {
                context.Remove(device);
                await context.SaveChangesAsync();
            }
            
        }
        public static async Task<Int32?> GetDeviceId(MySqlContext context, Int32 ManufacturerId, String DeviceSerialNo)
        {
            //retrieve device id in a way that is safe from sql-injection
            Int32? deviceId = null;

            SysDevice device = await context.SysDevices.FromSqlInterpolated($"SELECT * FROM sys_device WHERE ManufacturerId={ManufacturerId} AND DeviceSerialNo={DeviceSerialNo} ")
                .AsNoTracking()
                .FirstOrDefaultAsync();
            
            //SysDevice device = await context.SysDevices.WhereAwait(async x => (x.ManufacturerId.Equals(ManufacturerId) && x.DeviceSerialNo.Equals(DeviceSerialNo)))
            //    .FirstOrDefaultAsync();
            if (device != null)
            {
                return device.DeviceId;
            }
            else
                return null;
        }

        public static async Task<Models.v1.Json.SensorDeviceShort> GetDeviceShort(MySqlContext context, Int32 ManufacturerId, String DeviceSerialNo)
        {
            SysDevice device = await context.SysDevices.FromSqlInterpolated($"SELECT * FROM sys_device WHERE ManufacturerId={ManufacturerId} AND DeviceSerialNo={DeviceSerialNo} ")
                .AsNoTracking()
                .FirstOrDefaultAsync();

            if (device==null)
                return null;
            else
            {
                gc.Models.v1.Json.SensorDeviceShort d = new gc.Models.v1.Json.SensorDeviceShort();
                d.id = device.DeviceSerialNo;
                d.name = device.DeviceName;
                return d;
            }
        }
        public static async Task SensorDataPost(MySqlContext context, Int32 manufacturerId, List<SysDevice> deviceList, List<gc.Models.v1.Json.SensorDevice> dataFile)
        {

            //add the internal deviceId (int32) to the data
            List<gc.Models.v1.InternalSensorDevice> dataFile3 = (from d in dataFile
                                                                    join dl in deviceList on new { d.id } equals new { id = (string)dl.DeviceSerialNo }
                                                                    select new gc.Models.v1.InternalSensorDevice
                                                                    {
                                                                        id = d.id,
                                                                        name = d.name,
                                                                        deviceId = dl.DeviceId,
                                                                        latlngData = d.latlngData,
                                                                        sysData = d.sysData,
                                                                        windData = d.windData,
                                                                        measurementData = d.measurementData
                                                                    }).ToList();

            //insert the data                
            await SaveToDatGc(context, dataFile3);
            await SaveToDatWind(context, dataFile3);
            await SaveToDeviceLatLng(context, dataFile3);
            await SaveToDatGcSys(context, dataFile3);
            
        }
        //public static async Task AirSensorDataPost_ProcessUpload(MySqlConnection conn)
        //{
        //    using (MySqlCommand cmd = new MySqlCommand("usp_air_sensor_data_post_process_upload", conn))
        //    {
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.UpdatedRowSource = UpdateRowSource.None;
        //        await cmd.ExecuteNonQueryAsync();
        //    }
       // }
        /*public static async Task SaveDevice(MySqlContext context,Int32 ManufacturerId, List<gc.Models.v1.Json.SensorDeviceShort> devicesToAdd)
        {
            string ConnectionString = ConnectionWriter();
            string Command = String.Format(@"INSERT IGNORE INTO sys_device(ManufacturerId, DeviceSerialNo, DeviceName, DeviceDescription, SensorType, IsEnabled,DeviceAddedDt) 
                                VALUES(
                                     ?ManufacturerId
                                    ,?DeviceSerialNo
                                    ,?DeviceName
                                    ,?DeviceDescription
                                    ,?SensorType
                                    ,?IsEnabled
                                    ,?DeviceAddedDt
                                    );");
            using (var mConnection = new MySqlConnection(ConnectionString))
            {
                await mConnection.OpenAsync();

                try
                {
                    MySqlTransaction trans = await mConnection.BeginTransactionAsync();

                    using (MySqlCommand cmd = new MySqlCommand(Command, mConnection))
                    {
                        cmd.Transaction = trans;
                        foreach (gc.Models.v1.Json.SensorDeviceShort newDevice in devicesToAdd)
                        {
                            cmd.Parameters.Add("?ManufacturerId", MySqlDbType.VarChar, 50).Value = ManufacturerId;
                            cmd.Parameters.Add("?DeviceSerialNo", MySqlDbType.VarChar, 50).Value = newDevice.id;
                            cmd.Parameters.Add("?DeviceName", MySqlDbType.VarChar, 50).Value = newDevice.name;
                            cmd.Parameters.Add("?DeviceDescription", MySqlDbType.VarChar, 50).Value = newDevice.name;
                            cmd.Parameters.Add("?SiteId", MySqlDbType.Int32).Value = newDevice.siteid;
                            cmd.Parameters.Add("?SensorType", MySqlDbType.VarChar, 50).Value = "OUTDOOR";
                            cmd.Parameters.Add("?IsEnabled", MySqlDbType.VarChar, 50).Value = "Y";
                            cmd.Parameters.Add("?DeviceAddedDt", MySqlDbType.DateTime).Value = DateTime.Now;
                            await cmd.ExecuteNonQueryAsync();
                            cmd.Parameters.Clear();
                        }
                        await trans.CommitAsync();
                    }
                }
                finally
                {
                    await mConnection.CloseAsync();
                }

            }
        }*/
        private static async Task SaveToDeviceLatLng(MySqlContext context, List<gc.Models.v1.InternalSensorDevice> deviceList)
        {

            //first make global flat list
            List<gc.Models.v1.InternalLatLng> list = new List<gc.Models.v1.InternalLatLng>();
            List<gc.Models.v1.InternalLatLng> listLatest = new List<gc.Models.v1.InternalLatLng>();
            foreach (gc.Models.v1.InternalSensorDevice device in deviceList)
            {
                if (device.latlngData!=null)
                {
                    List<gc.Models.v1.InternalLatLng> miniList = (from l in device.latlngData
                                                                  orderby l.ts
                                                                  select new gc.Models.v1.InternalLatLng
                                                                  {
                                                                      ts = l.ts,
                                                                      lat = l.lat,
                                                                      lng = l.lng,
                                                                      deviceId = device.deviceId
                                                                  }).ToList();
                    list.AddRange(miniList);
                }
            }

            if (list.Count>0)
            {
                await FastSave<gc.Models.v1.InternalLatLng>(list, tmp_latlng);
                /*
                List<TmpLatlng> latlngs = new List<TmpLatlng>();
                foreach (gc.Models.v1.InternalLatLng item in list)
                {
                    TmpLatlng latlng = new TmpLatlng();
                    latlng.DeviceId = item.deviceId;
                    DateTime dt = item.ts.UtcDateTime;
                    latlng.Ts = dt;
                    latlng.Lat = item.lat;
                    latlng.Lng = item.lng;
                    latlngs.Add(latlng);
                }
                await context.SaveChangesAsync();
                */
            }
        }
        private static async Task SaveToDatGcSys(MySqlContext context, List<gc.Models.v1.InternalSensorDevice> deviceList)
        {
            //first make global flat list
            List<gc.Models.v1.InternalSys> list = new List<gc.Models.v1.InternalSys>();
            List<gc.Models.v1.InternalSys> listLatest = new List<gc.Models.v1.InternalSys>();
            foreach (gc.Models.v1.InternalSensorDevice device in deviceList)
            {
                if (device.sysData!=null)
                {
                    List<gc.Models.v1.InternalSys> miniList = (from s in device.sysData
                                                               where (s.microTempC != null || s.batteryVoltage != null || s.enclosureTempC != null)
                                                               orderby s.ts
                                                               select new gc.Models.v1.InternalSys
                                                               {
                                                                   ts = s.ts,
                                                                   batteryVoltage = s.batteryVoltage,
                                                                   enclosureTempC = s.enclosureTempC,
                                                                   microTempC = s.microTempC,
                                                                   deviceId = device.deviceId
                                                               }).ToList();
                    list.AddRange(miniList);
                }
            }

            
            if (list.Count > 0)
            {
                await FastSave<gc.Models.v1.InternalSys>(list, tmp_gc_sys);
                /*
                List<TmpGcSy> gcsysList = new List<TmpGcSy>();
                foreach (gc.Models.v1.InternalSys item in list)
                {
                    TmpGcSy gcsys = new TmpGcSy();
                    gcsys.DeviceId = item.deviceId;
                    DateTime dt = item.ts.UtcDateTime;
                    gcsys.Ts = dt;
                    gcsys.BatteryVoltage = item.batteryVoltage;
                    gcsys.EnclosureTempC = item.enclosureTempC;
                    gcsys.MicroTempC = item.microTempC;
                    gcsysList.Add(gcsys);
                }
                await context.SaveChangesAsync();
                */
            }
        }

        private static async Task SaveToDatWind(MySqlContext context, List<gc.Models.v1.InternalSensorDevice> deviceList)
        {
            List<gc.Models.v1.InternalWind> list = new List<gc.Models.v1.InternalWind>();
            List<gc.Models.v1.InternalWind> listLatest = new List<gc.Models.v1.InternalWind>();
            foreach (gc.Models.v1.InternalSensorDevice device in deviceList)
            {
                if (device.windData!=null)
                {
                    List<gc.Models.v1.InternalWind> miniList = (from w in device.windData
                                                                where (w.gust != null || !String.IsNullOrEmpty(w.unit) || w.speed != null || w.dir != null)
                                                                orderby w.ts
                                                                select new gc.Models.v1.InternalWind
                                                                {
                                                                    ts = w.ts,
                                                                    unit = w.unit,
                                                                    dir = w.dir,
                                                                    gust = w.gust,
                                                                    speed = w.speed,
                                                                    deviceId = device.deviceId
                                                                }).ToList();
                    list.AddRange(miniList);
                }
            }

            if (list.Count > 0)
            {
                await FastSave<gc.Models.v1.InternalWind>(list, tmp_wind);
                /*
                List<TmpWind> windList = new List<TmpWind>();
                foreach (gc.Models.v1.InternalWind item in list)
                {
                    TmpWind wind = new TmpWind();
                    wind.DeviceId = item.deviceId;
                    DateTime dt = item.ts.UtcDateTime;
                    wind.Ts = dt;
                    wind.Unit = item.unit;
                    wind.Dir = item.dir;
                    wind.Gust = item.gust;
                    wind.Speed = item.speed;
                    windList.Add(wind);
                }
                await context.SaveChangesAsync();
                */
            }

        }
        
        private static async Task SaveToDatGc(MySqlContext context, List<gc.Models.v1.InternalSensorDevice> deviceList)
        {
            List<gc.Models.v1.InternalMeasurement> list = new List<gc.Models.v1.InternalMeasurement>();
            List<gc.Models.v1.InternalMeasurement> listLatest = new List<gc.Models.v1.InternalMeasurement>();
            foreach (gc.Models.v1.InternalSensorDevice device in deviceList)
            {
                if (device.measurementData != null)
                {
                    List<gc.Models.v1.InternalMeasurement> miniList = (from d in device.measurementData
                                                                       where (!String.IsNullOrEmpty(d.param) || d.result != null || d.peakTime != null || d.peakHeight != null || !String.IsNullOrEmpty(d.unit))
                                                                      orderby d.ts
                                                                      orderby d.param
                                                                      select new gc.Models.v1.InternalMeasurement
                                                                      {
                                                                          ts = d.ts,
                                                                          peakTime = d.peakTime,
                                                                          peakHeight = d.peakHeight,
                                                                          param = d.param,
                                                                          result = d.result,
                                                                          unit = d.unit,
                                                                          status = !String.IsNullOrEmpty(d.status) ? d.status : "meas",
                                                                          deviceId = device.deviceId
                                                                      }).ToList();
                    list.AddRange(miniList);
                }
            }
            
            if (list.Count > 0)
            {
                await FastSave<gc.Models.v1.InternalMeasurement>(list, tmp_gc);

                /*
                List<TmpGc> gclist = new List<TmpGc>();
                foreach (gc.Models.v1.InternalMeasurement item in list)
                {
                    TmpGc gc = new TmpGc();
                    gc.DeviceId = item.deviceId;
                    DateTime dt = item.ts.UtcDateTime;
                    gc.Ts = dt;
                    gc.PeakHeight = item.peakHeight;
                    gc.PeakTime = item.peakTime;
                    gc.Param = item.param;
                    gc.Result = item.result;
                    gc.Status = item.status;
                    gc.Unit = item.unit;
                    gclist.Add(gc);
                }
                await context.SaveChangesAsync();
                */
            }

        }
        //public static DataTable ToDataTable<TSource>(IEnumerable<TSource> list)
        //{
        //    using (DataTable table = new DataTable())
        //    {
        //        using (var reader = ObjectReader.Create(list))
        //        {
        //            table.Load(reader);
        //            return table;
        //        }
        //    }
        //}
        
        public static class DateTimeOffsetHelper
        {
            public static DateTimeOffset FromString(string offsetString)
            {

                DateTimeOffset offset;
                if (!DateTimeOffset.TryParse(offsetString, out offset))
                {
                    offset = DateTimeOffset.Now;
                }

                return offset;
            }
        }
        public static async Task<List<gc.Models.v1.Json.SensorDevice>> SensorDataGet(MySqlContext context, Int32 manufacturerId, String id, DateTime startUtc, DateTime endUtc)
        {
            SysDevice device = await GetDevice(context, manufacturerId, id);

            if (device == null)
                return null;

            List<DatGc> listGc = await context.DatGcs.FromSqlRaw($"SELECT * FROM dat_gc WHERE DeviceId=" + device.DeviceId + " AND TimestampUtc>='" + startUtc.ToString("yyyy-MM-dd HH:mm:ss") + "' AND TimestampUtc<='" + endUtc.ToString("yyyy-MM-dd HH:mm:ss") + "' ORDER BY TimestampUtc LIMIT 100;").AsNoTracking().ToListAsync();
            List<DatGcSy> listGcSys = await context.DatGcSys.FromSqlRaw("SELECT * FROM dat_gc_sys WHERE DeviceId=" + device.DeviceId + " AND TimestampUtc>='" + startUtc.ToString("yyyy-MM-dd HH:mm:ss") + "' AND TimestampUtc<='" + endUtc.ToString("yyyy-MM-dd HH:mm:ss") + "' ORDER BY TimestampUtc LIMIT 100;").AsNoTracking().ToListAsync();
            List<DatWind> listWind = await context.DatWinds.FromSqlRaw("SELECT * FROM dat_wind WHERE DeviceId=" + device.DeviceId + " AND TimestampUtc>='" + startUtc.ToString("yyyy-MM-dd HH:mm:ss") + "' AND TimestampUtc<='" + endUtc.ToString("yyyy-MM-dd HH:mm:ss") + "' ORDER BY TimestampUtc LIMIT 100;").AsNoTracking().ToListAsync();
            List<DatDeviceLatlng> listLatLng = await context.DatDeviceLatlngs.FromSqlRaw("SELECT * FROM dat_device_latlng WHERE DeviceId=" + device.DeviceId + " AND TimestampUtc>='" + startUtc.ToString("yyyy-MM-dd HH:mm:ss") + "' AND TimestampUtc<='" + endUtc.ToString("yyyy-MM-dd HH:mm:ss") + "' ORDER BY TimestampUtc LIMIT 100;").AsNoTracking().ToListAsync();

            List<gc.Models.v1.Json.MeasurementData> data = (from x2 in listGc
                                                           select new gc.Models.v1.Json.MeasurementData
                                                           {
                                                               ts = DateTimeOffsetHelper.FromString(x2.TimestampUtc.ToString()),
                                                               param = x2.ParamId,
                                                               result = x2.Result,
                                                               peakTime = x2.PeakTime,
                                                               peakHeight = x2.PeakHeight,
                                                               unit = x2.Unit,
                                                               status = x2.Status
                                                           }).ToList();

            List<gc.Models.v1.Json.SysData> sys = (from s in listGcSys
                                                   select new gc.Models.v1.Json.SysData
                                                   {
                                                       ts = DateTimeOffsetHelper.FromString(s.TimestampUtc.ToString()),
                                                       microTempC = s.MicroTempC,
                                                       batteryVoltage = s.BatteryVoltage,
                                                       enclosureTempC = s.EnclosureTemp
                                                   }).ToList();

            List<gc.Models.v1.Json.LatLngData> latlng = (from l in listLatLng
                                                         select new gc.Models.v1.Json.LatLngData
                                                         {
                                                             ts = DateTimeOffsetHelper.FromString(l.TimestampUtc.ToString()),
                                                             lat = l.Latitude,
                                                             lng = l.Longitude
                                                         }).ToList();

            List<gc.Models.v1.Json.WindData> wind = (from w in listWind
                                                     select new gc.Models.v1.Json.WindData
                                                     {
                                                         ts = DateTimeOffsetHelper.FromString(w.TimestampUtc.ToString()),
                                                         speed = w.WindSpeed,
                                                         dir = w.WindDirection,
                                                         gust = w.Gust,
                                                         unit = w.WindUnit
                                                     }).ToList();

            gc.Models.v1.Json.SensorDevice returnDevice = new gc.Models.v1.Json.SensorDevice();

            returnDevice.measurementData = data;
            returnDevice.sysData = sys;
            returnDevice.latlngData = latlng;
            returnDevice.windData = wind;

            List<gc.Models.v1.Json.SensorDevice> returnDeviceList = new List<gc.Models.v1.Json.SensorDevice>();
            returnDeviceList.Add(returnDevice);
            return returnDeviceList;
        }
        public static async Task<List<gc.Models.v1.Json.SensorDevice>> SensorDataGetRecent(MySqlContext context, Int32 manufacturerId)
        {
            List<Models.v1.Json.SensorDeviceShort> devices = await GetDevices(context, manufacturerId);

            if (devices.Count==0)
                return null;

            List<DatGc> listGc = await context.DatGcs.FromSqlRaw(@"SELECT a.DeviceId, a.TimestampUtc, a.ParamId, a.Result, a.Unit, 'meas' as Status
                FROM dat_latest_device_gc a
                INNER JOIN sys_device b
                WHERE a.DeviceId = b.DeviceId
                AND b.ManufacturerId = "+ manufacturerId.ToString() +"; ").AsNoTracking().ToListAsync();

            List<DatGcSy> listGcSys = await context.DatGcSys.FromSqlRaw(@"SELECT a.DeviceId, SysModifiedUtc AS TimestampUtc, a.MicrotempC, a.BatteryVoltage, a.EnclosureTemp, a.LastModifiedUtcDt
                    SELECT a.DeviceId, SysModifiedUtc AS TimestampUtc, a.MicrotempC, a.BatteryVoltage, a.EnclosureTemp
                    FROM dat_latest_device a
                    INNER JOIN sys_device b
                    WHERE a.DeviceId=b.DeviceId
                    AND SysModifiedUtc IS NOT NULL
                    AND b.ManufacturerId = " + manufacturerId.ToString() +"; ").AsNoTracking().ToListAsync();
            List<DatWind> listWind = await context.DatWinds.FromSqlRaw(@"SELECT a.DeviceId, WindModifiedUtc AS TimestampUtc, a.Windspeed, a.WindDirection, a.WindUnit, a.Gust
                    FROM dat_latest_device a
                    INNER JOIN sys_device b
                    WHERE a.DeviceId = b.DeviceId
                    AND WindModifiedUtc IS NOT NULL
                    AND b.ManufacturerId = " + manufacturerId.ToString() +"; ").AsNoTracking().ToListAsync();
            List<DatDeviceLatlng> listLatLng = await context.DatDeviceLatlngs.FromSqlRaw(@"SELECT a.DeviceId, LatLngModifiedUtc AS TimestampUtc, a.Latitude, a.Longitude
                    FROM dat_latest_device a
                    INNER JOIN sys_device b
                    WHERE a.DeviceId = b.DeviceId
                    AND LatLngModifiedUtc IS NOT NULL
                    AND b.ManufacturerId = " + manufacturerId.ToString() +"; ").AsNoTracking().ToListAsync();

            List<gc.Models.v1.InternalMeasurement> data = (from x2 in listGc
                                                           select new gc.Models.v1.InternalMeasurement
                                                           {
                                                               deviceId=x2.DeviceId,
                                                               ts = DateTimeOffsetHelper.FromString(x2.TimestampUtc.ToString()),
                                                               param = x2.ParamId,
                                                               result = x2.Result,
                                                               unit = x2.Unit,
                                                               status = x2.Status
                                                           }).ToList();

            List<gc.Models.v1.InternalSys> sys = (from s in listGcSys
                                                   select new gc.Models.v1.InternalSys
                                                   {
                                                       deviceId=s.DeviceId,
                                                       ts = DateTimeOffsetHelper.FromString(s.TimestampUtc.ToString()),
                                                       microTempC = s.MicroTempC,
                                                       batteryVoltage = s.BatteryVoltage,
                                                       enclosureTempC = s.EnclosureTemp
                                                   }).ToList();

            List<gc.Models.v1.InternalLatLng> latlng = (from l in listLatLng
                                                         select new gc.Models.v1.InternalLatLng
                                                         {
                                                             deviceId=l.DeviceId,
                                                             ts = DateTimeOffsetHelper.FromString(l.TimestampUtc.ToString()),
                                                             lat = l.Latitude,
                                                             lng = l.Longitude
                                                         }).ToList();

            List<gc.Models.v1.InternalWind> wind = (from w in listWind
                                                     select new gc.Models.v1.InternalWind
                                                     {
                                                         deviceId=w.DeviceId,
                                                         ts = DateTimeOffsetHelper.FromString(w.TimestampUtc.ToString()),
                                                         speed = w.WindSpeed,
                                                         dir = w.WindDirection,
                                                         gust = w.Gust,
                                                         unit = w.WindUnit
                                                     }).ToList();

            //bring it together into the heirarchal model, even though at most only 1 record per subtype is included.
            List<gc.Models.v1.Json.SensorDevice> returnDeviceList = (from d in devices
                                                                      select new gc.Models.v1.Json.SensorDevice
                                                                      {
                                                                          id = d.id,
                                                                          name = d.name,
                                                                          cat = d.cat,
                                                                          measurementData = (from a in data
                                                                                            where d.id.Equals(a.deviceId)
                                                                                            select new gc.Models.v1.Json.MeasurementData
                                                                                            {
                                                                                                ts = a.ts,
                                                                                                param = a.param,
                                                                                                result = a.result,
                                                                                                unit = a.unit,
                                                                                                status = a.status
                                                                                            }).ToList(),
                                                                          sysData = (from s in sys
                                                                                     where d.id.Equals(s.deviceId)
                                                                                     select new gc.Models.v1.Json.SysData
                                                                                     {
                                                                                         ts = s.ts,
                                                                                         microTempC = s.microTempC,
                                                                                         batteryVoltage = s.batteryVoltage,
                                                                                         enclosureTempC = s.enclosureTempC
                                                                                     }).ToList(),
                                                                          latlngData = (from l in latlng
                                                                                        where d.id.Equals(l.deviceId)
                                                                                        select new gc.Models.v1.Json.LatLngData
                                                                                        {
                                                                                            ts = l.ts,
                                                                                            lat = l.lat,
                                                                                            lng = l.lng
                                                                                        }
                                                                                        ).ToList(),
                                                                          windData = (from w in wind
                                                                                      where d.id.Equals(w.deviceId)
                                                                                      select new gc.Models.v1.Json.WindData
                                                                                      {
                                                                                          ts = w.ts,
                                                                                          dir = w.dir,
                                                                                          speed = w.speed,
                                                                                          unit = w.unit,
                                                                                          gust = w.gust
                                                                                      }
                                                                                      ).ToList()
                                                                      }).ToList();

            return returnDeviceList;
        }

        public static async Task<gc.Models.v1.Json.SensorDevice> GetAny(MySqlContext context, Int32 manufacturerId, String id)
        {

            SysDevice device = await context.SysDevices.WhereAwait(async x => (x.ManufacturerId.Equals(manufacturerId) && x.DeviceSerialNo.Equals(id))).FirstOrDefaultAsync();

            if (device == null)
                return null;

            List<DatGc> listGc = await context.DatGcs.FromSqlRaw("SELECT * FROM dat_gc WHERE DeviceId=" + device.DeviceId + " LIMIT 1;").AsNoTracking().ToListAsync();
            List<DatGcSy> listGcSys = await context.DatGcSys.FromSqlRaw("SELECT * FROM dat_gc_sys WHERE DeviceId=" + device.DeviceId + " LIMIT 1;").AsNoTracking().ToListAsync();
            List<DatWind> listWind = await context.DatWinds.FromSqlRaw("SELECT * FROM dat_wind WHERE DeviceId=" + device.DeviceId + " LIMIT 1 1;").AsNoTracking().ToListAsync();
            List<DatDeviceLatlng> listLatLng = await context.DatDeviceLatlngs.FromSqlRaw("SELECT * FROM dat_device_latlng WHERE DeviceId=" + device.DeviceId + " LIMIT 1;").AsNoTracking().ToListAsync();

            List<gc.Models.v1.Json.MeasurementData> data = (from x2 in listGc
                                                           select new gc.Models.v1.Json.MeasurementData
                                                           {
                                                               ts = DateTimeOffsetHelper.FromString(x2.TimestampUtc.ToString()),
                                                               param = x2.ParamId,
                                                               result = x2.Result,
                                                               peakTime = x2.PeakTime,
                                                               peakHeight = x2.PeakHeight,
                                                               unit = x2.Unit,
                                                               status = x2.Status
                                                           }).Take(1).ToList();

            List<gc.Models.v1.Json.SysData> sys = (from s in listGcSys
                                                   select new gc.Models.v1.Json.SysData
                                                   {
                                                       ts = DateTimeOffsetHelper.FromString(s.TimestampUtc.ToString()),
                                                       microTempC = s.MicroTempC,
                                                       batteryVoltage = s.BatteryVoltage,
                                                       enclosureTempC = s.EnclosureTemp
                                                   }).Take(1).ToList();

            List<gc.Models.v1.Json.LatLngData> latlng = (from l in listLatLng
                                                         select new gc.Models.v1.Json.LatLngData
                                                         {
                                                             ts = DateTimeOffsetHelper.FromString(l.TimestampUtc.ToString()),
                                                             lat = l.Latitude,
                                                             lng = l.Longitude
                                                         }).Take(1).ToList();

            List<gc.Models.v1.Json.WindData> wind = (from w in listWind
                                                     select new gc.Models.v1.Json.WindData
                                                     {
                                                         ts = DateTimeOffsetHelper.FromString(w.TimestampUtc.ToString()),
                                                         speed = w.WindSpeed,
                                                         dir = w.WindDirection,
                                                         gust = w.Gust,
                                                         unit = w.WindUnit
                                                     }).Take(1).ToList();

            gc.Models.v1.Json.SensorDevice returnDevice = new gc.Models.v1.Json.SensorDevice();

            returnDevice.measurementData = data;
            returnDevice.sysData = sys;
            returnDevice.latlngData = latlng;
            returnDevice.windData = wind;
            return returnDevice;
        }
    }
}

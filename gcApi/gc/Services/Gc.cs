﻿using gc.DataAccess.DataObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gc.Services
{
    public static partial class Api
    {
        public static async Task<List<gc.Models.v1.Delimited>> GetFlatAsync(Int32 manufacturerId, String id, DateTime startUtc, DateTime endUtc)
        {
            List<gc.Models.v1.Delimited> returnList = new List<gc.Models.v1.Delimited>();
            return returnList;
        }
        public static async Task<List<gc.Models.v1.Json.SensorDevice>> GetJson(MySqlContext context,Int32 manufacturerId, String id, DateTime startUtc, DateTime endUtc)
        {
            return await gc.Services.Aurora.SensorDataGet(context,manufacturerId, id, startUtc, endUtc);
        }
        public static async Task PostFlat(MySqlContext context,Int32 manufacturerId, List<gc.Models.v1.Delimited> dataFile)
        {
            List<gc.Models.v1.Json> finalList = new List<gc.Models.v1.Json>();
            List<gc.Models.v1.Json.SensorDevice> deviceList = new List<gc.Models.v1.Json.SensorDevice>();

            //group devices
            var devices =
                dataFile.GroupBy(c => new
                {
                    c.id,
                    c.name
                })
                .OrderBy(x => x.Key.name)
                .Select(gcs => new
                {
                    gcs.Key.id,
                    gcs.Key.name
                });

            //traverse devices, converting to strong typing
            foreach (var device in devices)
            {
                gc.Models.v1.Json.SensorDevice d = new gc.Models.v1.Json.SensorDevice();
                d.id = device.id;
                d.name = device.name;
                deviceList.Add(d);
            }

            //search for duplicates with different names; abort if found
            foreach (gc.Models.v1.Json.SensorDevice device in deviceList)
            {
                gc.Models.v1.Json.SensorDevice searchDevice = deviceList.Where(x => (
                        x.name.Equals(device.name) &&
                        x.id != device.id)).FirstOrDefault();
                if (searchDevice != null)
                    throw new Exception("Different device names for same device id.");
            }

            //traverse device list and convert flat to structured data
            foreach (gc.Models.v1.Json.SensorDevice device in deviceList)
            {
                List<gc.Models.v1.Delimited> searchList = dataFile.Where(x => x.id.Equals(device.id)).ToList();
                var data =
                    searchList.GroupBy(c => new
                    {
                        c.ts,
                        c.param,
                        c.result,
                        c.peakTime,
                        c.peakHeight,
                        c.unit,
                        c.status
                    })
                    .OrderBy(x => x.Key.ts)
                    .ThenBy(x => x.Key.param)
                    .Where(x => (
                        x.Key.param != null ||
                        x.Key.result != null ||
                        x.Key.peakTime != null ||
                        x.Key.peakHeight != null ||
                        x.Key.unit != null ||
                        x.Key.status != null
                    ))
                    .Select(gcs => new
                    {
                        gcs.Key.ts,
                        gcs.Key.param,
                        gcs.Key.result,
                        gcs.Key.peakTime,
                        gcs.Key.peakHeight,
                        gcs.Key.unit,
                        gcs.Key.status
                    });
                foreach (var d in data)
                {
                    gc.Models.v1.Json.MeasurementData d2 = new gc.Models.v1.Json.MeasurementData();
                    d2.ts = d.ts;
                    d2.param = d.param;
                    d2.result = d.result;
                    d2.peakTime = d.peakTime;
                    d2.peakHeight = d.peakHeight;
                    d2.unit = d.unit;
                    d2.status = d.status;
                    device.measurementData.Add(d2);
                }

                var sys =
                    searchList.GroupBy(c => new
                    {
                        c.ts,
                        c.microTempC,
                        c.batteryVoltage,
                        c.enclosureTempC
                    })
                    .OrderBy(x => x.Key.ts)
                    .Where(x => (
                        x.Key.microTempC != null ||
                        x.Key.batteryVoltage != null ||
                        x.Key.enclosureTempC != null
                    ))
                    .Select(gcs => new
                    {
                        gcs.Key.ts,
                        gcs.Key.microTempC,
                        gcs.Key.batteryVoltage,
                        gcs.Key.enclosureTempC
                    });
                foreach (var s in sys)
                {
                    gc.Models.v1.Json.SysData s2 = new gc.Models.v1.Json.SysData();
                    s2.ts = s.ts;
                    s2.microTempC = s.microTempC;
                    s2.batteryVoltage = s.batteryVoltage;
                    s2.enclosureTempC = s.enclosureTempC;
                    device.sysData.Add(s2);
                }

                var wind =
                    searchList.GroupBy(c => new
                    {
                        c.ts,
                        c.speed,
                        c.dir,
                        c.windUnit,
                        c.gust
                    })
                    .OrderBy(x => x.Key.ts)
                    .Where(x => (
                        x.Key.speed != null ||
                        x.Key.dir != null ||
                        x.Key.windUnit != null ||
                        x.Key.gust != null
                    ))
                    .Select(gcs => new
                    {
                        gcs.Key.ts,
                        gcs.Key.speed,
                        gcs.Key.dir,
                        gcs.Key.windUnit,
                        gcs.Key.gust
                    });
                foreach (var w in wind)
                {
                    gc.Models.v1.Json.WindData w2 = new gc.Models.v1.Json.WindData();
                    w2.ts = w.ts;
                    w2.speed = w.speed;
                    w2.dir = w.dir;
                    w2.unit = w.windUnit;
                    w2.gust = w.gust;
                    device.windData.Add(w2);
                }

                var latlng =
                    searchList.GroupBy(c => new
                    {
                        c.ts,
                        c.lat,
                        c.lng
                    })
                    .OrderBy(x => x.Key.ts)
                    .Where(x => (
                        x.Key.lat != null ||
                        x.Key.lng != null
                    ))
                    .Select(gcs => new
                    {
                        gcs.Key.ts,
                        gcs.Key.lat,
                        gcs.Key.lng
                    });
                foreach (var l in latlng)
                {
                    gc.Models.v1.Json.LatLngData l2 = new gc.Models.v1.Json.LatLngData();
                    l2.ts = l.ts;
                    l2.lat = (float)l.lat;
                    l2.lng = (float)l.lng;
                    device.latlngData.Add(l2);
                }
            }

            //Post(manufacturerId, deviceList);
        }
        public static async Task Post(MySqlContext context,Int32 manufacturerId, List<SysDevice> deviceList, List<gc.Models.v1.Json.SensorDevice> dataFile)
        {
            await gc.Services.Aurora.SensorDataPost(context,manufacturerId, deviceList, dataFile);
        }
        //public static async Task<List<dynamic>> GetDevices(MySqlContext context,Int32 manufacturerId)
        //{
        //    String sql = "SELECT DeviceSerialNo AS id, DeviceName as name, DeviceAddedDt as addedDt FROM sys_device WHERE ManufacturerId=" + manufacturerId.ToString() + " ORDER BY DeviceName;";           
        //    List<dynamic> deviceList = await gc.Services.Aurora.QueryGenericData(conn,sql).ToListAsync();
        //    return deviceList;
       // }
    }
}

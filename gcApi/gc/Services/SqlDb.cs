﻿using gc.Authentication;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
//using System.Configuration;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace gc.Services
{
    public static class SqlDb
    {
        public static class AuthSites
        {
            public static async Task<List<gc.Models.v1.Json.TerraBaseSites>> GetAll(Int32 manufacturerId)
            {
                var config = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json").Build();
                string _conn = config["ConnectionStrings:ConnStr"];
                
                using (SqlConnection sql = new SqlConnection(_conn))
                {
                    using (SqlCommand cmd = new SqlCommand("uspGetAuthTerraBaseSiteList", sql))
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        var response = new List<gc.Models.v1.Json.TerraBaseSites>();
                        cmd.Parameters.Add(new SqlParameter("@manufacturerId", manufacturerId));                        
                        await sql.OpenAsync();

                        using (var reader = await cmd.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                response.Add(MapToValue(reader));
                            }
                        }

                        return response;
                    }
                }
            }

            private static gc.Models.v1.Json.TerraBaseSites MapToValue(SqlDataReader reader)
            {
                return new gc.Models.v1.Json.TerraBaseSites()
                {
                    siteid=(int)reader["LovSiteId"],
                    sitename=reader["SiteName"].ToString(),
                    tenant=reader["TenantName"].ToString()
                };
            }
        
        }
    }
}

using gc.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.OpenApi.Models;
using Ben.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using System.Reflection;
using System;
using System.IO;
using Swashbuckle.AspNetCore.SwaggerUI;
using Swashbuckle.Examples;
//using Microsoft.Extensions.Caching.Distributed;
using static gc.Extensions.SwaggerExtensions;
using gc.Extensions;
using gc.DataAccess.DataObjects;
using Quartz;
using Quartz.Impl;
using Microsoft.Extensions.Logging;
using Quartz.Spi;
using Microsoft.AspNetCore.ResponseCompression;
using System.IO.Compression;
using StackExchange.Redis;

using System.Collections.Generic;
using Microsoft.Extensions.Caching.Distributed;

namespace gc
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                  .SetBasePath(env.ContentRootPath)
                  .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                  .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                  .AddEnvironmentVariables();
            Configuration = builder.Build();

            //Configuration = configuration;
            _env = env;
        }

        public IConfiguration Configuration { get; }
        private readonly IWebHostEnvironment _env;

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddApiVersioning(options =>
            {
                options.DefaultApiVersion = new ApiVersion(1, 0);
                options.AssumeDefaultVersionWhenUnspecified = true;
                options.ReportApiVersions = true;

            });
            services.AddVersionedApiExplorer(options =>
            {
                options.GroupNameFormat = "'v'VVV";
                options.SubstituteApiVersionInUrl = true;
            });
            //services.AddTransient<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerOptions>();
            //services.AddSwaggerGen(options => options.OperationFilter<SwaggerDefaultValues>());


            //services.AddApiVersioning(o =>
            //{
            //    o.AssumeDefaultVersionWhenUnspecified = true;
            //    o.DefaultApiVersion = new ApiVersion(1, 0);
            //});
            services.AddSwaggerGen(c =>
            {

                //c.OrderActionGroupsBy(new SwaggerControllerOrderComparer<ApiController>(Assembly.GetExecutingAssembly()));

                //c.OrderActionsBy(new SwaggerControllerOrderComparer<ApiController>(Assembly.GetExecutingAssembly()));
                c.EnableAnnotations();
                c.OperationFilter<OptionalRouteParameterOperationFilter>();
                c.DocumentFilter<RemoveSchemaDocumentFilter>();
                c.DocumentFilter<SchemaSortDocumentFilter>();

                //disable for production: this will ensure no misc. registrations occur
                c.DocumentFilter<RemoveMethodsDocumentFilter>();


                c.ParameterFilter<DateExampleParameterFilter>();
                //c.MapType<System.DateTime>(() => new OpenApiSchema
                //{
                //    Type = "string",
                //    Format="date-time",
                //    Description= @"Date-time string in <a href=""https://en.wikipedia.org/wiki/ISO_8601#UTC\"">ISO 8601 format</a>.
          //Date-time without time-zone is treated as UTC."
                //});

                // Enable Swagger examples
                //c.OperationFilter<ExamplesOperationFilter>();

                // Enable swagger descriptions
                //c.OperationFilter<DescriptionOperationFilter>();

                // Enable swagger response headers
                //c.OperationFilter<AddResponseHeadersFilter>();

                // Add (Auth) to action summary
                //c.OperationFilter<AppendAuthorizeToSummaryOperationFilter>();

                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "TerraBase Sensor API",
                    Description = "API to allow sensor manufacturers to send raw sensor device data, including measurement, location and wind.  JWT authentication is used, token will be issued via the login method.",
                    Version = "v1",
                    Contact = new OpenApiContact
                    {
                        Name = "TerraBase Support",
                        Email = "support@terrabase.com"
                    },
                });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath, true);
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Name = "Authorization",
                    Type = SecuritySchemeType.Http,
                    Scheme = "Bearer",
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Description = "JWT Authorization header using the Bearer scheme."                    
                });
                c.OperationFilter<AuthOperationFilter>();
                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                          new OpenApiSecurityScheme
                            {
                                Reference = new OpenApiReference
                                {
                                    Type = ReferenceType.SecurityScheme,
                                    Id = "Bearer"
                                }
                            },
                            new string[] {}
                   }
                });
            });

            services.AddControllers();
            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("ConnStr")));

            var connectionString = Configuration.GetConnectionString("MySqlContext");
            services.AddDbContext<MySqlContext>(
                dbContextOptions => dbContextOptions
                    .UseMySql(connectionString, ServerVersion.AutoDetect(connectionString))
                    .EnableSensitiveDataLogging() // <-- These two calls are optional but help
                    .EnableDetailedErrors()       // <-- with debugging (remove for production).
                    );

            // For Identity
                services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();
            services.AddScoped<IUserClaimsPrincipalFactory<ApplicationUser>, Services.AdditionalUserClaimsPrincipalFactory>();

            // Adding Authentication
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            })

            // Adding Jwt Bearer
            .AddJwtBearer(options =>
            {
                options.SaveToken = true;
                options.RequireHttpsMetadata = false;
                options.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime=false,
                    ValidateIssuerSigningKey=true,
                    ValidAudience = Configuration["JWT:ValidAudience"],
                    ValidIssuer = Configuration["JWT:ValidIssuer"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JWT:Secret"]))
                };
            });

            services.AddMvc()
                .AddJsonOptions(options => { options.JsonSerializerOptions.IgnoreNullValues = true; })
                .ConfigureApiBehaviorOptions(options =>
                {
                    options.SuppressMapClientErrors = true;
                    options.SuppressModelStateInvalidFilter = true;
                });

            //if (_env.IsDevelopment() || _env.IsStaging())
            //{
                services.AddDistributedMemoryCache();
            //}
            //else
           // {
           //     services.AddStackExchangeRedisCache(options =>
           //     {
           //         options.Configuration = "localhost:4455";
                    
           
           //         options.InstanceName = "SampleInstance";
           //     });

                //var sprovider = services.BuildServiceProvider();

                //Use sprovder to create an instance of IDistributedCache
                //var cache = sprovider.GetService<IDistributedCache>();

            //}

            services.AddSingleton<IConfiguration>(Configuration);

            /*
            services.AddQuartz(q =>
            {
                // Use a Scoped container to create jobs. I'll touch on this later
                q.UseMicrosoftDependencyInjectionScopedJobFactory();
            });
            services.AddQuartzHostedService(
                q => q.WaitForJobsToComplete = true);
            */

            // Add Quartz services
            services.AddSingleton<IJobFactory, SingletonJobFactory>();
            services.AddSingleton<ISchedulerFactory, StdSchedulerFactory>();
            services.AddSingleton<UploadJob>();
            services.AddSingleton(new JobSchedule(
                jobType: typeof(UploadJob),
                cronExpression: "0/5 * * * * ?")); // run every 5 seconds
            services.AddHostedService<QuartzHostedService>();


            services.AddResponseCompression();
            services.Configure<GzipCompressionProviderOptions>(options =>
            {
                options.Level = CompressionLevel.Optimal;
            });
            services.AddResponseCompression(options =>
            {
                options.EnableForHttps = true;
                options.Providers.Add<GzipCompressionProvider>();
            });


        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, //IWebHostEnvironment env,
                    IHostApplicationLifetime lifetime, IDistributedCache cache)
        {
            app.UseResponseCompression();
            
            //if (env.IsDevelopment())
            //{
            //    app.UseDeveloperExceptionPage();
            //    app.UseBlockingDetection();
            //}
            app.UseCors(option => option
               .AllowAnyOrigin()
               .AllowAnyMethod()
               .AllowAnyHeader());
            
            app.UseStaticFiles();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", $"v1");
                c.DocumentTitle = "TerraBase Sensor API";
                c.DocExpansion(DocExpansion.None);
                c.RoutePrefix = string.Empty;
                c.InjectStylesheet("/swagger-ui/custom.css");
                c.InjectStylesheet("/css/fontawesome-5.15.1/css/all.min.css");
                c.InjectJavascript("/swagger-ui/custom.js", "text/javascript");
                c.InjectJavascript("/swagger-ui/rapipdf-min.js");
            });

            //setup cache with a 24-hour life
            lifetime.ApplicationStarted.Register(() =>
            {
                var currentTimeUTC = DateTime.UtcNow.ToString();
                byte[] encodedCurrentTimeUTC = Encoding.UTF8.GetBytes(currentTimeUTC);
                var options = new DistributedCacheEntryOptions()
                    .SetSlidingExpiration(TimeSpan.FromDays(1));
                cache.Set("cachedTimeUTC", encodedCurrentTimeUTC, options);
            });

        }
    }
    
}

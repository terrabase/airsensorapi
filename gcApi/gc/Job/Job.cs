﻿using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Threading.Tasks;

[DisallowConcurrentExecution]
public class UploadJob : IJob
{
    private readonly ILogger<UploadJob> _logger;
    public UploadJob(ILogger<UploadJob> logger)
    {
        _logger = logger;
    }

    public Task Execute(IJobExecutionContext context)
    {
        try
        {
            return gc.Services.Aurora.FastLoad();
        }
        catch (Exception err)
        {
            _logger.LogError(err, "Scheduled job 'gc.Services.Aurora.FastLoad' failed.");
            return null;
        }

        //return gc.Services.Aurora.FastLoad();
    }
}
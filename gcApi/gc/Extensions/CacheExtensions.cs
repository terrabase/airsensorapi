﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;
using System.Text.Json;
using System.Text.Json.Serialization;
using Utf8Json;

namespace gc.Extensions
{
    
    public static class CacheExtensions
    {
        public static async Task<T> GetCacheValueAsync<T>(this IDistributedCache cache, string key) where T : class
        {
            string result = await cache.GetStringAsync(key);
            if (String.IsNullOrEmpty(result))
            {
                return null;
            }
            var deserializedObj = Utf8Json.JsonSerializer.Deserialize<T>(result);
            return deserializedObj;
        }

        public static async Task SetCacheValueAsync<T>(this IDistributedCache cache, string key, T value) where T : class
        {
            DistributedCacheEntryOptions cacheEntryOptions = new DistributedCacheEntryOptions();

            cacheEntryOptions.AbsoluteExpirationRelativeToNow = TimeSpan.FromHours(12);
            cacheEntryOptions.SlidingExpiration = TimeSpan.FromHours(12);
            
            string result = System.Text.Json.JsonSerializer.Serialize<T>(value);

            await cache.SetStringAsync(key, result);
        }
    }
    
}

﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;

namespace Microsoft.AspNet.Identity
{
    public static class IdentityExtensions
    {
        public static Int32 ManufacturerId(this IIdentity identity)
        {
            var claim = ((ClaimsIdentity)identity).Claims.FirstOrDefault(c => c.Type == "urn:Custom:ManufacturerId");
            Int32 manufacturerId = Int32.Parse(claim.ToString());
            //return (manufacturerId != null) ? manufacturerId : null;
            return manufacturerId;
        }
    }
}

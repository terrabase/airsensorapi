﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace gc.Extensions
{
    public static class StringExtensions
    {
        public static bool Contains(this string source, string toCheck, StringComparison comp)
        {
            return source?.IndexOf(toCheck, comp) >= 0;
        }
        public static void ToFile(this String outstring, string strFilePath)
        {
            using (var sw = new StreamWriter(strFilePath, false))
            {
                sw.WriteLine(outstring);
                sw.Write(sw.NewLine);
                sw.Flush();

            }
        }
    }
}

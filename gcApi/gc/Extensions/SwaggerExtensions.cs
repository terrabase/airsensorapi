﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System.Linq;
using System.Reflection;
using System;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Swashbuckle.AspNetCore.SwaggerGen;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Any;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;

namespace gc.Extensions
{
    public class SwaggerExtensions
    {
        
        public class OptionalRouteParameterOperationFilter: IOperationFilter
        {
            const string captureName = "routeParameter";
            public void Apply(OpenApiOperation operation, OperationFilterContext context)
            {
                try
                {
                    var httpMethodAttributes = context.MethodInfo
                        .GetCustomAttributes(true)
                        .OfType<Microsoft.AspNetCore.Mvc.Routing.HttpMethodAttribute>();
                    if (httpMethodAttributes == null)
                        return;

                    var httpMethodWithOptional1 = httpMethodAttributes?.FirstOrDefault();
                    if (httpMethodWithOptional1 == null)
                        return;
                    if (httpMethodWithOptional1.Template == null)
                        return;

                    var httpMethodWithOptional = httpMethodAttributes?.FirstOrDefault(m => m.Template.Contains("?"));
                    if (httpMethodWithOptional == null)
                        return;

                    string regex = $"{{(?{captureName}>\\w+)\\?}}";

                    var matches = System.Text.RegularExpressions.Regex.Matches(httpMethodWithOptional.Template, regex);

                    foreach (System.Text.RegularExpressions.Match match in matches)
                    {
                        var name = match.Groups[captureName].Value;

                        var parameter = operation.Parameters.FirstOrDefault(p => p.In == ParameterLocation.Path && p.Name == name);
                        if (parameter != null)
                        {
                            parameter.AllowEmptyValue = true;
                            parameter.Description = "Must check \"Send empty value\" or Swagger passes a comma for empty values otherwise";
                            parameter.Required = false;
                            parameter.Schema.Nullable = true;
                        }
                    }
                }
                catch(Exception err)
                {
                    return;
                }

            }
        }
        public class AuthOperationFilter : IOperationFilter
        {
            public void Apply(OpenApiOperation operation, OperationFilterContext context)
            {

                var authAttributes = context.MethodInfo
                  .GetCustomAttributes(true)
                  .OfType<AuthorizeAttribute>()
                  .Distinct();

                if (authAttributes.Any())
                {

                    operation.Responses.TryAdd("401", new OpenApiResponse { Description = "Unauthorized" });
                    operation.Responses.TryAdd("403", new OpenApiResponse { Description = "Forbidden" });

                    var jwtbearerScheme = new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "bearer" }
                    };

                    operation.Security = new List<OpenApiSecurityRequirement>
                {
                    new OpenApiSecurityRequirement
                    {
                        [ jwtbearerScheme ] = new string [] { }
                    }
                };
                }
            }
        }      
        public class RemoveMethodsDocumentFilter : Swashbuckle.AspNetCore.SwaggerGen.IDocumentFilter
        {
            public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
            {
                swaggerDoc.Paths.Remove("/api/v1/Authenticate/register");

                //remark out to register a new user
                swaggerDoc.Paths.Remove("/api/v1/Authenticate/register-admin");


                //foreach (var contextApiDescription in context.ApiDescriptions)
                //{
                //    var actionDescriptor = (ControllerActionDescriptor)contextApiDescription.ActionDescriptor;

                //    if (!actionDescriptor.ControllerTypeInfo.GetCustomAttributes<OpenApiTagAttribute>().Any() &&
                //    !actionDescriptor.MethodInfo.GetCustomAttributes<OpenApiTagAttribute>().Any())
                //    {
                //        var key = "/" + contextApiDescription.RelativePath.TrimEnd('/');
                //        swaggerDoc.Paths.Remove(key);
                //   }
                //}
            }
        }
        public class SchemaSortDocumentFilter : Swashbuckle.AspNetCore.SwaggerGen.IDocumentFilter
        {
            public SchemaSortDocumentFilter()
            {
            }

            // Implements IDocumentFilter.Apply().
            public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
            {
                if (swaggerDoc == null)
                    return;

                // Re-order the schemas alphabetically.
                swaggerDoc.Components.Schemas = swaggerDoc.Components.Schemas.OrderBy(kvp => kvp.Key, StringComparer.InvariantCulture)
                    .ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
            }
        }
        public class RemoveSchemaDocumentFilter : Swashbuckle.AspNetCore.SwaggerGen.IDocumentFilter
        {
            public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
            {
                //swaggerDoc.Components.Schemas.Remove("LoginModel");

                //remark out to register a new user
                swaggerDoc.Components.Schemas.Remove("RegisterModel");
            }
        }
        public class SwaggerIdFilter : Swashbuckle.AspNetCore.SwaggerGen.ISchemaFilter
        {
            public void Apply(OpenApiSchema schema, SchemaFilterContext context)
            {
                schema.Properties.Remove("Delimited");
                schema.Properties.Remove("Wind");
                //schema.Enum.Remove("Wind");
            }
        }
        public class DateExampleParameterFilter : IParameterFilter
        {
            public void Apply(OpenApiParameter parameter, ParameterFilterContext context)
            {
                // strongly-typed example values get serialized as JSON in ISO 8601 format, with undesired precision,
                // so replace them with a string
                if (parameter.Schema.Format == "date-time" &&
                    parameter.Schema.Example is OpenApiDate date)
                    parameter.Schema.Example = new OpenApiString(date.Value.ToString("yyyy-MM-dd HH:mm:ss"));
            }
        }
        
        public class SwaggerDefaultValues : Swashbuckle.AspNetCore.SwaggerGen.IOperationFilter
        {
            public void Apply(OpenApiOperation operation, OperationFilterContext context)
            {
                var apiDescription = context.ApiDescription;
                operation.Deprecated |= apiDescription.IsDeprecated();

                if (operation.Parameters == null)
                    return;

                // REF: https://github.com/domaindrivendev/Swashbuckle.AspNetCore/issues/412
                // REF: https://github.com/domaindrivendev/Swashbuckle.AspNetCore/pull/413
                foreach (var parameter in operation.Parameters)
                {
                    var description = apiDescription.ParameterDescriptions.First(p => p.Name == parameter.Name);
                    if (parameter.Description == null)
                    {
                        parameter.Description = description.ModelMetadata?.Description;
                    }

                    if (parameter.Schema.Default == null && description.DefaultValue != null)
                    {
                        parameter.Schema.Default = new OpenApiString(description.DefaultValue.ToString());
                    }

                    parameter.Required |= description.IsRequired;
                }
            }
        }

        public class ConfigureSwaggerOptions : IConfigureOptions<SwaggerGenOptions>
        {
            private readonly IApiVersionDescriptionProvider _provider;

            public ConfigureSwaggerOptions(IApiVersionDescriptionProvider provider) => _provider = provider;

            public void Configure(SwaggerGenOptions options)
            {
                // add a swagger document for each discovered API version
                // note: you might choose to skip or document deprecated API versions differently
                foreach (var description in _provider.ApiVersionDescriptions)
                {
                    options.SwaggerDoc(description.GroupName, CreateInfoForApiVersion(description));
                }
            }

            private static OpenApiInfo CreateInfoForApiVersion(ApiVersionDescription description)
            {
                var info = new OpenApiInfo()
                {
                    Title = "Sample API",
                    Version = description.ApiVersion.ToString(),
                };

                if (description.IsDeprecated)
                {
                    info.Description += " This API version has been deprecated.";
                }

                return info;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using gc.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Swashbuckle.AspNetCore.Annotations;
using System.ComponentModel.DataAnnotations;
using Kendo.Mvc.UI;
using Microsoft.Extensions.Caching.Distributed;
using gc.Extensions;
using gc.DataAccess.DataObjects;

namespace gc.Controllers
{
    [Authorize]
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [SwaggerTag("Allows for managing sensor devices")]
    public class SensorDeviceController : Controller
    {

        private readonly ILogger<SensorDeviceController> _logger;
        //private readonly IDistributedCache _cache;
        private MySqlContext _context;

        public SensorDeviceController(MySqlContext context, ILogger<SensorDeviceController> logger)
        {
            _logger = logger;
            //_cache = cache;
            _context = context;
        }

        /// <returns>List all devices registered to this manufacturer.</returns>
        // GET: api/Device
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [SwaggerResponse(200,"Ok")]
        [SwaggerResponse(400, "Invalid Request")]
        [SwaggerResponse(401,"Not Authorizard")]
        [SwaggerResponse(404, "Sensor device not found, or data not found in the timeframe referenced.")]
        [SwaggerResponse(500, "Server error. If issue persists, contact TerraBase.")]
        [SwaggerOperation(Summary = "List all devices registered to this manufacturer.", Description = "List all devices registered to this manufacturer.")]
        public async Task<ActionResult<List<gc.Models.v1.Json.SensorDeviceShort>>> Get()
        {
            try
            {
                //retrieve manufacturerid from claim
                var claim = ((ClaimsIdentity)User.Identity).Claims.FirstOrDefault(c => c.Type == "urn:Custom:ManufacturerId");
                Int32 manufacturerId = Int32.Parse(claim.Value.ToString());

                List<gc.Models.v1.Json.SensorDeviceShort> devices = new List<gc.Models.v1.Json.SensorDeviceShort>();
                devices = await gc.Services.Aurora.GetDevices(_context, manufacturerId);
                return devices;
            }

            catch (Exception err)
            {
                _logger.LogError(err, "SensorDeviceController.Get");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
        /// <param name="id" example="device092">Unique device identifier</param> 
        /// <returns>A single device record</returns>
        // GET: api/Device
        [HttpGet("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [SwaggerResponse(200, "Ok")]
        [SwaggerResponse(400, "Invalid Request")]
        [SwaggerResponse(401, "Not Authorizard")]
        [SwaggerResponse(404, "Sensor device not found.")]
        [SwaggerResponse(500, "Server error. If issue persists, contact TerraBase.")]
        [SwaggerOperation(Summary = "Get a single device record.", Description = "Get a single device record.")]
        public async Task<ActionResult<Models.v1.Json.SensorDeviceShort>> Get([FromRoute][Required] String id)
        {
            try
            {
                //retrieve manufacturerid from claim
                var claim = ((ClaimsIdentity)User.Identity).Claims.FirstOrDefault(c => c.Type == "urn:Custom:ManufacturerId");
                Int32 manufacturerId = Int32.Parse(claim.Value.ToString());

                Models.v1.Json.SensorDeviceShort device = new Models.v1.Json.SensorDeviceShort();
                device = await gc.Services.Aurora.GetDeviceShort(_context, manufacturerId, id);
                if (device == null)
                    return StatusCode(StatusCodes.Status404NotFound, new Response { Status = "Error", Message = "Device not found." });

                //retrieve dataset
                return device;

            }
            catch (Exception err)
            {
                _logger.LogError(err, "SensorDeviceController.Get");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // POST: api/Device
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]        
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]                
        [SwaggerResponse(201, "Record created successfully")]
        [SwaggerResponse(400, "Invalid Request")]
        [SwaggerResponse(401, "Not Authorized")]
        [SwaggerResponse(500, "Server error. If issue persists, contact TerraBase.")]
        [SwaggerOperation(Summary = "Create new sensor device.",
            Description = "Create new sensor device.")]
        public async Task<IActionResult> Post([FromBody] gc.Models.v1.Json.SensorDeviceShort device)
        {
            try
            {
                //retrieve manufacturerid from claim
                var claim = ((ClaimsIdentity)User.Identity).Claims.FirstOrDefault(c => c.Type == "urn:Custom:ManufacturerId");
                Int32 manufacturerId = Int32.Parse(claim.Value.ToString());
                List<gc.Models.v1.Json.SensorDeviceShort> deviceList = await gc.Services.Aurora.GetDevices(_context, manufacturerId);
                Boolean found = false;
                foreach (gc.Models.v1.Json.SensorDeviceShort d in deviceList)
                {
                    if (device.id.Equals(d.id))
                        found = true;
                }
                if (!found)
                {
                    await gc.Services.Aurora.PostDevice(_context, manufacturerId, device.id, device.name, device.cat);
                    //await updateCache(manufacturerId);
                    return Ok(new Response { Status = "Success", Message = "Device successfully created!" });
                }
                else
                    return StatusCode(StatusCodes.Status409Conflict, new Response { Status = "Error", Message = "Device already exists." });

            }
            catch (Exception err)
            {
                _logger.LogError(err, "SensorDeviceController.Post");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }

        /// <param name="id" example="device092">Unique device identifier</param>         
        [HttpPut("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [SwaggerResponse(200, "Device updated successfully")]
        [SwaggerResponse(400, "Invalid Request")]
        [SwaggerResponse(401, "Not Authorized")]
        [SwaggerResponse(404, "Sensor device not found")]
        [SwaggerResponse(500, "Server error. If issue persists, contact TerraBase.")]
        [SwaggerOperation(Summary = "Updates a sensor device name and/or siteid.",
            Description = "Update the name or siteid field of a specified sensor device.")]
        public async Task<IActionResult> Put([FromRoute][Required] String id, [FromBody] gc.Models.v1.Json.SensorDeviceShort device)
        {
            try
            {
                //retrieve manufacturerid from claim
                var claim = ((ClaimsIdentity)User.Identity).Claims.FirstOrDefault(c => c.Type == "urn:Custom:ManufacturerId");
                Int32 manufacturerId = Int32.Parse(claim.Value.ToString());
                List<gc.Models.v1.Json.SensorDeviceShort> deviceList = await gc.Services.Aurora.GetDevices(_context, manufacturerId);
                Boolean found = false;
                foreach (gc.Models.v1.Json.SensorDeviceShort d in deviceList)
                {
                    if (id.Equals(d.id))
                        found = true;
                }
                Boolean done = false;
                if (!found)
                    return StatusCode(StatusCodes.Status404NotFound, new Response { Status = "Error", Message = "Device not found." });

                await gc.Services.Aurora.PutDevice(_context, manufacturerId, id, device.name, device.cat);
                //await updateCache(manufacturerId);

                return Ok(new Response { Status = "Success", Message = "Device updated successfully!" });
            }
            catch (Exception err)
            {
                _logger.LogError(err, "SensorDeviceController.Put");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }

        /// <param name="id" example="device092">Unique device identifier</param> 
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status406NotAcceptable)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [SwaggerResponse(200, "Record successfully deleted")]
        [SwaggerResponse(204, "Record successfully deleted")]
        [SwaggerResponse(400, "Invalid Request")]
        [SwaggerResponse(401, "Not Authorized")]
        [SwaggerResponse(404, "Sensor device not found")]
        [SwaggerResponse(406, "Sensor device has child records")]
        [SwaggerResponse(500, "Server error. If issue persists, contact TerraBase.")]
        [SwaggerOperationFilter(typeof(gc.Extensions.SwaggerExtensions.OptionalRouteParameterOperationFilter))]
        [SwaggerOperation(Summary = "Deletes a sensor device.",
            Description = "Deletes a sensor device from the system as long as it doesn't have associated data.")]
        public async Task<IActionResult> DeleteDevice([FromRoute][Required] String id)
        {
            try
            {
                //retrieve manufacturerid from claim
                var claim = ((ClaimsIdentity)User.Identity).Claims.FirstOrDefault(c => c.Type == "urn:Custom:ManufacturerId");
                Int32 manufacturerId = Int32.Parse(claim.Value.ToString());

                gc.Models.v1.Json.SensorDevice device = await gc.Services.Aurora.GetAny(_context, manufacturerId, id);
                if (device == null)
                    return StatusCode(StatusCodes.Status404NotFound, new Response { Status = "Error", Message = "Sensor device not found." });

                Boolean hasRecs = false;
                if (device.measurementData != null)
                    if (device.measurementData.Count > 0)
                        hasRecs = true;

                if (device.latlngData != null)
                    if (device.latlngData.Count > 0)
                        hasRecs = true;

                if (device.sysData != null)
                    if (device.sysData.Count > 0)
                        hasRecs = true;

                if (device.windData != null)
                    if (device.windData.Count > 0)
                        hasRecs = true;

                if (hasRecs)
                    return StatusCode(StatusCodes.Status406NotAcceptable, new Response { Status = "Error", Message = "Sensor device has child records" });

                await gc.Services.Aurora.DeleteDevice(_context, manufacturerId, id);
                //await updateCache(manufacturerId);
                return Ok(new Response { Status = "Success", Message = "Device successfully deleted." });
            }
            catch (Exception err)
            {
                _logger.LogError(err, "SensorDeviceController.Delete");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (!disposing)
            {
                _context.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}

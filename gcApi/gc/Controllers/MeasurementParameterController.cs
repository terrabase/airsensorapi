﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using gc.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Swashbuckle.AspNetCore.Annotations;
using System.ComponentModel.DataAnnotations;
using Kendo.Mvc.UI;
using Microsoft.Extensions.Caching.Distributed;
using gc.Extensions;
using gc.DataAccess.DataObjects;
using Pomelo.EntityFrameworkCore.MySql;

namespace gc.Controllers
{
    [Authorize]
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [SwaggerTag("Allows for managing list of measurement parameters")]
    public class MeasurementParameterController : Controller
    {
        private readonly ILogger<MeasurementParameterController> _logger;
        //private readonly IDistributedCache _cache;
        private MySqlContext _context;

        public MeasurementParameterController(MySqlContext context, ILogger<MeasurementParameterController> logger)
        {
            _logger = logger;
            //_cache = cache;
            _context = context;
        }

        /// <returns>List all measurement parameters.</returns>
        // GET: api/Device
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [SwaggerResponse(200, "Ok")]
        [SwaggerResponse(400, "Invalid Request")]
        [SwaggerResponse(401, "Not Authorizard")]
        [SwaggerResponse(404, "Measurement parameter not found.")]
        [SwaggerResponse(500, "Server error. If issue persists, contact TerraBase.")]
        [SwaggerOperation(Summary = "List all measurement parameters.", Description = "List all measurement parameters.")]
        public async Task<ActionResult<List<gc.Models.LovMemShadowTypes.MeasurementParameters>>> Get()
        {
            try
            {
                //retrieve manufacturerid from claim
                var claim = ((ClaimsIdentity)User.Identity).Claims.FirstOrDefault(c => c.Type == "urn:Custom:ManufacturerId");
                Int32 manufacturerId = Int32.Parse(claim.Value.ToString());

                List<gc.Models.LovMemShadowTypes.MeasurementParameters> parameterList = new List<gc.Models.LovMemShadowTypes.MeasurementParameters>();
                parameterList = await gc.Services.MeasurementParams.Get(_context, manufacturerId);
                return parameterList;

            }
            catch (Exception err)
            {
                _logger.LogError(err, "MeasurementParameterController.Get");
                return StatusCode(StatusCodes.Status500InternalServerError, err.Message);
            }
            
        }

        /// <param name="id" example="pm_2_5">Unique parameter identifier</param> 
        /// <returns>A single parameter record</returns>
        // GET: api/Param
        [HttpGet("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [SwaggerResponse(200, "Ok")]
        [SwaggerResponse(400, "Invalid Request")]
        [SwaggerResponse(401, "Not Authorizard")]
        [SwaggerResponse(404, "parameter not found.")]
        [SwaggerResponse(500, "Server error. If issue persists, contact TerraBase.")]
        [SwaggerOperation(Summary = "Get a single parameter record.", Description = "Get a single parameter record.")]
        public async Task<ActionResult<gc.Models.LovMemShadowTypes.MeasurementParameters>> Get([FromRoute][Required] String id)
        {
            try
            {
                //retrieve manufacturerid from claim
                var claim = ((ClaimsIdentity)User.Identity).Claims.FirstOrDefault(c => c.Type == "urn:Custom:ManufacturerId");
                Int32 manufacturerId = Int32.Parse(claim.Value.ToString());

                gc.Models.LovMemShadowTypes.MeasurementParameters param = new gc.Models.LovMemShadowTypes.MeasurementParameters();
                param = await gc.Services.MeasurementParams.Get(_context, manufacturerId, id);

                if (param == null)
                    return StatusCode(StatusCodes.Status404NotFound, new Response { Status = "Error", Message = "Parameter not found." });

                //retrieve dataset
                return param;
            }
            catch (Exception err)
            {
                _logger.LogError(err, "MeasurementParameterController.Get");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }
        /// <param name="id" example="device092">Unique parameter identifier</param> 
        /// <param name="name" example="Field device #92">Parameter metadata descriptor</param> 
        // POST: api/Parameter
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [SwaggerResponse(201, "Parameter successfully created.")]
        [SwaggerResponse(400, "Invalid Request")]
        [SwaggerResponse(401, "Not Authorized")]
        [SwaggerResponse(409, "Parameter ID already exists.")]
        [SwaggerResponse(500, "Server error. If issue persists, contact TerraBase.")]
        [SwaggerOperation(Summary = "Create new parameter record.",
            Description = "Create new parameter record.")]
        public async Task<IActionResult> Post([FromBody] gc.Models.LovMemShadowTypes.MeasurementParameters param)
        {
            try
            {
                //retrieve manufacturerid from claim
                var claim = ((ClaimsIdentity)User.Identity).Claims.FirstOrDefault(c => c.Type == "urn:Custom:ManufacturerId");
                Int32 manufacturerId = Int32.Parse(claim.Value.ToString());

                String username = User.Identity.Name;

                //search for record to see if it already exists
                gc.Models.LovMemShadowTypes.MeasurementParameters search = await gc.Services.MeasurementParams.Get(_context, manufacturerId, param.ParamId);
                if (search != null)
                    return StatusCode(StatusCodes.Status409Conflict, new Response { Status = "Error", Message = "Parameter ID already exists." });
                gc.Models.LovMemShadowTypes.MeasurementParameters search2 = await gc.Services.MeasurementParams.Get(_context, manufacturerId, param.ParamId);
                if (search != null)
                    return StatusCode(StatusCodes.Status409Conflict, new Response { Status = "Error", Message = "Parameter Description already exists." });

                await gc.Services.MeasurementParams.Post(_context, manufacturerId, username, param);
                //await updateCache(manufacturerId);


                return Ok(new Response { Status = "Success", Message = "Parameter successfully created." });
            }
            catch (Exception err)
            {
                _logger.LogError(err, "MeasurementParameterController.Post");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }

        /// <param name="id" example="pm_2_5">Unique parameter identifier</param> 
        /// <param name="name" example="PM 2.5">Parameter metadata descriptor</param> 
        [HttpPut("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [SwaggerResponse(200, "Parameter updated successfully")]
        [SwaggerResponse(400, "Invalid Request")]
        [SwaggerResponse(401, "Not Authorized")]
        [SwaggerResponse(404, "Parameter ID not found")]
        [SwaggerResponse(409, "Conflict")]
        [SwaggerResponse(500, "Server error. If issue persists, contact TerraBase.")]
        [SwaggerOperation(Summary = "Update a parameter.",
            Description = "Update a parameter.")]
        public async Task<IActionResult> Put([FromRoute][Required] String id, [FromBody] gc.Models.LovMemShadowTypes.MeasurementParameters param)
        {
            try
            {
                //retrieve manufacturerid from claim
                var claim = ((ClaimsIdentity)User.Identity).Claims.FirstOrDefault(c => c.Type == "urn:Custom:ManufacturerId");
                Int32 manufacturerId = Int32.Parse(claim.Value.ToString());


                gc.Models.LovMemShadowTypes.MeasurementParameters search = await gc.Services.MeasurementParams.Get(_context, manufacturerId, id);
                if (search == null)
                    return StatusCode(StatusCodes.Status404NotFound, new Response { Status = "Error", Message = "Parameter ID not found" });



                if (id != param.ParamId)
                {
                    search = await gc.Services.MeasurementParams.Get(_context, manufacturerId, param.ParamId);
                    if (search != null)
                        return StatusCode(StatusCodes.Status409Conflict, new Response { Status = "Error", Message = "New parameter ID already exists in another record." });
                }

                gc.Models.LovMemShadowTypes.MeasurementParameters search2 = await gc.Services.MeasurementParams.GetDesc(_context, manufacturerId, param.Description);
                if (search != null)
                    if (search2.ParamId != id)
                    {
                        return StatusCode(StatusCodes.Status409Conflict, new Response { Status = "Error", Message = "Parameter Description already exists." });
                    }

                await gc.Services.MeasurementParams.Put(_context, manufacturerId, id, param);
                //await updateCache(manufacturerId);

                return Ok(new Response { Status = "Success", Message = "Parameter updated successfully" });
            }
            catch (Exception err)
            {
                _logger.LogError(err, "MeasurementParameterController.Put");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <param name="id" example="pm_2_5">Unique parameter identifier</param> 
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [SwaggerResponse(200, "Parameter successfully deleted.")]
        [SwaggerResponse(204, "Parameter successfully deleted.")]
        [SwaggerResponse(400, "Invalid Request")]
        [SwaggerResponse(401, "Not Authorized")]
        [SwaggerResponse(404, "Parameter ID not found")]
        [SwaggerResponse(500, "Server error. If issue persists, contact TerraBase.")]
        [SwaggerOperationFilter(typeof(gc.Extensions.SwaggerExtensions.OptionalRouteParameterOperationFilter))]
        [SwaggerOperation(Summary = "Deletes a parameter.",
            Description = "Deletes a parameter.")]
        public async Task<IActionResult> Delete([FromRoute][Required] String id)
        {
            try
            {
                //retrieve manufacturerid from claim
                var claim = ((ClaimsIdentity)User.Identity).Claims.FirstOrDefault(c => c.Type == "urn:Custom:ManufacturerId");
                Int32 manufacturerId = Int32.Parse(claim.Value.ToString());

                gc.Models.LovMemShadowTypes.MeasurementParameters search = await gc.Services.MeasurementParams.Get(_context, manufacturerId, id);
                if (search == null)
                    return StatusCode(StatusCodes.Status404NotFound, new Response { Status = "Error", Message = "Parameter ID not found." });

                await gc.Services.MeasurementParams.Delete(_context, manufacturerId, id);
                //await updateCache(manufacturerId);

                return Ok(new Response { Status = "Success", Message = "Parameter successfully deleted." });
            }
            catch (Exception err)
            {
                _logger.LogError(err, "MeasurementParameterController.Delete");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }
        protected override void Dispose(bool disposing)
        {
            if (!disposing)
            {
                _context.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}

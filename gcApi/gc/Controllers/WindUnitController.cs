﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using gc.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Swashbuckle.AspNetCore.Annotations;
using System.ComponentModel.DataAnnotations;
using Kendo.Mvc.UI;
using Microsoft.Extensions.Caching.Distributed;
using gc.Extensions;
using gc.DataAccess.DataObjects;

namespace gc.Controllers
{
    [Authorize]
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [SwaggerTag("Allows for managing list of wind units")]
    public class WindUnitController : Controller
    {
        private readonly ILogger<WindUnitController> _logger;
        //private readonly IDistributedCache _cache;
        private MySqlContext _context;

        public WindUnitController(MySqlContext context,ILogger<WindUnitController> logger)
        {
            _logger = logger;
            //_cache = cache;
            _context = context;
        }

        /// <returns>List all wind units.</returns>
        // GET: api/Device
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [SwaggerResponse(200, "Ok")]
        [SwaggerResponse(400, "Invalid Request")]
        [SwaggerResponse(401, "Not Authorizard")]
        [SwaggerResponse(404, "Wind units not found.")]
        [SwaggerResponse(500, "Server error. If issue persists, contact TerraBase.")]
        [SwaggerOperation(Summary = "List all wind units.", Description = "List all wind units.")]
        public async Task<ActionResult<List<gc.Models.LovMemShadowTypes.WindUnits>>> Get()
        {
            try
            {
                //retrieve manufacturerid from claim
                var claim = ((ClaimsIdentity)User.Identity).Claims.FirstOrDefault(c => c.Type == "urn:Custom:ManufacturerId");
                Int32 manufacturerId = Int32.Parse(claim.Value.ToString());

                List<gc.Models.LovMemShadowTypes.WindUnits> list = new List<gc.Models.LovMemShadowTypes.WindUnits>();
                list = await gc.Services.WindUnits.Get(_context, manufacturerId);
                return list;
            }
            catch (Exception err)
            {
                _logger.LogError(err, "WindUnitController.Get");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }
        /// <param name="id" example="m/s">Unique wind unit identifier</param> 
        /// <returns>A single wind unit record</returns>
        // GET: api/WindUnit
        [HttpGet("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [SwaggerResponse(200, "Ok")]
        [SwaggerResponse(400, "Invalid Request")]
        [SwaggerResponse(401, "Not Authorizard")]
        [SwaggerResponse(404, "Wind unit not found.")]
        [SwaggerResponse(500, "Server error. If issue persists, contact TerraBase.")]
        [SwaggerOperation(Summary = "Get a single wind unit record.", Description = "Get a single wind unit record.")]
        public async Task<ActionResult<gc.Models.LovMemShadowTypes.WindUnits>> Get([FromRoute][Required] String id)
        {
            try
            {
                //retrieve manufacturerid from claim
                var claim = ((ClaimsIdentity)User.Identity).Claims.FirstOrDefault(c => c.Type == "urn:Custom:ManufacturerId");
                Int32 manufacturerId = Int32.Parse(claim.Value.ToString());

                gc.Models.LovMemShadowTypes.WindUnits item = new gc.Models.LovMemShadowTypes.WindUnits();
                item = await gc.Services.WindUnits.Get(_context, manufacturerId, id);
                if (item == null)
                    return StatusCode(StatusCodes.Status404NotFound, new Response { Status = "Error", Message = "Wind unit not found." });

                //retrieve dataset
                return item;
            }
            catch (Exception err)
            {
                _logger.LogError(err, "WindUnitController.Get");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
           
        }

        /// <param name="id" example="m/s">Unique wind unit identifier</param> 
        // POST: api/WindUnit
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [SwaggerResponse(201, "Wind unit successfully created.")]
        [SwaggerResponse(400, "Invalid Request")]
        [SwaggerResponse(401, "Not Authorized")]
        [SwaggerResponse(409, "Wind unit already exists.")]
        [SwaggerResponse(500, "Server error. If issue persists, contact TerraBase.")]
        [SwaggerOperation(Summary = "Create new wind unit record.",
            Description = "Create new wind unit record.")]
        public async Task<IActionResult> Post([FromBody] gc.Models.LovMemShadowTypes.WindUnits windUnit)
        {
            try
            {
                //retrieve manufacturerid from claim
                var claim = ((ClaimsIdentity)User.Identity).Claims.FirstOrDefault(c => c.Type == "urn:Custom:ManufacturerId");
                Int32 manufacturerId = Int32.Parse(claim.Value.ToString());

                String username = User.Identity.Name;

                //search for record to see if it already exists
                gc.Models.LovMemShadowTypes.WindUnits search = await gc.Services.WindUnits.Get(_context, manufacturerId, windUnit.Unit);
                if (search != null)
                    return StatusCode(StatusCodes.Status409Conflict, new Response { Status = "Error", Message = "Wind unit already exists." });

                await gc.Services.WindUnits.Post(_context, manufacturerId, username, windUnit);
                //await updateCache(manufacturerId);

                return Ok(new Response { Status = "Success", Message = "Wind unit successfully created." });
            }
            catch (Exception err)
            {
                _logger.LogError(err, "WindUnitController.Post");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
            
        }

        /// <param name="id" example="m/s">Wind unit identifier</param> 
        [HttpPut("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [SwaggerResponse(200, "Wind unit updated successfully")]
        [SwaggerResponse(400, "Invalid Request")]
        [SwaggerResponse(401, "Not Authorized")]
        [SwaggerResponse(404, "Wind unit not found")]
        [SwaggerResponse(409, "Conflict")]
        [SwaggerResponse(500, "Server error. If issue persists, contact TerraBase.")]
        [SwaggerOperation(Summary = "Update a wind unit.",
            Description = "Update a wind unit.")]
        public async Task<IActionResult> Put([FromRoute][Required] String id, [FromBody] gc.Models.LovMemShadowTypes.WindUnits windUnit)
        {
            try
            {
                //retrieve manufacturerid from claim
                var claim = ((ClaimsIdentity)User.Identity).Claims.FirstOrDefault(c => c.Type == "urn:Custom:ManufacturerId");
                Int32 manufacturerId = Int32.Parse(claim.Value.ToString());

                //find wind unit record
                gc.Models.LovMemShadowTypes.WindUnits search = await gc.Services.WindUnits.Get(_context, manufacturerId, id);

                //if not found, throw err
                if (search == null)
                    return StatusCode(StatusCodes.Status404NotFound, new Response { Status = "Error", Message = "Wind unit not found" });

                //else if id not equal to new id, search for new id to ensure duplicates will not occur
                else if (id != windUnit.Unit)
                {
                    search = await gc.Services.WindUnits.Get(_context, manufacturerId, windUnit.Unit);
                    if (search != null)
                        return StatusCode(StatusCodes.Status409Conflict, new Response { Status = "Error", Message = "New wind unit already exists in another record." });
                }

                //perform update
                await gc.Services.WindUnits.Put(_context, manufacturerId, id, windUnit);
                //await updateCache(manufacturerId);

                return Ok(new Response { Status = "Success", Message = "Wind unit updated successfully" });
            }
            catch (Exception err)
            {
                _logger.LogError(err, "WindUnitController.Put");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <param name="id" example="m/s">Unique wind unit identifier</param> 
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [SwaggerResponse(200, "Wind unit successfully deleted.")]
        [SwaggerResponse(204, "Wind unit successfully deleted.")]
        [SwaggerResponse(400, "Invalid Request")]
        [SwaggerResponse(401, "Not Authorized")]
        [SwaggerResponse(404, "Wind unit not found")]
        [SwaggerResponse(500, "Server error. If issue persists, contact TerraBase.")]
        [SwaggerOperationFilter(typeof(gc.Extensions.SwaggerExtensions.OptionalRouteParameterOperationFilter))]
        [SwaggerOperation(Summary = "Deletes a wind unit.",
            Description = "Deletes a wind unit.")]
        public async Task<IActionResult> Delete([FromRoute][Required] String id)
        {
            try
            {
                //retrieve manufacturerid from claim
                var claim = ((ClaimsIdentity)User.Identity).Claims.FirstOrDefault(c => c.Type == "urn:Custom:ManufacturerId");
                Int32 manufacturerId = Int32.Parse(claim.Value.ToString());

                gc.Models.LovMemShadowTypes.WindUnits search = await gc.Services.WindUnits.Get(_context, manufacturerId, id);
                if (search == null)
                    return StatusCode(StatusCodes.Status404NotFound, new Response { Status = "Error", Message = "Wind unit not found." });

                await gc.Services.WindUnits.Delete(_context, manufacturerId, id);
                //await updateCache(manufacturerId);

                return Ok(new Response { Status = "Success", Message = "Wind unit successfully deleted." });
            }
            catch (Exception err)
            {
                _logger.LogError(err, "WindUnitController.Delete");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (!disposing)
            {
                _context.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}

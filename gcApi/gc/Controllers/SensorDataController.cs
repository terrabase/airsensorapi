﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Swashbuckle.AspNetCore.Annotations;
using System.ComponentModel.DataAnnotations;
using Microsoft.Extensions.Caching.Distributed;
using gc.Extensions;
using gc.Authentication;
using gc.DataAccess.DataObjects;
using Utf8Json;

namespace gc.Controllers
{
    [Authorize]
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]    
    [SwaggerTag("Allows for reading and writing sensor data")]
    public class SensorDataController : Controller
    {

        private readonly ILogger<SensorDataController> _logger;
        private readonly IDistributedCache _cache;        
        private MySqlContext _context;

        public SensorDataController(MySqlContext context, IDistributedCache cache, ILogger<SensorDataController> logger)
        {
            _logger = logger;
            _cache = cache;
            _context = context;
        }

        //[HttpGet]
        //[Produces("application/json")]
        //public async Task<List<dynamic>> GetDevices()
        // {
        //retrieve manufacturerid from claim
        //    var claim = ((ClaimsIdentity)User.Identity).Claims.FirstOrDefault(c => c.Type == "urn:Custom:ManufacturerId");
        //    Int32 manufacturerId = Int32.Parse(claim.ToString());

        //retrieve dataset
        //    return await gc.Services.Api.GetDevices(manufacturerId);
        // }

        /// <param name="id" example="device092">Unique Device ID</param> 
        /// <param name="startDate" example="2020-12-01" required="true">Start date of data in UTC (YYYY-MM-DD)</param> 
        /// <param name="endDate" example="2020-12-02" required="true">End date of data in UTC (YYYY-MM-DD)</param> 
        /// <returns>A single device with all its associated data for the timeframe requested limited to 100 records.</returns>
        // GET: api/Gc        
        [HttpGet("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(List<gc.Models.v1.Json.SensorDevice>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [SwaggerResponse(200, "Ok")]        
        [SwaggerResponse(400, "Invalid Request")]
        [SwaggerResponse(401, "Not Authorized")]
        [SwaggerResponse(404, "Sensor Device not found, or data not found in the timeframe referenced.")]
        [SwaggerResponse(500, "Server error. If issue persists, contact TerraBase.")]
        [SwaggerOperation(Summary = "Get all data associated with a specific sensor device.", Description = "Retrieves up to 100 time-related measurement, sensor system, wind and coordinate information. No more than 1 week of data can be queried at a time.")]
        public async Task<ActionResult<List<gc.Models.v1.Json.SensorDevice>>> Get(
            [FromRoute][Required] String id,
            [FromQuery][Required] String startDate,
            [FromQuery][Required] String endDate)
        {
            try
            {
                //retrieve manufacturerid from claim
                var claim = ((ClaimsIdentity)User.Identity).Claims.FirstOrDefault(c => c.Type == "urn:Custom:ManufacturerId");
                Int32 manufacturerId = Int32.Parse(claim.Value.ToString());

                if (manufacturerId == null)
                    return BadRequest("request is incorrect");

                List<gc.Models.v1.Json.SensorDevice> devices = new List<gc.Models.v1.Json.SensorDevice>();

                //start date must be a valid date
                DateTime qDate1;
                if (DateTime.TryParse(startDate, out qDate1))
                {
                    //success
                    String strQDate1 = qDate1.ToString("yyyy-MM-dd");
                    qDate1 = DateTime.Parse(strQDate1 + " 00:00:00");

                }
                else
                {
                    //failure
                    return BadRequest("startDate is not a valid date.");
                };

                //get today in UTC at the very beginning of the day
                DateTime today = DateTime.UtcNow;
                String strToday = today.ToString("yyyy-MM-dd");
                today = DateTime.Parse(strToday + " 00:00:00");

                //start date must not be in the future
                if (qDate1 > today)
                    return BadRequest("startDate must not be in the future.");

                //endDate must be a valid date
                DateTime qDate2;
                if (DateTime.TryParse(endDate, out qDate2))
                {
                    //success
                    String strQDate2 = qDate2.ToString("yyyy-MM-dd");
                    qDate2 = DateTime.Parse(strQDate2 + " 23:59:59");
                }
                else
                {
                    //failure
                    return BadRequest("endDate is not a valid date.");
                };

                //endDate must be greater than or equal to startStart
                if (qDate2 <= qDate1)
                    return BadRequest("endDate must be greater than or equal to startDate");

                //startDate must be greater than minimum allowable date
                String strMinDate = "1970-01-01 00:00:00";
                DateTime minDate = DateTime.Parse(strMinDate);
                if (qDate1 < minDate)
                    return BadRequest("startDate is out of range");

                if (((qDate2 - qDate1).TotalDays) > 7)
                    return BadRequest("Number of days queried cannot be greater than 7.");

                //String strDate1 = qDate1.ToString("yyyy-MM-dd");
                //String strDate2 = qDate1.ToString("yyyy-MM-dd");

                //DateTime sDate = DateTime.Parse(strDate1+" 00:00:00");
                //DateTime eDate = DateTime.Parse(strDate2 + " 23:59:59");            
                DateTime sDate = qDate1;
                DateTime eDate = qDate2;

                //retrieve dataset
                devices = await gc.Services.Aurora.SensorDataGet(_context, (int)manufacturerId, id, sDate, eDate);

                if (devices is null)
                    return NotFound();
                else
                    return devices;
            }
            catch (Exception err)
            {
                _logger.LogError(err, "SensorDataController.Get");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }
                
        /// <returns>All devices for the current manufacturer along with the most recent data.</returns>
        // GET: api/Gc        
        [HttpGet("recent")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(List<gc.Models.v1.Json.SensorDevice>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [SwaggerResponse(200, "Ok")]
        [SwaggerResponse(400, "Invalid Request")]
        [SwaggerResponse(401, "Not Authorized")]
        //[SwaggerResponse(404, "Sensor Device not found, or data not found in the timeframe referenced.")]
        [SwaggerResponse(500, "Server error. If issue persists, contact TerraBase.")]
        [SwaggerOperation(Summary = "Returns most recent results for all devices.", Description = "Retrieves up to <limit> time-related analytical, system, wind and coordinate information. Results will be the most recent in each category.")]
        public async Task<ActionResult<List<gc.Models.v1.Json.SensorDevice>>> GetRecent()
        {
            try
            {
                //retrieve manufacturerid from claim
                var claim = ((ClaimsIdentity)User.Identity).Claims.FirstOrDefault(c => c.Type == "urn:Custom:ManufacturerId");
                Int32 manufacturerId = Int32.Parse(claim.Value.ToString());

                if (manufacturerId == null)
                    return BadRequest("request is incorrect");

                List<gc.Models.v1.Json.SensorDevice> devices = new List<gc.Models.v1.Json.SensorDevice>();
                

                //retrieve dataset
                devices = await gc.Services.Aurora.SensorDataGetRecent(_context, (int)manufacturerId);

                if (devices is null)
                    return NotFound();
                else
                    return devices;
            }
            catch (Exception err)
            {
                _logger.LogError(err, "SensorDataController.Get");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }


        /// <param name="data">Device list in the 'SensorDevice' schema format.</param>                
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [SwaggerResponse(201, "Record created successfully")]
        [SwaggerResponse(400, "Invalid Request")]
        [SwaggerResponse(401, "Not Authorized")]
        [SwaggerResponse(500, "Server error. If issue persists, contact TerraBase.")]
        [SwaggerOperation(Summary = "Posts new sensor device data.",
            Description = "Posts new timebased measurements, sensor system, wind and coordinate information for one or more sensor device(s). Accepts device list in the 'device' schema format. New device will be created if it doesn't already exist.")]
        public async Task<IActionResult> PostList([FromBody][SwaggerParameter(Description ="Device list and associated data",Required =true)]
            List<gc.Models.v1.Json.SensorDevice> data)
        {
            try
            {
                //retrieve manufacturerid from claim
                var claim = ((ClaimsIdentity)User.Identity).Claims.FirstOrDefault(c => c.Type == "urn:Custom:ManufacturerId");
                Int32 manufacturerId = Int32.Parse(claim.Value.ToString());                

                //String logMsg = "";
                List<String> logMsgs = new List<String>();
                if (data == null)
                {
                    String msg1 = "Either no data was submitted or the data was in an incorrect format or incorrect data types.";
                    _logger.LogInformation(msg1);                    
                    return BadRequest(msg1);
                }


                //validate parameter list
                //1. retrieve list from cache, if available
                String badParameters = await ValidateParameterList(manufacturerId, data);
                if (!String.IsNullOrEmpty(badParameters))
                {
                    String  msg2 = "The following measurement parameter(s) were not found in your valid value list: " + badParameters;
                    _logger.LogInformation(msg2);
                    logMsgs.Add(msg2);
                }

                //validate siteid
                //1. retrieve list from cache, if available
                /*String listName2 = "M_" + manufacturerId.ToString() + "_TerraBaseSites";
                List<Int32> siteIdList = await _cache.GetCacheValueAsync<List<Int32>>(listName2);
                if (siteIdList == null)
                {
                    List<gc.Models.v1.Json.TerraBaseSites> siteList = await gc.Services.Aurora.TerraBaseSitesGetAll(_context, manufacturerId);
                    siteIdList = siteList.Select(x => x.siteid).ToList();
                    await _cache.SetCacheValueAsync(listName2, siteIdList);
                }*/

                //2. report any siteid's provided that are not in the authorized list
                /*if (data.Any(x => x.siteid != null && !siteIdList.Contains((int)x.siteid)))
                {
                    String badSiteIdList = String.Join(",", data.Where(x => x.siteid != null && !siteIdList.Contains((int)x.siteid)).Select(x => x.siteid).Distinct().ToList());
                    String msg3 = "The siteid(s) '" + badSiteIdList + "' are not found in your list of authorized TerraBase sites.";
                    _logger.LogInformation(msg3);
                    logMsgs.Add(msg3);                 
                }*/

                //validate measurement data and result units lookup values
                //1. Validate measurement units
                String badMeasurementUnits = await ValidateMeasurementResultUnitsList(manufacturerId, data);
                if (!string.IsNullOrEmpty(badMeasurementUnits))
                {
                    String msg4 = "The following measurement unit(s) were not found in your valid value list: " + badMeasurementUnits;
                    _logger.LogInformation(msg4);
                    logMsgs.Add(msg4);

                }

                //ValidateMeasurementStatusCodeList
                String badMeasurementStatusCodes = await ValidateMeasurementStatusCodeList(manufacturerId, data);
                if (!string.IsNullOrEmpty(badMeasurementStatusCodes))
                {
                    String msg5 = "The following status code(s) were identified: " + badMeasurementStatusCodes;
                    _logger.LogInformation(msg5);
                    logMsgs.Add(msg5);
                }

                if (HasWind(data))
                {
                    //ValidateWindUnitsList
                    String badWindUnits = await ValidateWindUnitsList(manufacturerId, data);
                    if (!string.IsNullOrEmpty(badWindUnits))
                    {
                        String msg6 = "The following wind unit(s) were not found in your valid value list: " + badWindUnits;
                        _logger.LogInformation(msg6);
                        logMsgs.Add(msg6);
                    }

                    //ValidateWindData
                    String badWindData = await ValidateWindData(manufacturerId, data);
                    if (!string.IsNullOrEmpty(badWindData))
                    {
                        String msg7 = badWindData;
                        _logger.LogInformation(msg7);
                        logMsgs.Add(msg7);
                        //return BadRequest(badWindData);
                    }
                }


                //if errors are logged, return them all
                if (logMsgs.Count>0)
                {
                    return BadRequest(String.Join("; ", logMsgs));
                }

                //validate devices, add new one(s) if necessary
                List<SysDevice> deviceListLong = await ValidateDeviceList(manufacturerId, data);

                //post dataset                        
                await gc.Services.Aurora.SensorDataPost(_context, (int)manufacturerId, deviceListLong, data);

                return Ok(new Response { Status = "Success" });
            }
            catch (Exception err)
            {
                _logger.LogError(err,"SensorDataController.PostList");
                return StatusCode(StatusCodes.Status500InternalServerError,err.Message);
            }
 
        }
        private async Task<List<SysDevice>> RefreshSensorDeviceLongList(String cacheName, Int32 manufacturerId)
        {
            List<SysDevice> deviceListLong = await gc.Services.Aurora.GetDevicesVerbose(_context, manufacturerId);
            await _cache.SetCacheValueAsync(cacheName, deviceListLong);
            return deviceListLong;
        }

        private async Task<List<SysDevice>> ValidateDeviceList(Int32 manufacturerId, List<gc.Models.v1.Json.SensorDevice> data)
        {
            //validate parameter list
            //1. retrieve list from cache, if available
            String listName5 = "M_" + manufacturerId.ToString() + "_SensorDeviceShort";
            List<String> deviceListShort = await _cache.GetCacheValueAsync<List<String>>(listName5);
            String listName6 = "M_" + manufacturerId.ToString() + "_SensorDeviceLong";
            List<SysDevice> deviceListLong = new List<SysDevice>();

           // Boolean refreshed = false;
            if (deviceListShort == null)
            {
                if (deviceListLong==null)
                    deviceListLong = await RefreshSensorDeviceLongList(listName6, manufacturerId);
                deviceListShort = deviceListLong.Select(x => x.DeviceSerialNo).ToList();
                await _cache.SetCacheValueAsync(listName5, deviceListShort);
                //refreshed = true;
            }
            else
            {
                deviceListLong = await _cache.GetCacheValueAsync<List<SysDevice>>(listName6);
            }

            //2. unknown devices
            List<gc.Models.v1.Json.SensorDevice> badList = FindBadSensorDevices(deviceListShort, data);

            //3. add new devices & refresh caches
            if (badList.Count>0) // && !refreshed)
            {
                //refresh caches before deciding to add device as new
                deviceListLong = await RefreshSensorDeviceLongList(listName6, manufacturerId);
                deviceListShort = deviceListLong.Select(x => x.DeviceSerialNo).ToList();
                await _cache.SetCacheValueAsync(listName5, deviceListShort);

                //search a second time for the errant devices
                badList = FindBadSensorDevices(deviceListShort, data);

                //if the devices are still not found, then add
                if (badList.Count>0)
                {
                    //add device
                    await AddNewDevices(manufacturerId, badList);

                    //refresh cache
                    deviceListLong = await RefreshSensorDeviceLongList(listName6, manufacturerId);
                    deviceListShort = deviceListLong.Select(x => x.DeviceSerialNo).ToList();
                    await _cache.SetCacheValueAsync(listName5, deviceListShort);
                }

            }
            return deviceListLong;
        }
        private async Task AddNewDevices(Int32 manufacturerId, List<gc.Models.v1.Json.SensorDevice> list)
        {
            //retrieve default device category
            String defaultCat = await gc.Services.Aurora.GetDefaultDeviceCategory(_context, manufacturerId);

            //traverse devices to be added
            foreach (gc.Models.v1.Json.SensorDevice device in list)
            {
                if (String.IsNullOrEmpty(device.cat))
                    await gc.Services.Aurora.PostDevice(_context, manufacturerId, device.id, device.name, defaultCat);
                else
                    await gc.Services.Aurora.PostDevice(_context, manufacturerId, device.id, device.name, device.cat);
            }
        }
        private List<gc.Models.v1.Json.SensorDevice> FindBadSensorDevices(List<String> list, List<gc.Models.v1.Json.SensorDevice> data)
        {
            List<gc.Models.v1.Json.SensorDevice> badList = new List<gc.Models.v1.Json.SensorDevice>();
            foreach (gc.Models.v1.Json.SensorDevice device in data)
            {
                Boolean found = list.Contains(device.id, StringComparer.OrdinalIgnoreCase);
                if (!found)
                {                    
                    badList.Add(device);
                }
            }
            return badList;
        }

        private async Task<String> ValidateWindData(Int32 manufacturerId, List<gc.Models.v1.Json.SensorDevice> data)
        {            
            String errorMsg = "";
            foreach (gc.Models.v1.Json.SensorDevice device in data)
            {
                if (device.windData != null)
                {
                    if (device.windData.Any(x => (x.speed == null && x.gust == null && x.dir == null)))
                    {
                        errorMsg = "There are some wind records where speed, gust and dir are all empty. At least one of these three fields must have data to make a valid record.";

                    }
                    if (device.windData.Any(x => ((x.gust != null || x.speed != null) && String.IsNullOrEmpty(x.unit))))
                    {
                        errorMsg = "There are some wind records that have gust or speed reported, but no units.";
                    }
                }
            }
            return errorMsg;

        }
        private async Task<String> ValidateWindUnitsList(Int32 manufacturerId, List<gc.Models.v1.Json.SensorDevice> data)
        {
            //validate parameter list
            //1. retrieve list from cache, if available
            String listName1 = "M_" + manufacturerId.ToString() + "_WindUnits";
            Boolean refreshed = false;
            List<String> list = await _cache.GetCacheValueAsync<List<String>>(listName1);
            if (list == null)
            {
                list = await RefreshCacheWindUnits(listName1, manufacturerId);
                refreshed = true;
            }

            //2. Bad parameters
            String badList = FindBadWindUnits(list, data);

            //3. if bad parameters exists, refresh cache and look again
            if (!String.IsNullOrEmpty(badList) && !refreshed)
            {
                list = await RefreshCacheWindUnits(listName1, manufacturerId);
                badList = FindBadParameters(list, data);

                //4. if bad parameters still exists, send to user
                return badList;
            }

            return badList;
        }
        private async Task<String> ValidateParameterList(Int32 manufacturerId, List<gc.Models.v1.Json.SensorDevice> data)
        {
            //validate parameter list
            //1. retrieve list from cache, if available
            String listName1 = "M_" + manufacturerId.ToString() + "_MeasurementParameters";
            Boolean refreshed = false;
            List<String> list = await _cache.GetCacheValueAsync<List<String>>(listName1);
            if (list == null)
            {
                list = await RefreshCacheMeasurementParameters(listName1, manufacturerId);
                refreshed = true;
            }                

            //2. Bad parameters
            String badList = FindBadParameters(list, data);

            //3. if bad parameters exists, refresh cache and look again
            if (!String.IsNullOrEmpty(badList) && !refreshed)
            {
                list = await RefreshCacheMeasurementParameters(listName1, manufacturerId);
                badList = FindBadParameters(list, data);

                //4. if bad parameters still exists, send to user
                return badList;
            }

            return badList;            
        }
        private async Task<String> ValidateMeasurementStatusCodeList(Int32 manufacturerId, List<gc.Models.v1.Json.SensorDevice> data)
        {
            List<String> statusCodes = new List<string> { "meas", "cal" };
            String badList = "";
            foreach (gc.Models.v1.Json.SensorDevice device in data)
            {
                if (device.measurementData != null)
                {
                    if (device.measurementData.Any(x => !statusCodes.Contains(x.status, StringComparer.OrdinalIgnoreCase) && !String.IsNullOrEmpty(x.status)))
                    {
                        if (!String.IsNullOrEmpty(badList))
                            badList += ",";
                        badList+= String.Join(",", device.measurementData.Where(x => !statusCodes.Contains(x.status, StringComparer.OrdinalIgnoreCase) && !String.IsNullOrEmpty(x.status)).Select(x => x.status).Distinct().ToList());
                    }
                }
            }
            return badList;
        }
        private async Task<String> ValidateMeasurementResultUnitsList(Int32 manufacturerId, List<gc.Models.v1.Json.SensorDevice> data)
        {
            //validate list
            //1. retrieve list from cache, if available
            String listName1 = "M_" + manufacturerId.ToString() + "_MeasurementResultUnits";
            Boolean refreshed = false;
            List<String> list = await _cache.GetCacheValueAsync<List<String>>(listName1);
            if (list == null)
            {
                list = await RefreshCacheMeasurementUnits(listName1, manufacturerId);
                refreshed = true;
            }                

            //2. Bad values
            String badList = FindBadMeasurementUnits(list, data);

            //3. if bad values exists, refresh cache and look again
            if (!String.IsNullOrEmpty(badList)  && !refreshed)
            {
                list = await RefreshCacheMeasurementUnits(listName1, manufacturerId);
                badList = FindBadMeasurementUnits(list, data);

                //4. if bad values still exists, send to user
                return badList;
            }

            return badList;
        }
        private async Task<List<String>> RefreshCacheWindUnits(String cacheName, Int32 manufacturerId)
        {
            List<gc.Models.LovMemShadowTypes.WindUnits> windUnitListVerbose = await gc.Services.WindUnits.Get(_context, manufacturerId);
            List<String> windUnitList = windUnitListVerbose.Select(x => x.Unit).ToList();
            await _cache.SetCacheValueAsync(cacheName, windUnitList);
            return windUnitList;
        }
        private async Task<List<String>> RefreshCacheMeasurementParameters(String cacheName, Int32 manufacturerId)
        {
            List<gc.Models.LovMemShadowTypes.MeasurementParameters> measurementParameterListVerbose = await gc.Services.MeasurementParams.Get(_context, manufacturerId);
            List<String> measurementParameterList = measurementParameterListVerbose.Select(x => x.ParamId).ToList();
            await _cache.SetCacheValueAsync(cacheName, measurementParameterList);
            return measurementParameterList;
        }
        private async Task<List<String>> RefreshCacheMeasurementUnits(String cacheName, Int32 manufacturerId)
        {            
            List<gc.Models.LovMemShadowTypes.MeasurementResultUnits> measurementResultUnitListVerbose = await gc.Services.MeasurementResultUnits.Get(_context, manufacturerId);
            List<String> measurementResultUnitList = measurementResultUnitListVerbose.Select(x => x.Unit).ToList();
            await _cache.SetCacheValueAsync(cacheName, measurementResultUnitList);
            return measurementResultUnitList;
        }
        private String FindBadWindUnits(List<String> list, List<gc.Models.v1.Json.SensorDevice> data)
        {
            String badList = "";
            foreach (gc.Models.v1.Json.SensorDevice device in data)
            {
                if (device.windData != null)
                {
                    if (device.windData.Any(x => !list.Contains(x.unit, StringComparer.OrdinalIgnoreCase) && !String.IsNullOrEmpty(x.unit)))
                    {
                        if (!String.IsNullOrEmpty(badList))
                            badList += ",";
                        badList += String.Join(",", device.windData.Where(x => !list.Contains(x.unit, StringComparer.OrdinalIgnoreCase) && !String.IsNullOrEmpty(x.unit)).Select(x => x.unit).Distinct().ToList());
                    }
                }
            }
            return badList;
        }
        private String FindBadMeasurementUnits(List<String> list, List<gc.Models.v1.Json.SensorDevice> data)
        {
            String badList = "";
            foreach (gc.Models.v1.Json.SensorDevice device in data)
            {
                if (device.measurementData != null)
                {
                    if (device.measurementData.Any(x => !list.Contains(x.unit, StringComparer.OrdinalIgnoreCase)))
                    {
                        if (!String.IsNullOrEmpty(badList))
                            badList += ",";
                        badList += String.Join(",", device.measurementData.Where(x => !list.Contains(x.unit, StringComparer.OrdinalIgnoreCase)).Select(x => x.unit).Distinct().ToList());
                    }
                }
            }
            return badList;
        }
        private Boolean HasWind(List<gc.Models.v1.Json.SensorDevice> data)
        {
            foreach (gc.Models.v1.Json.SensorDevice device in data)
            {
                if (device.windData != null)
                    return true;
            }
            return false;
        }
        private Boolean HasLatLng(List<gc.Models.v1.Json.SensorDevice> data)
        {
            foreach (gc.Models.v1.Json.SensorDevice device in data)
            {
                if (device.latlngData != null)
                    return true;
            }
            return false;
        }
        private Boolean HasSys(List<gc.Models.v1.Json.SensorDevice> data)
        {
            foreach (gc.Models.v1.Json.SensorDevice device in data)
            {
                if (device.sysData != null)
                    return true;
            }
            return false;
        }

        private String FindBadParameters(List<String> list, List<gc.Models.v1.Json.SensorDevice> data)
        {
            String badList = "";
            foreach (gc.Models.v1.Json.SensorDevice device in data)
            {
                if (device.measurementData != null)
                {
                    if (device.measurementData.Any(x => !list.Contains(x.param, StringComparer.OrdinalIgnoreCase)))
                    {
                        if (!String.IsNullOrEmpty(badList))
                            badList += ",";
                        badList +=String.Join(",", device.measurementData.Where(x => !list.Contains(x.param, StringComparer.OrdinalIgnoreCase)).Select(x => x.param).Distinct().ToList());
                    }
                }
            }
            return badList;
        }
        protected override void Dispose(bool disposing)
        {
            if (!disposing)
            {
                _context.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using gc.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Swashbuckle.AspNetCore.Annotations;
using System.ComponentModel.DataAnnotations;
using Kendo.Mvc.UI;
using Microsoft.Extensions.Caching.Distributed;
using gc.Extensions;
using gc.DataAccess.DataObjects;
using Pomelo.EntityFrameworkCore.MySql;

namespace gc.Controllers
{
    [Authorize]
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [SwaggerTag("Allows for managing measurement parameter data")]
    public class MeasurementDataController : Controller
    {
        private readonly ILogger<MeasurementDataController> _logger;
        //private readonly IDistributedCache _cache;
        private MySqlContext _context;

        public MeasurementDataController(MySqlContext context, ILogger<MeasurementDataController> logger)
        {
            _logger = logger;
            //_cache = cache;
            _context = context;
        }
        
        /// <param name="ids" example="device092,device093">Device(s) to query</param> 
        /// <returns>A single parameter record</returns>
        // GET: api/Param
        [HttpGet("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [SwaggerResponse(200, "Ok")]
        [SwaggerResponse(400, "Invalid Request")]
        [SwaggerResponse(401, "Not Authorizard")]
        [SwaggerResponse(404, "parameter not found.")]
        [SwaggerResponse(500, "Server error. If issue persists, contact TerraBase.")]
        [SwaggerOperation(Summary = "Get a single parameter record.", Description = "Get a single parameter record.")]
        public async Task<ActionResult<gc.Models.LovMemShadowTypes.MeasurementParameters>> Get([FromRoute][Required] String id)
        {
            try
            {
                //retrieve manufacturerid from claim
                var claim = ((ClaimsIdentity)User.Identity).Claims.FirstOrDefault(c => c.Type == "urn:Custom:ManufacturerId");
                Int32 manufacturerId = Int32.Parse(claim.Value.ToString());

                gc.Models.LovMemShadowTypes.MeasurementParameters param = new gc.Models.LovMemShadowTypes.MeasurementParameters();
                param = await gc.Services.MeasurementParams.Get(_context, manufacturerId, id);

                if (param == null)
                    return StatusCode(StatusCodes.Status404NotFound, new Response { Status = "Error", Message = "Parameter not found." });

                //retrieve dataset
                return param;
            }
            catch (Exception err)
            {
                _logger.LogError(err, "MeasurementParameterController.Get");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }
        
        protected override void Dispose(bool disposing)
        {
            if (!disposing)
            {
                _context.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}

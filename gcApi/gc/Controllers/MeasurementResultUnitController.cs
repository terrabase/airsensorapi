﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using gc.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Swashbuckle.AspNetCore.Annotations;
using System.ComponentModel.DataAnnotations;
using Kendo.Mvc.UI;
using Microsoft.Extensions.Caching.Distributed;
using gc.Extensions;
using gc.DataAccess.DataObjects;


namespace gc.Controllers
{
    [Authorize]
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [SwaggerTag("Allows for managing list of measurement result units")]
    public class MeasurementResultUnitController : Controller
    {
        private readonly ILogger<MeasurementParameterController> _logger;
        //private readonly IDistributedCache _cache;
        private MySqlContext _context;

        public MeasurementResultUnitController(MySqlContext context, IDistributedCache cache, ILogger<MeasurementParameterController> logger)
        {
            _logger = logger;
            //_cache = cache;
            _context = context;
        }

        /// <returns>List all measurement result units.</returns>
        // GET: api/Device
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [SwaggerResponse(200, "Ok")]
        [SwaggerResponse(400, "Invalid Request")]
        [SwaggerResponse(401, "Not Authorizard")]
        [SwaggerResponse(404, "Measurement result units not found.")]
        [SwaggerResponse(500, "Server error. If issue persists, contact TerraBase.")]
        [SwaggerOperation(Summary = "List all measurement result units.", Description = "List all measurement result units.")]
        public async Task<ActionResult<List<gc.Models.LovMemShadowTypes.MeasurementResultUnits>>> Get()
        {
            try
            {
                //retrieve manufacturerid from claim
                var claim = ((ClaimsIdentity)User.Identity).Claims.FirstOrDefault(c => c.Type == "urn:Custom:ManufacturerId");
                Int32 manufacturerId = Int32.Parse(claim.Value.ToString());

                List<gc.Models.LovMemShadowTypes.MeasurementResultUnits> list = new List<gc.Models.LovMemShadowTypes.MeasurementResultUnits>();
                list = await gc.Services.MeasurementResultUnits.Get(_context, manufacturerId);
                return list;
            }
            catch (Exception err)
            {
                _logger.LogError(err, "MeasurementResultUnitController.Get");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
        /// <param name="id" example="ug/m3">Unique measurement result unit identifier</param> 
        /// <returns>A single measurement result unit record</returns>
        // GET: api/WindUnit
        [HttpGet("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [SwaggerResponse(200, "Ok")]
        [SwaggerResponse(400, "Invalid Request")]
        [SwaggerResponse(401, "Not Authorizard")]
        [SwaggerResponse(404, "Measurement result unit not found.")]
        [SwaggerResponse(500, "Server error. If issue persists, contact TerraBase.")]
        [SwaggerOperation(Summary = "Get a single measurement result unit record.", Description = "Get a single measurement result unit record.")]
        public async Task<ActionResult<gc.Models.LovMemShadowTypes.MeasurementResultUnits>> Get([FromRoute][Required] String id)
        {
            try
            {
                //retrieve manufacturerid from claim
                var claim = ((ClaimsIdentity)User.Identity).Claims.FirstOrDefault(c => c.Type == "urn:Custom:ManufacturerId");
                Int32 manufacturerId = Int32.Parse(claim.Value.ToString());

                gc.Models.LovMemShadowTypes.MeasurementResultUnits item = new gc.Models.LovMemShadowTypes.MeasurementResultUnits();
                item = await gc.Services.MeasurementResultUnits.Get(_context, manufacturerId, id);

                if (item == null)
                    return StatusCode(StatusCodes.Status404NotFound, new Response { Status = "Error", Message = "Measurement result unit not found." });

                //retrieve dataset
                return item;
            }
            catch (Exception err)
            {
                _logger.LogError(err, "MeasurementResultUnitController.Get");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <param name="id" example="ug/m3">Unique measurement result unit identifier</param> 
        // POST: api/MeasurementResultUnit
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [SwaggerResponse(201, "Measurement result unit successfully created.")]
        [SwaggerResponse(400, "Invalid Request")]
        [SwaggerResponse(401, "Not Authorized")]
        [SwaggerResponse(409, "Measurement result unit already exists.")]
        [SwaggerResponse(500, "Server error. If issue persists, contact TerraBase.")]
        [SwaggerOperation(Summary = "Create new measurement result unit record.",
            Description = "Create new measurement result unit record.")]
        public async Task<IActionResult> Post([FromBody] gc.Models.LovMemShadowTypes.MeasurementResultUnits measurementResultUnit)
        {
            try
            {
                //retrieve manufacturerid from claim
                var claim = ((ClaimsIdentity)User.Identity).Claims.FirstOrDefault(c => c.Type == "urn:Custom:ManufacturerId");
                Int32 manufacturerId = Int32.Parse(claim.Value.ToString());

                String username = User.Identity.Name;

                //search for record to see if it already exists
                gc.Models.LovMemShadowTypes.MeasurementResultUnits search = await gc.Services.MeasurementResultUnits.Get(_context, manufacturerId, measurementResultUnit.Unit);
                if (search != null)
                    return StatusCode(StatusCodes.Status409Conflict, new Response { Status = "Error", Message = "Measurement result unit already exists." });

                await gc.Services.MeasurementResultUnits.Post(_context, manufacturerId, username, measurementResultUnit);
                //await updateCache(manufacturerId);
                return Ok(new Response { Status = "Success", Message = "Measurement result unit successfully created." });
            }
            catch (Exception err)
            {
                _logger.LogError(err, "MeasurementResultUnitController.Post");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }

        /// <param name="id" example="ug/m3">Measurement result unit identifier</param> 
        [HttpPut("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [SwaggerResponse(200, "Measurement result unit updated successfully")]
        [SwaggerResponse(400, "Invalid Request")]
        [SwaggerResponse(401, "Not Authorized")]
        [SwaggerResponse(404, "Measurement result unit not found")]
        [SwaggerResponse(409, "Conflict")]
        [SwaggerResponse(500, "Server error. If issue persists, contact TerraBase.")]
        [SwaggerOperation(Summary = "Update an measurement result unit.",
            Description = "Update an measurement result unit.")]
        public async Task<IActionResult> Put([FromRoute][Required] String id, [FromBody] gc.Models.LovMemShadowTypes.MeasurementResultUnits measurementResultUnit)
        {
            try
            {
                //retrieve manufacturerid from claim
                var claim = ((ClaimsIdentity)User.Identity).Claims.FirstOrDefault(c => c.Type == "urn:Custom:ManufacturerId");
                Int32 manufacturerId = Int32.Parse(claim.Value.ToString());

                //if (id != param.ParamId)
                //    StatusCode(StatusCodes.Status409Conflict, new Response { Status = "Error", Message = "ID provided in URL does not match with the Parameter ID provided in the body request." });

                //find measurement result unit record
                gc.Models.LovMemShadowTypes.MeasurementResultUnits search = await gc.Services.MeasurementResultUnits.Get(_context, manufacturerId, id);

                //if not found, throw err
                if (search == null)
                    return StatusCode(StatusCodes.Status404NotFound, new Response { Status = "Error", Message = "Measurement result unit not found" });

                //else if id not equal to new id, search for new id to ensure duplicates will not occur
                else if (id != measurementResultUnit.Unit)
                {
                    search = await gc.Services.MeasurementResultUnits.Get(_context, manufacturerId, measurementResultUnit.Unit);
                    if (search != null)
                        return StatusCode(StatusCodes.Status409Conflict, new Response { Status = "Error", Message = "New measurement result unit already exists in another record." });
                }

                //perform update
                await gc.Services.MeasurementResultUnits.Put(_context, manufacturerId, id, measurementResultUnit);
                //await updateCache(manufacturerId);

                return Ok(new Response { Status = "Success", Message = "Measurement result unit updated successfully" });
            }
            catch (Exception err)
            {
                _logger.LogError(err, "MeasurementResultUnitController.Put");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <param name="id" example="ug/m3">Unique measurement result unit identifier</param> 
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [SwaggerResponse(200, "Measurement result unit successfully deleted.")]
        [SwaggerResponse(204, "Measurement result unit successfully deleted.")]
        [SwaggerResponse(400, "Invalid Request")]
        [SwaggerResponse(401, "Not Authorized")]
        [SwaggerResponse(404, "Measurement result unit not found")]
        [SwaggerResponse(500, "Server error. If issue persists, contact TerraBase.")]
        [SwaggerOperationFilter(typeof(gc.Extensions.SwaggerExtensions.OptionalRouteParameterOperationFilter))]
        [SwaggerOperation(Summary = "Deletes a measurement result unit.",
            Description = "Deletes a measurement result unit.")]
        public async Task<IActionResult> Delete([FromRoute][Required] String id)
        {
            try
            {
                //retrieve manufacturerid from claim
                var claim = ((ClaimsIdentity)User.Identity).Claims.FirstOrDefault(c => c.Type == "urn:Custom:ManufacturerId");
                Int32 manufacturerId = Int32.Parse(claim.Value.ToString());

                gc.Models.LovMemShadowTypes.MeasurementResultUnits search = await gc.Services.MeasurementResultUnits.Get(_context, manufacturerId, id);
                if (search == null)
                    return StatusCode(StatusCodes.Status404NotFound, new Response { Status = "Error", Message = "Measurement result unit not found." });

                await gc.Services.MeasurementResultUnits.Delete(_context, manufacturerId, id);
                //await updateCache(manufacturerId);

                return Ok(new Response { Status = "Success", Message = "Measurement result unit successfully deleted." });
            }
            catch (Exception err)
            {
                _logger.LogError(err, "easurementResultUnitController.Delete");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (!disposing)
            {
                _context.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Swashbuckle.AspNetCore.Annotations;
using System.ComponentModel.DataAnnotations;
using Microsoft.Extensions.Caching.Distributed;
using gc.Extensions;
using gc.Authentication;
using gc.DataAccess.DataObjects;

namespace gc.Controllers
{
    [Authorize]
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [SwaggerTag("Allows for validating sensor data instead of submitting")]
    public class SensorDataValidateController : Controller
    {

        private readonly ILogger<SensorDataController> _logger;
        private readonly IDistributedCache _cache;
        private MySqlContext _context;

        public SensorDataValidateController(MySqlContext context, IDistributedCache cache, ILogger<SensorDataController> logger)
        {
            _logger = logger;
            _cache = cache;
            _context = context;
        }


        /// <param name="data">Device list in the 'SensorDevice' schema format.</param>                
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]        
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [SwaggerResponse(201, "Record passes validation")]
        [SwaggerResponse(400, "Invalid Request")]
        [SwaggerResponse(401, "Not Authorized")]
        [SwaggerResponse(500, "Server error. If issue persists, contact TerraBase.")]
        [SwaggerOperation(Summary = "Validates sensor device data.",
            Description = "Performs validation on sensor data compatible with the /api/v1/SensorData/ method.  This method merely returns validations on said data without posting it to the database")]
        public async Task<IActionResult> VerifyList([FromBody][SwaggerParameter(Description ="Device list and associated data",Required =true)]
            List<gc.Models.v1.Json.SensorDevice> data)
        {
            try
            {
                //retrieve manufacturerid from claim
                var claim = ((ClaimsIdentity)User.Identity).Claims.FirstOrDefault(c => c.Type == "urn:Custom:ManufacturerId");
                Int32 manufacturerId = Int32.Parse(claim.Value.ToString());

                //String logMsg = "";
                List<String> logMsgs = new List<String>();
                if (data == null)
                {
                    String msg1 = "Either no data was submitted or the data was in an incorrect format or incorrect data types.";
                    _logger.LogInformation(msg1);
                    return BadRequest(msg1);
                }


                //validate parameter list
                //1. retrieve list from cache, if available
                String badParameters = await ValidateParameterList(manufacturerId, data);
                if (!String.IsNullOrEmpty(badParameters))
                {
                    String msg2 = "The following analytical parameter(s) were not found in your valid value list: " + badParameters;
                    _logger.LogInformation(msg2);
                    logMsgs.Add(msg2);
                }

                //validate siteid
                //1. retrieve list from cache, if available
                String listName2 = "M_" + manufacturerId.ToString() + "_TerraBaseSites";
                List<Int32> siteIdList = await _cache.GetCacheValueAsync<List<Int32>>(listName2);

                if (siteIdList == null)
                {
                    List<gc.Models.v1.Json.TerraBaseSites> siteList = await gc.Services.Aurora.TerraBaseSitesGetAll(_context, manufacturerId);
                    siteIdList = siteList.Select(x => x.siteid).ToList();
                    await _cache.SetCacheValueAsync(listName2, siteIdList);
                }

                //validate analytical data and result units lookup values
                //1. Validate analytical units
                String badAnalyticalUnits = await ValidateMeasurementResultUnitsList(manufacturerId, data);
                if (!string.IsNullOrEmpty(badAnalyticalUnits))
                {
                    String msg4 = "The following analytical unit(s) were not found in your valid value list: " + badAnalyticalUnits;
                    _logger.LogInformation(msg4);
                    logMsgs.Add(msg4);

                }

                //ValidateAnalyticalStatusCodeList
                String badAnalyticalStatusCodes = await ValidateAnalyticalStatusCodeList(manufacturerId, data);
                if (!string.IsNullOrEmpty(badAnalyticalStatusCodes))
                {
                    String msg5 = "The following status code(s) were identified: " + badAnalyticalStatusCodes;
                    _logger.LogInformation(msg5);
                    logMsgs.Add(msg5);
                }

                //ValidateWindUnitsList
                String badWindUnits = await ValidateWindUnitsList(manufacturerId, data);
                if (!string.IsNullOrEmpty(badAnalyticalUnits))
                {
                    String msg6 = "The following wind unit(s) were not found in your valid value list: " + badWindUnits;
                    _logger.LogInformation(msg6);
                    logMsgs.Add(msg6);
                }

                //ValidateWindData
                String badWindData = await ValidateWindData(manufacturerId, data);
                if (!string.IsNullOrEmpty(badWindData))
                {
                    String msg7 = badWindData;
                    _logger.LogInformation(msg7);
                    logMsgs.Add(msg7);
                    //return BadRequest(badWindData);
                }

                //if errors are logged, return them all
                if (logMsgs.Count > 0)
                {
                    return BadRequest(String.Join("; ", logMsgs));
                }

                //validate devices, add new one(s) if necessary
                //List<SysDevice> deviceListLong = await ValidateDeviceList(manufacturerId, data);

                return Ok(new Response { Status = "Success", Message = "Record passes validation" });
            }
            catch (Exception err)
            {
                _logger.LogError(err, "SensorDataValidateController.VerifyList");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }
        private async Task<List<SysDevice>> RefreshSensorDeviceLongList(String cacheName, Int32 manufacturerId)
        {
            List<SysDevice> deviceListLong = await gc.Services.Aurora.GetDevicesVerbose(_context, manufacturerId);
            await _cache.SetCacheValueAsync(cacheName, deviceListLong);
            return deviceListLong;
        }

        private async Task<List<SysDevice>> ValidateDeviceList(Int32 manufacturerId, List<gc.Models.v1.Json.SensorDevice> data)
        {
            //validate parameter list
            //1. retrieve list from cache, if available
            String listName5 = "M_" + manufacturerId.ToString() + "_SensorDeviceShort";
            List<String> deviceListShort = await _cache.GetCacheValueAsync<List<String>>(listName5);
            String listName6 = "M_" + manufacturerId.ToString() + "_SensorDeviceLong";
            List<SysDevice> deviceListLong = new List<SysDevice>();

            Boolean refreshed = false;
            if (deviceListShort == null)
            {
                deviceListLong = await RefreshSensorDeviceLongList(listName6, manufacturerId);
                deviceListShort = deviceListLong.Select(x => x.DeviceSerialNo).ToList();
                await _cache.SetCacheValueAsync(listName5, deviceListShort);
                refreshed = true;
            }
            else
            {
                deviceListLong = await _cache.GetCacheValueAsync<List<SysDevice>>(listName6);
            }

            //2. unknown devices
            List<gc.Models.v1.Json.SensorDevice> badList = FindBadSensorDevices(deviceListShort, data);

            //3. add new devices & refresh caches
            if (badList.Count > 0 && !refreshed)
            {
                //refresh caches
                deviceListLong = await RefreshSensorDeviceLongList(listName6, manufacturerId);
                deviceListShort = deviceListLong.Select(x => x.DeviceSerialNo).ToList();
                await _cache.SetCacheValueAsync(listName5, deviceListShort);

                //search a second time for the errant devices
                badList = FindBadSensorDevices(deviceListShort, data);

                //if the devices are still not found, then add
                if (badList.Count > 0)
                    await AddNewDevices(manufacturerId, badList);
            }
            return deviceListLong;
        }
        private async Task AddNewDevices(Int32 manufacturerId, List<gc.Models.v1.Json.SensorDevice> list)
        {
            //retrieve default device category
            String defaultCat = await gc.Services.Aurora.GetDefaultDeviceCategory(_context, manufacturerId);

            //traverse devices to be added
            foreach (gc.Models.v1.Json.SensorDevice device in list)
            {
                if (String.IsNullOrEmpty(device.cat))
                    await gc.Services.Aurora.PostDevice(_context, manufacturerId, device.id, device.name, defaultCat);
                else
                    await gc.Services.Aurora.PostDevice(_context, manufacturerId, device.id, device.name, device.cat);
            }
        }
        private List<gc.Models.v1.Json.SensorDevice> FindBadSensorDevices(List<String> list, List<gc.Models.v1.Json.SensorDevice> data)
        {
            List<gc.Models.v1.Json.SensorDevice> badList = new List<gc.Models.v1.Json.SensorDevice>();
            foreach (gc.Models.v1.Json.SensorDevice device in data)
            {
                Boolean found = list.Contains(device.id, StringComparer.OrdinalIgnoreCase);
                if (!found)
                {
                    badList.Add(device);
                }
            }
            return badList;
        }

        private async Task<String> ValidateWindData(Int32 manufacturerId, List<gc.Models.v1.Json.SensorDevice> data)
        {
            String errorMsg = "";
            foreach (gc.Models.v1.Json.SensorDevice device in data)
            {
                if (device.windData != null)
                {
                    if (device.windData.Any(x => (x.speed == null && x.gust == null && x.dir == null)))
                    {
                        errorMsg = "There are some wind records where speed, gust and dir are all empty. At least one of these three fields must have data to make a valid record.";

                    }
                    if (device.windData.Any(x => ((x.gust != null || x.speed != null) && String.IsNullOrEmpty(x.unit))))
                    {
                        errorMsg = "There are some wind records that have gust or speed reported, but no units.";
                    }
                }
            }
            return errorMsg;

        }
        private async Task<String> ValidateWindUnitsList(Int32 manufacturerId, List<gc.Models.v1.Json.SensorDevice> data)
        {
            //validate parameter list
            //1. retrieve list from cache, if available
            String listName1 = "M_" + manufacturerId.ToString() + "_WindUnits";
            Boolean refreshed = false;
            List<String> list = await _cache.GetCacheValueAsync<List<String>>(listName1);
            if (list == null)
            {
                list = await RefreshCacheWindUnits(listName1, manufacturerId);
                refreshed = true;
            }

            //2. Bad parameters
            String badList = FindBadWindUnits(list, data);

            //3. if bad parameters exists, refresh cache and look again
            if (!String.IsNullOrEmpty(badList) && !refreshed)
            {
                list = await RefreshCacheWindUnits(listName1, manufacturerId);
                badList = FindBadParameters(list, data);

                //4. if bad parameters still exists, send to user
                return badList;
            }

            return badList;
        }
        private async Task<String> ValidateParameterList(Int32 manufacturerId, List<gc.Models.v1.Json.SensorDevice> data)
        {
            //validate parameter list
            //1. retrieve list from cache, if available
            String listName1 = "M_" + manufacturerId.ToString() + "_MeasurementParameters";
            Boolean refreshed = false;
            List<String> list = await _cache.GetCacheValueAsync<List<String>>(listName1);
            if (list == null)
            {
                list = await RefreshCacheMeasurementParameters(listName1, manufacturerId);
                refreshed = true;
            }

            //2. Bad parameters
            String badList = FindBadParameters(list, data);

            //3. if bad parameters exists, refresh cache and look again
            if (!String.IsNullOrEmpty(badList) && !refreshed)
            {
                list = await RefreshCacheMeasurementParameters(listName1, manufacturerId);
                badList = FindBadParameters(list, data);

                //4. if bad parameters still exists, send to user
                return badList;
            }

            return badList;
        }
        private async Task<String> ValidateAnalyticalStatusCodeList(Int32 manufacturerId, List<gc.Models.v1.Json.SensorDevice> data)
        {
            List<String> statusCodes = new List<string> { "meas", "cal" };
            String badList = "";
            foreach (gc.Models.v1.Json.SensorDevice device in data)
            {
                if (device.measurementData != null)
                {
                    if (device.measurementData.Any(x => !statusCodes.Contains(x.status, StringComparer.OrdinalIgnoreCase) && !String.IsNullOrEmpty(x.status)))
                    {
                        if (!String.IsNullOrEmpty(badList))
                            badList += ",";
                        badList += String.Join(",", device.measurementData.Where(x => !statusCodes.Contains(x.status, StringComparer.OrdinalIgnoreCase) && !String.IsNullOrEmpty(x.status)).Select(x => x.status).Distinct().ToList());
                    }
                }
            }
            return badList;
        }
        private async Task<String> ValidateMeasurementResultUnitsList(Int32 manufacturerId, List<gc.Models.v1.Json.SensorDevice> data)
        {
            //validate list
            //1. retrieve list from cache, if available
            String listName1 = "M_" + manufacturerId.ToString() + "_MeasurementResultUnits";
            Boolean refreshed = false;
            List<String> list = await _cache.GetCacheValueAsync<List<String>>(listName1);
            if (list == null)
            {
                list = await RefreshCacheAnalyticalUnits(listName1, manufacturerId);
                refreshed = true;
            }

            //2. Bad values
            String badList = FindBadAnalyticalUnits(list, data);

            //3. if bad values exists, refresh cache and look again
            if (!String.IsNullOrEmpty(badList) && !refreshed)
            {
                list = await RefreshCacheAnalyticalUnits(listName1, manufacturerId);
                badList = FindBadAnalyticalUnits(list, data);

                //4. if bad values still exists, send to user
                return badList;
            }

            return badList;
        }
        private async Task<List<String>> RefreshCacheWindUnits(String cacheName, Int32 manufacturerId)
        {
            List<gc.Models.LovMemShadowTypes.WindUnits> windUnitListVerbose = await gc.Services.WindUnits.Get(_context, manufacturerId);
            List<String> windUnitList = windUnitListVerbose.Select(x => x.Unit).ToList();
            await _cache.SetCacheValueAsync(cacheName, windUnitList);
            return windUnitList;
        }
        private async Task<List<String>> RefreshCacheMeasurementParameters(String cacheName, Int32 manufacturerId)
        {
            List<gc.Models.LovMemShadowTypes.MeasurementParameters> analyticalParameterListVerbose = await gc.Services.MeasurementParams.Get(_context, manufacturerId);
            List<String> analyticalParameterList = analyticalParameterListVerbose.Select(x => x.ParamId).ToList();
            await _cache.SetCacheValueAsync(cacheName, analyticalParameterList);
            return analyticalParameterList;
        }
        private async Task<List<String>> RefreshCacheAnalyticalUnits(String cacheName, Int32 manufacturerId)
        {
            List<gc.Models.LovMemShadowTypes.MeasurementResultUnits> analyticalResultUnitListVerbose = await gc.Services.MeasurementResultUnits.Get(_context, manufacturerId);
            List<String> analyticalResultUnitList = analyticalResultUnitListVerbose.Select(x => x.Unit).ToList();
            await _cache.SetCacheValueAsync(cacheName, analyticalResultUnitList);
            return analyticalResultUnitList;
        }
        private String FindBadAnalyticalUnits(List<String> list, List<gc.Models.v1.Json.SensorDevice> data)
        {
            String badList = "";
            foreach (gc.Models.v1.Json.SensorDevice device in data)
            {
                if (device.windData != null)
                {
                    if (device.windData.Any(x => !list.Contains(x.unit, StringComparer.OrdinalIgnoreCase) && !String.IsNullOrEmpty(x.unit)))
                    {
                        if (!String.IsNullOrEmpty(badList))
                            badList += ",";
                        badList = String.Join(",", device.windData.Where(x => !list.Contains(x.unit, StringComparer.OrdinalIgnoreCase) && !String.IsNullOrEmpty(x.unit)).Select(x => x.unit).Distinct().ToList());
                    }
                }
            }
            return badList;
        }
        private String FindBadWindUnits(List<String> list, List<gc.Models.v1.Json.SensorDevice> data)
        {
            String badList = "";
            foreach (gc.Models.v1.Json.SensorDevice device in data)
            {
                if (device.measurementData != null)
                {
                    if (device.measurementData.Any(x => !list.Contains(x.unit, StringComparer.OrdinalIgnoreCase)))
                    {
                        if (!String.IsNullOrEmpty(badList))
                            badList += ",";
                        badList = String.Join(",", device.measurementData.Where(x => !list.Contains(x.unit, StringComparer.OrdinalIgnoreCase)).Select(x => x.unit).Distinct().ToList());
                    }
                }
            }
            return badList;
        }
        private String FindBadParameters(List<String> list, List<gc.Models.v1.Json.SensorDevice> data)
        {
            String badList = "";
            foreach (gc.Models.v1.Json.SensorDevice device in data)
            {
                if (device.measurementData != null)
                {
                    if (device.measurementData.Any(x => !list.Contains(x.param, StringComparer.OrdinalIgnoreCase)))
                    {
                        if (!String.IsNullOrEmpty(badList))
                            badList += ",";
                        badList = String.Join(",", device.measurementData.Where(x => !list.Contains(x.param, StringComparer.OrdinalIgnoreCase)).Select(x => x.param).Distinct().ToList());
                    }
                }
            }
            return badList;
        }
        protected override void Dispose(bool disposing)
        {
            if (!disposing)
            {
                _context.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}

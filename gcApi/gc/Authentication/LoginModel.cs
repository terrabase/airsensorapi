﻿using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.ComponentModel.DataAnnotations;

namespace gc.Authentication
{
    public class LoginModel
    {
        /// <summary>
        /// Username to access the system
        /// </summary>
        /// <example>myusername</example>
        [Required(ErrorMessage = "User Name is required")]
        public string Username { get; set; }

        /// <summary>
        /// Strong password to gain access
        /// </summary>
        /// <example>MyStrongPwd123(**#^</example>
        [Required(ErrorMessage = "Password is required")]
        public string Password { get; set; }
    }
    [SwaggerSchema("JWT Authentication Token and Expiry")]
    public class AuthenticationToken
    {

        /// <summary>
        /// JWT Token
        /// </summary>
        /// <example>eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb</example>
        [Required]
        public string token { get; set; }

        /// <summary>
        /// JWT Token Expiration Date/Time in UTC
        /// </summary>
        /// <example>2019-04-22T12:20:35Z</example>
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime expiration { get; set; }

    }
}

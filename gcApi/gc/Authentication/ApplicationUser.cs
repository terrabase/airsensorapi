﻿using Microsoft.AspNetCore.Identity;

namespace gc.Authentication
{
    public class ApplicationUser: IdentityUser
    {
        [PersonalData]
        public int ManufacturerId { get; set; }
    }
}

﻿using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace gc.Models
{
    public class v1
    {
        public class Json
        {                
            
            public class TerraBaseSites
            {
                /// <summary>
                /// Site ID
                /// </summary>
                /// <example>231</example>
                [SwaggerSchema("Site ID")]
                [ReadOnly(true)]
                [Required]
                [Key]
                public Int32 siteid { get; set; }

                /// <summary>
                /// TerraBase Tenant Name
                /// </summary>
                /// <example>ABC Corporation</example>
                [SwaggerSchema("TerraBase Tenant Name")]
                [ReadOnly(true)]
                [Required]
                public string tenant { get; set; }

                /// <summary>
                /// TerraBase Site Name
                /// </summary>
                /// <example>Epson Warehouse</example>
                [SwaggerSchema("TerraBase Site Name")]
                [ReadOnly(true)]
                [Required]
                public string sitename { get; set; }

            }
            [SwaggerSchema("Sensor Device")]            
            public class SensorDevice
            {
                /// <summary>
                /// Device ID
                /// </summary>
                /// <example>device001</example>
                [SwaggerSchema("Device ID")]                
                [Required]
                [Key]
                [StringLength(255, MinimumLength = 1, ErrorMessage = "Device ID length must be between 1 and 50 characters")]
                public String id { get; set; }

                /// <summary>
                /// Device Name
                /// </summary>
                /// <example>Front Parking Lot</example>
                [SwaggerSchema("Device Name")]
                [StringLength(255, ErrorMessage = "Device Name Max Length is 255")]
                public String name { get; set; }

                /// <summary>
                /// Device Category ('air','water')
                /// </summary>
                /// <example>air</example>
                [SwaggerSchema("Device Category ('air','water')")]
                [StringLength(255, ErrorMessage = "Device Category Max Length is 255")]
                public String cat { get; set; }

                [SwaggerSchema("Sensor time-based coordinates")] 
                public List<LatLngData> latlngData { get; set; }
                [SwaggerSchema("Sensor time-based measurement results")]
                public List<MeasurementData> measurementData { get; set; }
                [SwaggerSchema("Sensor time-based system information")]
                public List<SysData> sysData { get; set; }
                [SwaggerSchema("Sensor Time-based anemometer readings for air-type sensors")]
                public List<WindData> windData { get; set; }
            }
            [SwaggerSchema("Sensor Device without subtypes")]
            public class SensorDeviceShort
            {
                /// <summary>
                /// Device ID
                /// </summary>
                /// <example>device001</example>
                [SwaggerSchema("Device ID")]
                [Required]
                [Key]
                [StringLength(255, MinimumLength = 1, ErrorMessage = "Device ID length must be between 1 and 50 characters")]
                public String id { get; set; }

                /// <summary>
                /// Device Name
                /// </summary>
                /// <example>Front Parking Lot</example>
                [SwaggerSchema("Device Name")]
                [Required]
                [StringLength(255, ErrorMessage = "Description Max Length is 255")]
                public String name { get; set; }

                /// <summary>
                /// Device Category ('air','water')
                /// </summary>
                /// <example>air</example>
                [SwaggerSchema("Device Category ('air','water')")]
                [Required]
                [StringLength(255, ErrorMessage = "Device Category Max Length is 255")]
                public String cat { get; set; }

            }
            [SwaggerSchema("Sensor time-based coordinates")]
            public class LatLngData
            {
                /// <summary>
                /// Reading Date/Time in UTC
                /// </summary>
                /// <example>2019-04-22T12:20:35Z</example>
                [SwaggerSchema("Reading Date/Time in UTC")]
                [Required]
                [Key]
                [DataType(DataType.DateTime)]
                [Range(typeof(DateTimeOffset), "01/01/2000", "01/01/2060",ErrorMessage = "Valid dates for the Property {0} between {1} and {2}")]
                public DateTimeOffset ts { get; set; }

                /// <summary>
                /// Latitude
                /// </summary>
                /// <example>49.0346</example>
                [SwaggerSchema("Latitude")]
                [Range(-90.0, 90.0, ErrorMessage = "Latitude must be between -90 and 90.")]
                [Required]
                public float lat { get; set; }

                /// <summary>
                /// Longitude
                /// </summary>
                /// <example>-122.784</example>
                [SwaggerSchema("Longitude")]
                [Range(-180.0, 180.0, ErrorMessage = "Longitude must be between -180 and 180.")]
                [Required]
                public float lng { get; set; }
            }
            [SwaggerSchema("Sensor time-based measurement results")]
            public class MeasurementData
            {
                /// <summary>
                /// Reading Date/Time in UTC
                /// </summary>
                /// <example>2019-04-22T12:20:35Z</example>
                [SwaggerSchema("Reading Date/Time in UTC")]
                [Required]
                [Key]
                [DataType(DataType.DateTime)]
                [Range(typeof(DateTimeOffset), "01/01/2000", "01/01/2060", ErrorMessage = "Valid dates for the Property {0} between {1} and {2}")]
                public DateTimeOffset ts { get; set; }

                /// <summary>
                /// Parameter
                /// </summary>
                /// <example>BENZENE</example>
                [SwaggerSchema("Parameter")]
                [Required]
                [Key]
                [StringLength(50, MinimumLength = 1, ErrorMessage = "param length must be between 1 and 50 characters")]
                public String param { get; set; }

                /// <summary>
                /// Result
                /// </summary>
                /// <example>4.7</example>
                [SwaggerSchema("Result")]
                [Range(0, Double.PositiveInfinity)]
                public float? result { get; set; }

                /// <summary>
                /// Peak Time
                /// </summary>
                /// <example>231</example>                
                [SwaggerSchema("Peak Time")]
                [Range(0, float.PositiveInfinity)] 
                public float? peakTime { get; set; }

                /// <summary>
                /// Peak Height
                /// </summary>
                /// <example>23</example>
                [SwaggerSchema("Peak Height")]
                [Range(0, float.PositiveInfinity)]
                public float? peakHeight { get; set; }

                /// <summary>
                /// Result Unit
                /// </summary>
                /// <example>ppb</example>
                [SwaggerSchema("Result Unit")]
                [Required]
                [StringLength(50, MinimumLength = 1, ErrorMessage = "Description Max Length is 50")]
                public String unit { get; set; }

                /// <summary>
                /// Result Status ('meas'=Measurement 'cal'=Calibration)
                /// </summary>
                /// <example>meas</example>
                [SwaggerSchema("Result Status ('meas'=Measurement 'cal'=Calibration)")]
                [DefaultValue("meas")]
                [StringLength(5, ErrorMessage = "Result Status Max Length is 5")]
                public String status { get; set; }
            }
            [SwaggerSchema("Sensor time-based system information")]
            public class SysData
            {
                /// <summary>
                /// Reading Date/Time in UTC
                /// </summary>
                /// <example>2019-04-22T12:20:35Z</example>
                [SwaggerSchema("Reading Date/Time in UTC")]
                [Required]
                [Key]
                [DataType(DataType.DateTime)]
                [Range(typeof(DateTimeOffset), "01/01/2000", "01/01/2060", ErrorMessage = "Valid dates for the Property {0} between {1} and {2}")]
                public DateTimeOffset ts { get; set; }

                /// <summary>
                /// Microprocessor Temperature in degrees C
                /// </summary>
                /// <example>24.8</example>
                [SwaggerSchema("Microprocessor Temperature in degrees C")]
                [Range(0, Double.PositiveInfinity)]
                public float? microTempC { get; set; }

                /// <summary>
                /// Battery Voltage
                /// </summary>
                /// <example>12.1</example>
                [SwaggerSchema("Battery Voltage")]
                [Range(0, Double.PositiveInfinity)]
                public float? batteryVoltage { get; set; }

                /// <summary>
                /// Enclosure Temperature in degrees C
                /// </summary>
                /// <example>23.1</example>
                [SwaggerSchema("Enclosure Temperature in degrees C")]
                [Range(0, Double.PositiveInfinity)]
                public float? enclosureTempC { get; set; }
            }
            [SwaggerSchema("Sensor time-based anemometer readings")]
            public class WindData
            {
                /// <summary>
                /// Reading Date/Time in UTC
                /// </summary>
                /// <example>2019-04-22T12:20:35Z</example>
                [SwaggerSchema("Reading Date/Time in UTC")]
                [Required]
                [Key]
                [DataType(DataType.DateTime)]
                [Range(typeof(DateTimeOffset), "01/01/2000", "01/01/2060", ErrorMessage = "Valid dates for the Property {0} between {1} and {2}")]
                public DateTimeOffset ts { get; set; }

                /// <summary>
                /// Wind Speed
                /// </summary>
                /// <example>4.6</example>
                [SwaggerSchema("Wind Speed")]
                [Range(0, Double.PositiveInfinity)]
                public float? speed { get; set; }

                /// <summary>
                /// Wind Direction in degrees
                /// </summary>
                /// <example>8.7</example>
                [SwaggerSchema("Wind Direction in degrees")]
                [Range(0.0, 360.0, ErrorMessage = "Wind direction must be betwen 0 and 360 degrees")]
                public float? dir { get; set; }

                /// <summary>
                /// Wind Speed Unit
                /// </summary>
                /// <example>m/s</example>
                [SwaggerSchema("Wind Speed Unit ('m/s','knots','mph'")]
                [StringLength(10, ErrorMessage = "Description Max Length is 10")]
                public String unit { get; set; }


                /// <summary>
                /// Gust Speed
                /// </summary>
                /// <example>5.6</example>
                [SwaggerSchema("Gust Speed")]
                [Range(0, Double.PositiveInfinity)]
                public float? gust { get; set; }
            }
        }
        public class InternalDatDevice
        {
            public Int32 DeviceId { get; set; }
            public Int32 ManufacturerId { get; set; }
            public String DeviceSerialNo { get; set; }
            public String DeviceName { get; set; }
            public String DeviceDescription { get; set; }
            public String SensorType { get; set; }
            public DateTime? LatestDataDt { get; set; }
            public String IsEnabled { get; set; }
            public Int32? LatLngDeviceId { get; set; }
            public DateTime? DeviceAddedDt { get; set; }
            public Int32? SiteId { get; set; }
        }
        public class InternalSensorDevice : Json.SensorDevice
        {
            public Int32 deviceId { get; set; }
        }
        public class InternalWind : Json.WindData
        {
            public Int32 deviceId { get; set; }
        }
        public class InternalSys : Json.SysData
        {
            public Int32 deviceId { get; set; }
        }
        public class InternalLatLng: Json.LatLngData
        {
            public Int32 deviceId { get; set; }
        }
        public class InternalMeasurement: Json.MeasurementData
        {
            public Int32 deviceId { get; set; }
        }
        public class Delimited
        {
            [Required]
            [StringLength(255, ErrorMessage = "Description Max Length is 255")]
            public String id { get; set; }
            [StringLength(255, ErrorMessage = "Description Max Length is 255")]
            public String name { get; set; }
            [Required]
            [DataType(DataType.DateTime)]
            [Range(typeof(DateTimeOffset), "01/01/2000", "01/01/2060", ErrorMessage = "Valid dates for the Property {0} between {1} and {2}")]
            public DateTimeOffset ts { get; set; }

            /// <summary>
            /// Internal Device ID (read-only)
            /// </summary>
            /// <example>88</example>
            //[SwaggerSchema("Internal Device ID (read-only)")]
            //public Int32? internalId { get; set; }

            /// <summary>
            /// Site ID
            /// </summary>
            /// <example>231</example>
            [SwaggerSchema("Site ID")]
            [Range(1, 100000, ErrorMessage = "Site ID must be an integer value between 1 and 100000.")]
            public Int32? siteid { get; set; }

            public float? microTempC { get; set; }
            public float? batteryVoltage { get; set; }
            public float? enclosureTempC { get; set; }
            public float? lat { get; set; }
            public float? lng { get; set; }
            [StringLength(50, ErrorMessage = "Description Max Length is 50")]
            public String param { get; set; }
            public float? result { get; set; }
            public float? peakTime { get; set; }
            public float? peakHeight { get; set; }
            [StringLength(50, ErrorMessage = "Description Max Length is 50")]
            public String unit { get; set; }
            [StringLength(50, ErrorMessage = "Description Max Length is 50")]
            public String status { get; set; }
            public float? speed { get; set; }
            public float? dir { get; set; }
            [StringLength(50, ErrorMessage = "Description Max Length is 50")]
            public String windUnit { get; set; }
            public float? gust { get; set; }

        }

    }
}
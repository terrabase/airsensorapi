﻿using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace gc.Models
{
    
    public class LovMemShadowTypes
    {
        public class WindUnits
        {
            /// <summary>
            /// Unit
            /// </summary>
            /// <example>m/s</example>
            [SwaggerSchema("Unit")]
            [Required]
            [Key]
            [StringLength(50, MinimumLength = 1, ErrorMessage = "Unit Max Length is 50")]
            public String Unit { get; set; }

            /// <summary>
            /// System Defined (read-only)
            /// </summary>
            /// <example>N</example>
            [SwaggerSchema("System Defined (read-only)")]
            [DefaultValue("N")]
            [StringLength(1, MinimumLength = 1, ErrorMessage = "System Defined Max Length is 1")]
            public String SystemDefined { get; set; }
        }
        public class MeasurementResultUnits
        {
            /// <summary>
            /// Unit
            /// </summary>
            /// <example>ug/m3</example>
            [SwaggerSchema("Unit")]
            [Required]
            [Key]
            [StringLength(50, MinimumLength = 1, ErrorMessage = "Unit Max Length is 50")]
            public String Unit { get; set; }

            /// <summary>
            /// System Defined (read-only)
            /// </summary>
            /// <example>N</example>
            [SwaggerSchema("System Defined (read-only)")]
            [DefaultValue("N")]
            [StringLength(1, MinimumLength = 1, ErrorMessage = "System Defined Max Length is 1")]
            public String SystemDefined { get; set; }
        }

        public class MeasurementParameters
        {
            /// <summary>
            /// Parameter ID
            /// </summary>
            /// <example>pm_2_5</example>
            [SwaggerSchema("Parameter ID")]
            [Required]
            [Key]
            [StringLength(50, MinimumLength = 1, ErrorMessage = "Parameter ID Max Length is 50")]
            public String ParamId { get; set; }

            /// <summary>
            /// Parameter Description
            /// </summary>
            /// <example>PM 2.5</example>
            [SwaggerSchema("Parameter Description")]
            [StringLength(255, ErrorMessage = "Description Max Length is 255")]
            public string Description { get; set; }

            /// <summary>
            /// System Defined (read-only)
            /// </summary>
            /// <example>N</example>
            [SwaggerSchema("System Defined (read-only)")]
            [DefaultValue("N")]
            [StringLength(1, MinimumLength = 1, ErrorMessage = "System Defined Max Length is 1")]
            public String SystemDefined { get; set; }
        }
    }

}

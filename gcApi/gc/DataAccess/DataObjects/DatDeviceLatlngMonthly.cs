﻿using System;
using System.Collections.Generic;

#nullable disable

namespace gc.DataAccess.DataObjects
{
    public partial class DatDeviceLatlngMonthly
    {
        public short DeviceId { get; set; }
        public DateTime TimestampUtc { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public int? RecCount { get; set; }
        public DateTime? LastModifiedUtcDt { get; set; }
    }
}

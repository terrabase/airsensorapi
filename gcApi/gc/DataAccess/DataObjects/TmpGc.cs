﻿using System;
using System.Collections.Generic;

#nullable disable

namespace gc.DataAccess.DataObjects
{
    public partial class TmpGc
    {
        public long Id { get; set; }
        public int? DeviceId { get; set; }
        public DateTime? Ts { get; set; }
        public string Param { get; set; }
        public float? Result { get; set; }
        public float? PeakTime { get; set; }
        public float? PeakHeight { get; set; }
        public string Unit { get; set; }
        public string Status { get; set; }
    }
}

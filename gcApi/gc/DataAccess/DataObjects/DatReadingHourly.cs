﻿using System;
using System.Collections.Generic;

#nullable disable

namespace gc.DataAccess.DataObjects
{
    public partial class DatReadingHourly
    {
        public int DeviceId { get; set; }
        public DateTime TimestampUtc { get; set; }
        public DateTime TimestampLocal { get; set; }
        public float? Temp { get; set; }
        public float? Humidity { get; set; }
        public float? Dewpoint { get; set; }
        public float? Pressure { get; set; }
        public float? O3 { get; set; }
        public float? Pm1 { get; set; }
        public float? Pm25 { get; set; }
        public float? Pm10 { get; set; }
        public float? Co { get; set; }
        public float? Co2 { get; set; }
        public float? No { get; set; }
        public float? No2 { get; set; }
        public float? Nox { get; set; }
        public float? So2 { get; set; }
        public float? TotalVoc { get; set; }
        public float? H2s { get; set; }
        public float? TempRollavg { get; set; }
        public float? HumidityRollavg { get; set; }
        public float? DewpointRollavg { get; set; }
        public float? PressureRollavg { get; set; }
        public float? O3Rollavg { get; set; }
        public float? Pm1Rollavg { get; set; }
        public float? Pm25Rollavg { get; set; }
        public float? Pm10Rollavg { get; set; }
        public float? CoRollavg { get; set; }
        public float? Co2Rollavg { get; set; }
        public float? NoRollavg { get; set; }
        public float? No2Rollavg { get; set; }
        public float? NoxRollavg { get; set; }
        public float? So2Rollavg { get; set; }
        public float? TotalVocRollavg { get; set; }
        public float? TempDelta { get; set; }
        public float? HumidityDelta { get; set; }
        public float? DewpointDelta { get; set; }
        public float? PressureDelta { get; set; }
        public float? O3Delta { get; set; }
        public float? Pm1Delta { get; set; }
        public float? Pm25Delta { get; set; }
        public float? Pm10Delta { get; set; }
        public float? CoDelta { get; set; }
        public float? Co2Delta { get; set; }
        public float? NoDelta { get; set; }
        public float? No2Delta { get; set; }
        public float? NoxDelta { get; set; }
        public float? So2Delta { get; set; }
        public float? TotalVocDelta { get; set; }
        public short? TempScore { get; set; }
        public short? HumidityScore { get; set; }
        public short? DewpointScore { get; set; }
        public short? PressureScore { get; set; }
        public short? O3Score { get; set; }
        public short? Pm1Score { get; set; }
        public short? Pm25Score { get; set; }
        public short? Pm10Score { get; set; }
        public short? CoScore { get; set; }
        public short? Co2Score { get; set; }
        public short? NoScore { get; set; }
        public short? No2Score { get; set; }
        public short? NoxScore { get; set; }
        public short? So2Score { get; set; }
        public short? TotalVocScore { get; set; }
        public DateTime? LastModifiedDt { get; set; }
        public int? RecCount { get; set; }
        public float? Wd { get; set; }
        public DateTime? RollingAvgDt { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

#nullable disable

namespace gc.DataAccess.DataObjects
{
    public partial class TmpLatlng
    {
        public long Id { get; set; }
        public int? DeviceId { get; set; }
        public DateTime? Ts { get; set; }
        public float? Lat { get; set; }
        public float? Lng { get; set; }
    }
}

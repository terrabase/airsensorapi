﻿using System;
using System.Collections.Generic;

#nullable disable

namespace gc.DataAccess.DataObjects
{
    public partial class SysSetting
    {
        public string SettingName { get; set; }
        public string SettingValue { get; set; }
    }
}

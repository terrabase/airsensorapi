﻿using System;
using System.Collections.Generic;

#nullable disable

namespace gc.DataAccess.DataObjects
{
    public partial class DatWindOld
    {
        public long Id { get; set; }
        public int DeviceId { get; set; }
        public DateTime TimestampUtc { get; set; }
        public float? WindSpeed { get; set; }
        public float? WindDirection { get; set; }
        public string WindUnit { get; set; }
        public float? Gust { get; set; }
        public long? SourceFileId { get; set; }
        public DateTime? LastModifiedUtcDt { get; set; }
    }
}

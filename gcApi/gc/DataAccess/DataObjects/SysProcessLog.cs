﻿using System;
using System.Collections.Generic;

#nullable disable

namespace gc.DataAccess.DataObjects
{
    public partial class SysProcessLog
    {
        public long RecId { get; set; }
        public int RecCount { get; set; }
        public int RecImported { get; set; }
        public int RecErred { get; set; }
        public string Message { get; set; }
        public DateTime Ts { get; set; }
    }
}

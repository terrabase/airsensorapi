﻿using System;
using System.Collections.Generic;

#nullable disable

namespace gc.DataAccess.DataObjects
{
    public partial class AutDynamodbEcomesure3
    {
        public int SensorId { get; set; }
        public string Date { get; set; }
        public string Co { get; set; }
        public string Co2 { get; set; }
        public string Pmtot { get; set; }
        public string Pressure { get; set; }
        public string Rh { get; set; }
        public string Temp { get; set; }
        public string Voc { get; set; }
        public string No2 { get; set; }
        public string O3 { get; set; }
        public string Pm10 { get; set; }
        public string Pm25 { get; set; }
        public string Vocs { get; set; }
        public string Pm1 { get; set; }
        public string Cov { get; set; }
        public string Mainsupply { get; set; }
        public string No { get; set; }
        public string H2s { get; set; }
        public string Wd { get; set; }
        public string So2 { get; set; }
        public string WindDirection { get; set; }
    }
}

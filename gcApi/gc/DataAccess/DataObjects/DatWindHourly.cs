﻿using System;
using System.Collections.Generic;

#nullable disable

namespace gc.DataAccess.DataObjects
{
    public partial class DatWindHourly
    {
        public int DeviceId { get; set; }
        public DateTime TimestampUtc { get; set; }
        public float? AvgWindSpeed { get; set; }
        public float? AvgWindDirection { get; set; }
        public string WindUnit { get; set; }
        public float? AvgGust { get; set; }
        public int? RecCount { get; set; }
        public float? MinWindSpeed { get; set; }
        public float? MinWindDirection { get; set; }
        public float? MinGust { get; set; }
        public float? MaxWindSpeed { get; set; }
        public float? MaxWindDirection { get; set; }
        public float? MaxGust { get; set; }
        public DateTime? LastModifiedUtcDt { get; set; }
    }
}

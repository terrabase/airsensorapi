﻿using System;
using System.Collections.Generic;

#nullable disable

namespace gc.DataAccess.DataObjects
{
    public partial class AutRawEcomesureErr
    {
        public long RecId { get; set; }
        public string Message { get; set; }
        public DateTime Ts { get; set; }
    }
}

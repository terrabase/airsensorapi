﻿using System;
using System.Collections.Generic;

#nullable disable

namespace gc.DataAccess.DataObjects
{
    public partial class DatGcOld
    {
        public long Id { get; set; }
        public int DeviceId { get; set; }
        public DateTime TimestampUtc { get; set; }
        public string ParamId { get; set; }
        public float? Result { get; set; }
        public float? PeakTime { get; set; }
        public float? PeakHeight { get; set; }
        public string Unit { get; set; }
        public long? SourceFileId { get; set; }
        public string Status { get; set; }
        public DateTime? LastModifiedUtcDt { get; set; }
    }
}

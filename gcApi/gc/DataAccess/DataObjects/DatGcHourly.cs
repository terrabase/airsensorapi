﻿using System;
using System.Collections.Generic;

#nullable disable

namespace gc.DataAccess.DataObjects
{
    public partial class DatGcHourly
    {
        public int DeviceId { get; set; }
        public DateTime TimestampUtc { get; set; }
        public string ParamId { get; set; }
        public float? AvgResult { get; set; }
        public float? MinResult { get; set; }
        public float? MaxResult { get; set; }
        public string Unit { get; set; }
        public float? RollingAvg1 { get; set; }
        public sbyte? RollingAvg1Hrs { get; set; }
        public float? RollingAvg2 { get; set; }
        public sbyte? RollingAvg2Hrs { get; set; }
        public short? RecCount { get; set; }
        public DateTime? LastModifiedUtcDt { get; set; }
    }
}

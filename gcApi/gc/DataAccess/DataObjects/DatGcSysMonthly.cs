﻿using System;
using System.Collections.Generic;

#nullable disable

namespace gc.DataAccess.DataObjects
{
    public partial class DatGcSysMonthly
    {
        public int DeviceId { get; set; }
        public DateTime TimestampUtc { get; set; }
        public float? AvgMicroTempC { get; set; }
        public float? AvgBatteryVoltage { get; set; }
        public float? AvgEnclosureTemp { get; set; }
        public float? MinMicroTempC { get; set; }
        public float? MinBatteryVoltage { get; set; }
        public float? MinEnclosureTemp { get; set; }
        public float? MaxMicroTempC { get; set; }
        public float? MaxBatteryVoltage { get; set; }
        public float? MaxEnclosureTemp { get; set; }
        public int? RecCount { get; set; }
        public DateTime? LastModifiedUtcDt { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

#nullable disable

namespace gc.DataAccess.DataObjects
{
    public partial class DatLatestDeviceGc
    {
        public int DeviceId { get; set; }
        public string ParamId { get; set; }
        public DateTime TimestampUtc { get; set; }
        public float? Result { get; set; }
        public string Unit { get; set; }
        public float? RollingAvg1 { get; set; }
        public sbyte? RollingAvg1Hrs { get; set; }
        public float? RollingAvg2 { get; set; }
        public sbyte? RollingAvg2Hrs { get; set; }
        public DateTime? RollingAvgDt { get; set; }
        public DateTime? DeviceLastTransmittedDt { get; set; }
    }
}

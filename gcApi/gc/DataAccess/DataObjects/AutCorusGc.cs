﻿using System;
using System.Collections.Generic;

#nullable disable

namespace gc.DataAccess.DataObjects
{
    public partial class AutCorusGc
    {
        public long Id { get; set; }
        public string FileContents { get; set; }
        public DateTime AwsLastModifiedDt { get; set; }
        public DateTime LoggedDt { get; set; }
        public short Status { get; set; }
        public string FileKey { get; set; }
    }
}

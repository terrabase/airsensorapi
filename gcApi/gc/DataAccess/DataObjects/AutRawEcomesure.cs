﻿using System;
using System.Collections.Generic;

#nullable disable

namespace gc.DataAccess.DataObjects
{
    public partial class AutRawEcomesure
    {
        public string Serial { get; set; }
        public string TimestampUtc { get; set; }
        public string TimestampLocal { get; set; }
        public string Temp { get; set; }
        public string Humidity { get; set; }
        public string Dewpoint { get; set; }
        public string Pressure { get; set; }
        public string O3 { get; set; }
        public string Pm1 { get; set; }
        public string Pm25 { get; set; }
        public string Pm10 { get; set; }
        public string Co { get; set; }
        public string Co2 { get; set; }
        public string No { get; set; }
        public string No2 { get; set; }
        public string Nox { get; set; }
        public string So2 { get; set; }
        public string Tvoc { get; set; }
        public string Hcho { get; set; }
        public long RecId { get; set; }
        public DateTime Ts { get; set; }
        public string Cov { get; set; }
        public string Comm { get; set; }
        public string Mainsupply { get; set; }
        public string H2s { get; set; }
        public string Wd { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

#nullable disable

namespace gc.DataAccess.DataObjects
{
    public partial class SysManufacturerParam
    {
        public int ManufacturerId { get; set; }
        public int ParameterId { get; set; }
        public string ParameterUnit { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

#nullable disable

namespace gc.DataAccess.DataObjects
{
    public partial class DatDeviceParameter
    {
        public int DeviceId { get; set; }
        public string ParamId { get; set; }
    }
}

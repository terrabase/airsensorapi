﻿using System;
using System.Collections.Generic;

#nullable disable

namespace gc.DataAccess.DataObjects
{
    public partial class Automation
    {
        public string Serial { get; set; }
        public DateTime? TimestampUtc { get; set; }
        public DateTime? TimestampLocal { get; set; }
        public float? Temp { get; set; }
        public float? Humidity { get; set; }
        public float? Dewpoint { get; set; }
        public float? Pressure { get; set; }
        public float? O3 { get; set; }
        public float? Pm1 { get; set; }
        public float? Pm25 { get; set; }
        public float? Pm10 { get; set; }
        public float? Co { get; set; }
        public float? Co2 { get; set; }
        public float? No { get; set; }
        public float? No2 { get; set; }
        public float? Nox { get; set; }
        public float? So2 { get; set; }
        public float? Tvoc { get; set; }
        public float? Hcho { get; set; }
    }
}

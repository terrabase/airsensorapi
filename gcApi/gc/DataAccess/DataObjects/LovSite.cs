﻿using System;
using System.Collections.Generic;

#nullable disable

namespace gc.DataAccess.DataObjects
{
    public partial class LovSite
    {
        public int LovSiteId { get; set; }
        public int TenantId { get; set; }
        public string TenantName { get; set; }
        public int SiteId { get; set; }
        public string SiteName { get; set; }
        public DateTime CreatedDt { get; set; }
        public string CreatedBy { get; set; }
    }
}

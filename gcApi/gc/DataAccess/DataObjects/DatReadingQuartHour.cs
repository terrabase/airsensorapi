﻿using System;
using System.Collections.Generic;

#nullable disable

namespace gc.DataAccess.DataObjects
{
    public partial class DatReadingQuartHour
    {
        public int DeviceId { get; set; }
        public DateTime TimestampUtc { get; set; }
        public DateTime TimestampLocal { get; set; }
        public float? Temp { get; set; }
        public float? Humidity { get; set; }
        public float? Dewpoint { get; set; }
        public float? Pressure { get; set; }
        public float? O3 { get; set; }
        public float? Pm1 { get; set; }
        public float? Pm25 { get; set; }
        public float? Pm10 { get; set; }
        public float? Co { get; set; }
        public float? Co2 { get; set; }
        public float? No { get; set; }
        public float? No2 { get; set; }
        public float? Nox { get; set; }
        public float? So2 { get; set; }
        public float? TotalVoc { get; set; }
        public float? H2s { get; set; }
        public float? Wd { get; set; }
        public float? TempRollavg { get; set; }
        public float? HumidityRollavg { get; set; }
        public float? DewpointRollavg { get; set; }
        public float? PressureRollavg { get; set; }
        public float? O3Rollavg { get; set; }
        public float? Pm1Rollavg { get; set; }
        public float? Pm25Rollavg { get; set; }
        public float? Pm10Rollavg { get; set; }
        public float? CoRollavg { get; set; }
        public float? Co2Rollavg { get; set; }
        public float? NoRollavg { get; set; }
        public float? No2Rollavg { get; set; }
        public float? NoxRollavg { get; set; }
        public float? So2Rollavg { get; set; }
        public float? TotalVocRollavg { get; set; }
        public float? H2sRollavg { get; set; }
        public DateTime? LastModifiedDt { get; set; }
        public int? RecCount { get; set; }
        public float? TempMin { get; set; }
        public float? TempMax { get; set; }
        public float? HumidityMin { get; set; }
        public float? HumidityMax { get; set; }
        public float? DewpointMin { get; set; }
        public float? DewpointMax { get; set; }
        public float? PressureMin { get; set; }
        public float? PressureMax { get; set; }
        public float? O3Min { get; set; }
        public float? O3Max { get; set; }
        public float? Pm1Min { get; set; }
        public float? Pm1Max { get; set; }
        public float? Pm25Min { get; set; }
        public float? Pm25Max { get; set; }
        public float? Pm10Min { get; set; }
        public float? Pm10Max { get; set; }
        public float? CoMin { get; set; }
        public float? CoMax { get; set; }
        public float? NoMin { get; set; }
        public float? NoMax { get; set; }
        public float? No2Min { get; set; }
        public float? No2Max { get; set; }
        public float? NoxMin { get; set; }
        public float? NoxMax { get; set; }
        public float? So2Min { get; set; }
        public float? So2Max { get; set; }
        public float? TotalVocMin { get; set; }
        public float? TotalVocMax { get; set; }
        public float? H2sMin { get; set; }
        public float? H2sMax { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

#nullable disable

namespace gc.DataAccess.DataObjects
{
    public partial class DatCustomOdorComplaint
    {
        public long Id { get; set; }
        public int TenantId { get; set; }
        public DateTime ComplaintDate { get; set; }
        public string ComplaintTime { get; set; }
        public string City { get; set; }
        public string SoWorkcenter { get; set; }
        public string SoNumber { get; set; }
        public float? Latitude { get; set; }
        public float? Longitude { get; set; }
    }
}

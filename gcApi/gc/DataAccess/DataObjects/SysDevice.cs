﻿using System;
using System.Collections.Generic;

#nullable disable

namespace gc.DataAccess.DataObjects
{
    public partial class SysDevice
    {
        public int DeviceId { get; set; }
        public int ManufacturerId { get; set; }
        public string DeviceSerialNo { get; set; }
        public string DeviceName { get; set; }
        public string DeviceDescription { get; set; }
        public string SensorType { get; set; }
        public DateTime? LatestDataDt { get; set; }
        public string IsEnabled { get; set; }
        public int? LatLngDeviceId { get; set; }
        public DateTime? DeviceAddedDt { get; set; }
        public int? SiteId { get; set; }
        public string DeviceCategory { get; set; }
    }
}

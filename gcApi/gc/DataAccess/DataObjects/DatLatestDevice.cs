﻿using System;
using System.Collections.Generic;

#nullable disable

namespace gc.DataAccess.DataObjects
{
    public partial class DatLatestDevice
    {
        public int DeviceId { get; set; }
        public DateTime? LastModifiedUtc { get; set; }
        public float? Latitude { get; set; }
        public float? Longitude { get; set; }
        public DateTime? LatLngModifiedUtc { get; set; }
        public float? MicroTempC { get; set; }
        public float? BatteryVoltage { get; set; }
        public float? EnclosureTemp { get; set; }
        public DateTime? SysModifiedUtc { get; set; }
        public float? WindSpeed { get; set; }
        public float? WindDirection { get; set; }
        public string WindUnit { get; set; }
        public float? Gust { get; set; }
        public DateTime? WindModifiedUtc { get; set; }
        public DateTime? DeviceLastTransmittedDt { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

#nullable disable

namespace gc.DataAccess.DataObjects
{
    public partial class TmpGcSys2
    {
        public long Id { get; set; }
        public int? DeviceId { get; set; }
        public DateTime? Ts { get; set; }
        public float? MicroTempC { get; set; }
        public float? BatteryVoltage { get; set; }
        public float? EnclosureTempC { get; set; }
    }
}

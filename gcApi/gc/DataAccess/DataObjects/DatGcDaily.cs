﻿using System;
using System.Collections.Generic;

#nullable disable

namespace gc.DataAccess.DataObjects
{
    public partial class DatGcDaily
    {
        public int DeviceId { get; set; }
        public DateTime TimestampUtc { get; set; }
        public string ParamId { get; set; }
        public float? AvgResult { get; set; }
        public float? MinResult { get; set; }
        public float? MaxResult { get; set; }
        public string Unit { get; set; }
        public int? RecCount { get; set; }
        public DateTime? LastModifiedUtcDt { get; set; }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace gc.DataAccess.DataObjects
{
    public partial class MySqlContext : DbContext
    {
        public MySqlContext()
        {
        }

        public MySqlContext(DbContextOptions<MySqlContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AutCorusGc> AutCorusGcs { get; set; }
        public virtual DbSet<AutDynamodbEcomesure2> AutDynamodbEcomesure2s { get; set; }
        public virtual DbSet<AutDynamodbEcomesure2Err> AutDynamodbEcomesure2Errs { get; set; }
        public virtual DbSet<AutDynamodbEcomesure3> AutDynamodbEcomesure3s { get; set; }
        public virtual DbSet<AutEnmet> AutEnmets { get; set; }
        public virtual DbSet<AutRawEcomesure> AutRawEcomesures { get; set; }
        public virtual DbSet<AutRawEcomesureErr> AutRawEcomesureErrs { get; set; }
        public virtual DbSet<AutTmpDatGc> AutTmpDatGcs { get; set; }
        public virtual DbSet<Automation> Automations { get; set; }
        public virtual DbSet<DatCustomOdorComplaint> DatCustomOdorComplaints { get; set; }
        public virtual DbSet<DatDeviceLatlng> DatDeviceLatlngs { get; set; }
        public virtual DbSet<DatDeviceLatlngDaily> DatDeviceLatlngDailies { get; set; }
        public virtual DbSet<DatDeviceLatlngHourly> DatDeviceLatlngHourlies { get; set; }
        public virtual DbSet<DatDeviceLatlngMonthly> DatDeviceLatlngMonthlies { get; set; }
        public virtual DbSet<DatDeviceLatlngOld> DatDeviceLatlngOlds { get; set; }
        public virtual DbSet<DatDeviceParameter> DatDeviceParameters { get; set; }
        public virtual DbSet<DatGc> DatGcs { get; set; }
        public virtual DbSet<DatGcDaily> DatGcDailies { get; set; }
        public virtual DbSet<DatGcHourly> DatGcHourlies { get; set; }
        public virtual DbSet<DatGcMonthly> DatGcMonthlies { get; set; }
        public virtual DbSet<DatGcOld> DatGcOlds { get; set; }
        public virtual DbSet<DatGcSy> DatGcSys { get; set; }
        public virtual DbSet<DatGcSysDaily> DatGcSysDailies { get; set; }
        public virtual DbSet<DatGcSysHourly> DatGcSysHourlies { get; set; }
        public virtual DbSet<DatGcSysMonthly> DatGcSysMonthlies { get; set; }
        public virtual DbSet<DatGcSysOld> DatGcSysOlds { get; set; }
        public virtual DbSet<DatLatestDevice> DatLatestDevices { get; set; }
        public virtual DbSet<DatLatestDeviceGc> DatLatestDeviceGcs { get; set; }
        public virtual DbSet<DatReading> DatReadings { get; set; }
        public virtual DbSet<DatReadingDaily> DatReadingDailies { get; set; }
        public virtual DbSet<DatReadingHourly> DatReadingHourlies { get; set; }
        public virtual DbSet<DatReadingQuartHour> DatReadingQuartHours { get; set; }
        public virtual DbSet<DatWind> DatWinds { get; set; }
        public virtual DbSet<DatWindDaily> DatWindDailies { get; set; }
        public virtual DbSet<DatWindHourly> DatWindHourlies { get; set; }
        public virtual DbSet<DatWindMonthly> DatWindMonthlies { get; set; }
        public virtual DbSet<DatWindOld> DatWindOlds { get; set; }
        public virtual DbSet<LovMem> LovMems { get; set; }
        public virtual DbSet<LovSite> LovSites { get; set; }
        public virtual DbSet<SysDevice> SysDevices { get; set; }
        public virtual DbSet<SysDeviceLatlngSubstitute> SysDeviceLatlngSubstitutes { get; set; }
        public virtual DbSet<SysManufacturer> SysManufacturers { get; set; }
        public virtual DbSet<SysManufacturerParam> SysManufacturerParams { get; set; }
        public virtual DbSet<SysParameter> SysParameters { get; set; }
        public virtual DbSet<SysProcessLog> SysProcessLogs { get; set; }
        public virtual DbSet<SysSetting> SysSettings { get; set; }
        public virtual DbSet<SysTenantAuth> SysTenantAuths { get; set; }
        public virtual DbSet<TmpGc> TmpGcs { get; set; }
        public virtual DbSet<TmpGc2> TmpGc2s { get; set; }
        public virtual DbSet<TmpGcSy> TmpGcSys { get; set; }
        public virtual DbSet<TmpGcSys2> TmpGcSys2s { get; set; }
        public virtual DbSet<TmpLatlng> TmpLatlngs { get; set; }
        public virtual DbSet<TmpLatlng2> TmpLatlng2s { get; set; }
        public virtual DbSet<TmpWind> TmpWinds { get; set; }
        public virtual DbSet<TmpWind2> TmpWind2s { get; set; }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //if (!optionsBuilder.IsConfigured)
            //{
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
            //    optionsBuilder.UseMySql(configuration.GetConnectionString(), Microsoft.EntityFrameworkCore.ServerVersion.Parse("5.7.12-mysql"));
            //}
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            modelBuilder.Entity<AutCorusGc>(entity =>
            {
                entity.ToTable("aut_corus_gc");

                entity.Property(e => e.Id).HasColumnType("bigint(20)");

                entity.Property(e => e.AwsLastModifiedDt).HasColumnType("datetime");

                entity.Property(e => e.FileContents)
                    .IsRequired()
                    .HasColumnType("json");

                entity.Property(e => e.FileKey)
                    .IsRequired()
                    .HasMaxLength(255)
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.LoggedDt).HasColumnType("datetime");

                entity.Property(e => e.Status).HasColumnType("smallint(6)");
            });

            modelBuilder.Entity<AutDynamodbEcomesure2>(entity =>
            {
                entity.ToTable("aut_dynamodb_ecomesure_2");

                entity.Property(e => e.Id)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("id");

                entity.Property(e => e.Co)
                    .HasMaxLength(50)
                    .HasColumnName("CO")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.Co2)
                    .HasMaxLength(50)
                    .HasColumnName("CO2")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.Communication).HasMaxLength(50);

                entity.Property(e => e.Cov)
                    .HasMaxLength(50)
                    .HasColumnName("COV");

                entity.Property(e => e.Date)
                    .IsRequired()
                    .HasMaxLength(50)
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.H2s)
                    .HasMaxLength(50)
                    .HasColumnName("H2S");

                entity.Property(e => e.Mainsupply).HasMaxLength(50);

                entity.Property(e => e.No)
                    .HasMaxLength(50)
                    .HasColumnName("NO");

                entity.Property(e => e.No2)
                    .HasMaxLength(50)
                    .HasColumnName("NO2");

                entity.Property(e => e.O3).HasMaxLength(50);

                entity.Property(e => e.Pm1)
                    .HasMaxLength(50)
                    .HasColumnName("PM1");

                entity.Property(e => e.Pm10)
                    .HasMaxLength(50)
                    .HasColumnName("PM10");

                entity.Property(e => e.Pm25)
                    .HasMaxLength(50)
                    .HasColumnName("PM25");

                entity.Property(e => e.Pmtot)
                    .HasMaxLength(50)
                    .HasColumnName("PMtot")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.Pressure)
                    .HasMaxLength(50)
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.Rh)
                    .HasMaxLength(50)
                    .HasColumnName("RH")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.SensorId).HasColumnType("int(11)");

                entity.Property(e => e.Temp)
                    .HasMaxLength(50)
                    .HasColumnName("Temp_")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.Voc)
                    .HasMaxLength(50)
                    .HasColumnName("VOC")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.Vocs)
                    .HasMaxLength(50)
                    .HasColumnName("VOCs");

                entity.Property(e => e.Wd)
                    .HasMaxLength(50)
                    .HasColumnName("WD");
            });

            modelBuilder.Entity<AutDynamodbEcomesure2Err>(entity =>
            {
                entity.ToTable("aut_dynamodb_ecomesure_2_err");

                entity.Property(e => e.Id)
                    .HasColumnType("bigint(20)")
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.Co)
                    .HasMaxLength(50)
                    .HasColumnName("CO")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.Co2)
                    .HasMaxLength(50)
                    .HasColumnName("CO2")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.Communication).HasMaxLength(50);

                entity.Property(e => e.Cov)
                    .HasMaxLength(50)
                    .HasColumnName("COV");

                entity.Property(e => e.Date)
                    .IsRequired()
                    .HasMaxLength(50)
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.H2s)
                    .HasMaxLength(50)
                    .HasColumnName("H2S");

                entity.Property(e => e.Mainsupply).HasMaxLength(50);

                entity.Property(e => e.No)
                    .HasMaxLength(50)
                    .HasColumnName("NO");

                entity.Property(e => e.No2)
                    .HasMaxLength(50)
                    .HasColumnName("NO2");

                entity.Property(e => e.O3).HasMaxLength(50);

                entity.Property(e => e.Pm1)
                    .HasMaxLength(50)
                    .HasColumnName("PM1");

                entity.Property(e => e.Pm10)
                    .HasMaxLength(50)
                    .HasColumnName("PM10");

                entity.Property(e => e.Pm25)
                    .HasMaxLength(50)
                    .HasColumnName("PM25");

                entity.Property(e => e.Pmtot)
                    .HasMaxLength(50)
                    .HasColumnName("PMtot")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.Pressure)
                    .HasMaxLength(50)
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.Rh)
                    .HasMaxLength(50)
                    .HasColumnName("RH")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.SensorId).HasColumnType("int(11)");

                entity.Property(e => e.Temp)
                    .HasMaxLength(50)
                    .HasColumnName("Temp_")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.Ts)
                    .HasColumnType("timestamp")
                    .HasColumnName("ts")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Voc)
                    .HasMaxLength(50)
                    .HasColumnName("VOC")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.Vocs)
                    .HasMaxLength(50)
                    .HasColumnName("VOCs");
            });

            modelBuilder.Entity<AutDynamodbEcomesure3>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("aut_dynamodb_ecomesure_3");

                entity.Property(e => e.Co)
                    .HasMaxLength(50)
                    .HasColumnName("CO")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.Co2)
                    .HasMaxLength(50)
                    .HasColumnName("CO2")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.Cov)
                    .HasMaxLength(50)
                    .HasColumnName("COV");

                entity.Property(e => e.Date)
                    .IsRequired()
                    .HasMaxLength(50)
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.H2s)
                    .HasMaxLength(50)
                    .HasColumnName("H2S");

                entity.Property(e => e.Mainsupply).HasMaxLength(50);

                entity.Property(e => e.No)
                    .HasMaxLength(50)
                    .HasColumnName("NO");

                entity.Property(e => e.No2)
                    .HasMaxLength(50)
                    .HasColumnName("NO2");

                entity.Property(e => e.O3).HasMaxLength(50);

                entity.Property(e => e.Pm1)
                    .HasMaxLength(50)
                    .HasColumnName("PM1");

                entity.Property(e => e.Pm10)
                    .HasMaxLength(50)
                    .HasColumnName("PM10");

                entity.Property(e => e.Pm25)
                    .HasMaxLength(50)
                    .HasColumnName("PM25");

                entity.Property(e => e.Pmtot)
                    .HasMaxLength(50)
                    .HasColumnName("PMtot")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.Pressure)
                    .HasMaxLength(50)
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.Rh)
                    .HasMaxLength(50)
                    .HasColumnName("RH")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.SensorId).HasColumnType("int(11)");

                entity.Property(e => e.So2)
                    .HasMaxLength(50)
                    .HasColumnName("SO2");

                entity.Property(e => e.Temp)
                    .HasMaxLength(50)
                    .HasColumnName("Temp_")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.Voc)
                    .HasMaxLength(50)
                    .HasColumnName("VOC")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.Vocs)
                    .HasMaxLength(50)
                    .HasColumnName("VOCs");

                entity.Property(e => e.Wd)
                    .HasMaxLength(50)
                    .HasColumnName("WD");

                entity.Property(e => e.WindDirection).HasMaxLength(50);
            });

            modelBuilder.Entity<AutEnmet>(entity =>
            {
                entity.ToTable("aut_enmet");

                entity.HasIndex(e => e.SiteId, "X1_aut_enmet");

                entity.HasIndex(e => new { e.DeviceId, e.TimestampUtcDt }, "X2_aut_enmet");

                entity.Property(e => e.Id).HasColumnType("bigint(20)");

                entity.Property(e => e.AlarmLevel).HasMaxLength(20);

                entity.Property(e => e.Baseline).HasMaxLength(50);

                entity.Property(e => e.BatteryVoltage)
                    .HasMaxLength(50)
                    .HasColumnName("Battery_Voltage");

                entity.Property(e => e.CalFactor).HasMaxLength(50);

                entity.Property(e => e.CarrDuty).HasMaxLength(50);

                entity.Property(e => e.ColTemp).HasMaxLength(50);

                entity.Property(e => e.Concentration).HasMaxLength(50);

                entity.Property(e => e.DateTime).HasMaxLength(50);

                entity.Property(e => e.DeadCol)
                    .HasMaxLength(50)
                    .HasColumnName("deadCol")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.DeviceId).HasMaxLength(50);

                entity.Property(e => e.EnclosureTemp).HasMaxLength(50);

                entity.Property(e => e.EngineTemp).HasMaxLength(50);

                entity.Property(e => e.Latitude).HasMaxLength(50);

                entity.Property(e => e.Longitude).HasMaxLength(50);

                entity.Property(e => e.PeakHeight).HasMaxLength(50);

                entity.Property(e => e.PeakLocation).HasMaxLength(50);

                entity.Property(e => e.Record)
                    .HasMaxLength(50)
                    .HasColumnName("RECORD");

                entity.Property(e => e.RunCount).HasMaxLength(50);

                entity.Property(e => e.RunType).HasMaxLength(50);

                entity.Property(e => e.SiteId)
                    .HasColumnName("Site_id")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.Status).HasMaxLength(50);

                entity.Property(e => e.Timestamp).HasMaxLength(50);

                entity.Property(e => e.TimestampLocal).HasMaxLength(50);

                entity.Property(e => e.TimestampLocalDt).HasColumnType("datetime");

                entity.Property(e => e.TimestampUtcDt)
                    .HasColumnType("datetime")
                    .HasColumnName("TimestampUtcDT");

                entity.Property(e => e.Vapor).HasMaxLength(50);

                entity.Property(e => e.Ver).HasMaxLength(50);

                entity.Property(e => e.WindDir).HasMaxLength(50);

                entity.Property(e => e.WsAvg)
                    .HasMaxLength(50)
                    .HasColumnName("WS_avg");

                entity.Property(e => e.WsMs)
                    .HasMaxLength(50)
                    .HasColumnName("WS_ms");
            });

            modelBuilder.Entity<AutRawEcomesure>(entity =>
            {
                entity.HasKey(e => e.RecId)
                    .HasName("PRIMARY");

                entity.ToTable("aut_raw_ecomesure");

                entity.HasIndex(e => e.Ts, "x1_aut_raw_ecomesure");

                entity.HasIndex(e => e.Serial, "x2_aut_raw_ecomesure");

                entity.HasIndex(e => e.TimestampUtc, "x3_aut_raw_ecomesure");

                entity.Property(e => e.RecId)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("rec_id");

                entity.Property(e => e.Co)
                    .HasMaxLength(255)
                    .HasColumnName("co")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.Co2)
                    .HasMaxLength(255)
                    .HasColumnName("co2")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.Comm)
                    .HasMaxLength(50)
                    .HasColumnName("comm");

                entity.Property(e => e.Cov)
                    .HasMaxLength(50)
                    .HasColumnName("cov");

                entity.Property(e => e.Dewpoint)
                    .HasMaxLength(255)
                    .HasColumnName("dewpoint")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.H2s)
                    .HasMaxLength(255)
                    .HasColumnName("h2s");

                entity.Property(e => e.Hcho)
                    .HasMaxLength(255)
                    .HasColumnName("hcho")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.Humidity)
                    .HasMaxLength(255)
                    .HasColumnName("humidity")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.Mainsupply)
                    .HasMaxLength(50)
                    .HasColumnName("mainsupply");

                entity.Property(e => e.No)
                    .HasMaxLength(255)
                    .HasColumnName("no")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.No2)
                    .HasMaxLength(255)
                    .HasColumnName("no2")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.Nox)
                    .HasMaxLength(255)
                    .HasColumnName("nox")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.O3)
                    .HasMaxLength(255)
                    .HasColumnName("o3")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.Pm1)
                    .HasMaxLength(255)
                    .HasColumnName("pm_1")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.Pm10)
                    .HasMaxLength(255)
                    .HasColumnName("pm_10")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.Pm25)
                    .HasMaxLength(255)
                    .HasColumnName("pm_2_5")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.Pressure)
                    .HasMaxLength(255)
                    .HasColumnName("pressure")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.Serial)
                    .HasColumnName("serial")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.So2)
                    .HasMaxLength(255)
                    .HasColumnName("so2")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.Temp)
                    .HasMaxLength(255)
                    .HasColumnName("temp")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.TimestampLocal)
                    .HasMaxLength(255)
                    .HasColumnName("timestamp_local")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.TimestampUtc)
                    .HasColumnName("timestamp_utc")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.Ts)
                    .HasColumnType("timestamp")
                    .HasColumnName("ts")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Tvoc)
                    .HasMaxLength(255)
                    .HasColumnName("tvoc")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.Wd)
                    .HasMaxLength(255)
                    .HasColumnName("WD");
            });

            modelBuilder.Entity<AutRawEcomesureErr>(entity =>
            {
                entity.HasKey(e => e.RecId)
                    .HasName("PRIMARY");

                entity.ToTable("aut_raw_ecomesure_err");

                entity.HasIndex(e => e.RecId, "x1_aut_raw_ecomesure_err");

                entity.Property(e => e.RecId)
                    .HasColumnType("bigint(20)")
                    .ValueGeneratedNever()
                    .HasColumnName("rec_id");

                entity.Property(e => e.Message)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("message");

                entity.Property(e => e.Ts)
                    .HasColumnType("timestamp")
                    .HasColumnName("ts")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");
            });

            modelBuilder.Entity<AutTmpDatGc>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("aut_tmp_dat_gc");

                entity.HasIndex(e => new { e.DeviceId, e.TimestampUtc, e.ParamId }, "X1_dat_gc");

                entity.Property(e => e.DeviceId).HasColumnType("int(11)");

                entity.Property(e => e.ParamId)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.SourceFileId).HasColumnType("bigint(20)");

                entity.Property(e => e.Status).HasMaxLength(20);

                entity.Property(e => e.TimestampUtc).HasColumnType("datetime");

                entity.Property(e => e.Unit)
                    .HasMaxLength(50)
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");
            });

            modelBuilder.Entity<Automation>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("automation");

                entity.Property(e => e.Co).HasColumnName("co");

                entity.Property(e => e.Co2).HasColumnName("co2");

                entity.Property(e => e.Dewpoint).HasColumnName("dewpoint");

                entity.Property(e => e.Hcho).HasColumnName("hcho");

                entity.Property(e => e.Humidity).HasColumnName("humidity");

                entity.Property(e => e.No).HasColumnName("no");

                entity.Property(e => e.No2).HasColumnName("no2");

                entity.Property(e => e.Nox).HasColumnName("nox");

                entity.Property(e => e.O3).HasColumnName("o3");

                entity.Property(e => e.Pm1).HasColumnName("pm_1");

                entity.Property(e => e.Pm10).HasColumnName("pm_10");

                entity.Property(e => e.Pm25).HasColumnName("pm_2_5");

                entity.Property(e => e.Pressure).HasColumnName("pressure");

                entity.Property(e => e.Serial)
                    .HasMaxLength(255)
                    .HasColumnName("serial")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.So2).HasColumnName("so2");

                entity.Property(e => e.Temp).HasColumnName("temp");

                entity.Property(e => e.TimestampLocal)
                    .HasColumnType("datetime")
                    .HasColumnName("timestamp_local");

                entity.Property(e => e.TimestampUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("timestamp_utc");

                entity.Property(e => e.Tvoc).HasColumnName("tvoc");
            });

            modelBuilder.Entity<DatCustomOdorComplaint>(entity =>
            {
                entity.ToTable("dat_custom_odor_complaints");

                entity.HasIndex(e => new { e.TenantId, e.SoNumber }, "X1_dat_custom_odor_complaitns");

                entity.Property(e => e.Id).HasColumnType("bigint(20)");

                entity.Property(e => e.City).HasMaxLength(50);

                entity.Property(e => e.ComplaintDate).HasColumnType("date");

                entity.Property(e => e.ComplaintTime).HasMaxLength(10);

                entity.Property(e => e.SoNumber)
                    .HasMaxLength(50)
                    .HasColumnName("SO_number");

                entity.Property(e => e.SoWorkcenter)
                    .HasMaxLength(50)
                    .HasColumnName("SO_workcenter");

                entity.Property(e => e.TenantId).HasColumnType("int(11)");
            });

            modelBuilder.Entity<DatDeviceLatlng>(entity =>
            {
                entity.HasKey(e => new { e.DeviceId, e.TimestampUtc })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("dat_device_latlng");

                entity.HasIndex(e => e.LastModifiedUtcDt, "x1_device_latlng");

                entity.Property(e => e.DeviceId).HasColumnType("smallint(6)");

                entity.Property(e => e.TimestampUtc).HasColumnType("datetime");

                entity.Property(e => e.LastModifiedUtcDt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.SourceFileId).HasColumnType("bigint(20)");
            });

            modelBuilder.Entity<DatDeviceLatlngDaily>(entity =>
            {
                entity.HasKey(e => new { e.DeviceId, e.TimestampUtc })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("dat_device_latlng_daily");

                entity.HasIndex(e => e.LastModifiedUtcDt, "x1_device_latlng_daily");

                entity.Property(e => e.DeviceId).HasColumnType("smallint(6)");

                entity.Property(e => e.TimestampUtc).HasColumnType("datetime");

                entity.Property(e => e.LastModifiedUtcDt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.RecCount).HasColumnType("int(11)");
            });

            modelBuilder.Entity<DatDeviceLatlngHourly>(entity =>
            {
                entity.HasKey(e => new { e.DeviceId, e.TimestampUtc })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("dat_device_latlng_hourly");

                entity.HasIndex(e => e.LastModifiedUtcDt, "x1_device_latlng_hourly");

                entity.Property(e => e.DeviceId).HasColumnType("smallint(6)");

                entity.Property(e => e.TimestampUtc).HasColumnType("datetime");

                entity.Property(e => e.LastModifiedUtcDt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.RecCount).HasColumnType("int(11)");
            });

            modelBuilder.Entity<DatDeviceLatlngMonthly>(entity =>
            {
                entity.HasKey(e => new { e.DeviceId, e.TimestampUtc })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("dat_device_latlng_monthly");

                entity.HasIndex(e => e.LastModifiedUtcDt, "x1_device_latlng_monthly");

                entity.Property(e => e.DeviceId).HasColumnType("smallint(6)");

                entity.Property(e => e.TimestampUtc).HasColumnType("datetime");

                entity.Property(e => e.LastModifiedUtcDt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.RecCount).HasColumnType("int(11)");
            });

            modelBuilder.Entity<DatDeviceLatlngOld>(entity =>
            {
                entity.HasKey(e => new { e.DeviceId, e.TimestampUtc })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("dat_device_latlng_old");

                entity.HasIndex(e => e.LastModifiedUtcDt, "x1_device_latlng");

                entity.Property(e => e.DeviceId).HasColumnType("smallint(6)");

                entity.Property(e => e.TimestampUtc).HasColumnType("datetime");

                entity.Property(e => e.LastModifiedUtcDt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.SourceFileId).HasColumnType("bigint(20)");
            });

            modelBuilder.Entity<DatDeviceParameter>(entity =>
            {
                entity.HasKey(e => new { e.DeviceId, e.ParamId })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("dat_device_parameter");

                entity.Property(e => e.DeviceId).HasColumnType("int(11)");
            });

            modelBuilder.Entity<DatGc>(entity =>
            {
                entity.HasKey(e => new { e.DeviceId, e.TimestampUtc, e.ParamId })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0, 0 });

                entity.ToTable("dat_gc");

                entity.HasIndex(e => e.LastModifiedUtcDt, "x2_dat_gc");

                entity.Property(e => e.DeviceId).HasColumnType("int(11)");

                entity.Property(e => e.TimestampUtc).HasColumnType("datetime");

                entity.Property(e => e.ParamId).HasMaxLength(50);

                entity.Property(e => e.LastModifiedUtcDt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.SourceFileId).HasColumnType("bigint(20)");

                entity.Property(e => e.Status).HasMaxLength(20);

                entity.Property(e => e.Unit)
                    .HasMaxLength(50)
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");
            });

            modelBuilder.Entity<DatGcDaily>(entity =>
            {
                entity.HasKey(e => new { e.DeviceId, e.TimestampUtc, e.ParamId })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0, 0 });

                entity.ToTable("dat_gc_daily");

                entity.HasIndex(e => e.LastModifiedUtcDt, "x1_gc_daily");

                entity.Property(e => e.DeviceId).HasColumnType("int(11)");

                entity.Property(e => e.TimestampUtc).HasColumnType("datetime");

                entity.Property(e => e.ParamId).HasMaxLength(50);

                entity.Property(e => e.LastModifiedUtcDt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.RecCount).HasColumnType("int(11)");

                entity.Property(e => e.Unit)
                    .HasMaxLength(50)
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");
            });

            modelBuilder.Entity<DatGcHourly>(entity =>
            {
                entity.HasKey(e => new { e.DeviceId, e.TimestampUtc, e.ParamId })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0, 0 });

                entity.ToTable("dat_gc_hourly");

                entity.HasIndex(e => e.LastModifiedUtcDt, "x1_gc_hourly");

                entity.Property(e => e.DeviceId).HasColumnType("int(11)");

                entity.Property(e => e.TimestampUtc).HasColumnType("datetime");

                entity.Property(e => e.ParamId).HasMaxLength(50);

                entity.Property(e => e.LastModifiedUtcDt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.RecCount).HasColumnType("smallint(6)");

                entity.Property(e => e.RollingAvg1Hrs).HasColumnType("tinyint(4)");

                entity.Property(e => e.RollingAvg2Hrs).HasColumnType("tinyint(4)");

                entity.Property(e => e.Unit)
                    .HasMaxLength(50)
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");
            });

            modelBuilder.Entity<DatGcMonthly>(entity =>
            {
                entity.HasKey(e => new { e.DeviceId, e.TimestampUtc, e.ParamId })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0, 0 });

                entity.ToTable("dat_gc_monthly");

                entity.HasIndex(e => e.LastModifiedUtcDt, "x1_gc_monthly");

                entity.Property(e => e.DeviceId).HasColumnType("int(11)");

                entity.Property(e => e.TimestampUtc).HasColumnType("datetime");

                entity.Property(e => e.ParamId).HasMaxLength(50);

                entity.Property(e => e.LastModifiedUtcDt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.RecCount).HasColumnType("int(11)");

                entity.Property(e => e.Unit)
                    .HasMaxLength(50)
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");
            });

            modelBuilder.Entity<DatGcOld>(entity =>
            {
                entity.ToTable("dat_gc_old");

                entity.HasIndex(e => new { e.DeviceId, e.TimestampUtc, e.ParamId }, "X1_dat_gc");

                entity.HasIndex(e => e.LastModifiedUtcDt, "x2_dat_gc");

                entity.Property(e => e.Id).HasColumnType("bigint(20)");

                entity.Property(e => e.DeviceId).HasColumnType("int(11)");

                entity.Property(e => e.LastModifiedUtcDt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.ParamId)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.SourceFileId).HasColumnType("bigint(20)");

                entity.Property(e => e.Status).HasMaxLength(20);

                entity.Property(e => e.TimestampUtc).HasColumnType("datetime");

                entity.Property(e => e.Unit)
                    .HasMaxLength(50)
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");
            });

            modelBuilder.Entity<DatGcSy>(entity =>
            {
                entity.HasKey(e => new { e.DeviceId, e.TimestampUtc })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("dat_gc_sys");

                entity.HasIndex(e => e.LastModifiedUtcDt, "x2_gc_sys");

                entity.Property(e => e.DeviceId).HasColumnType("int(11)");

                entity.Property(e => e.TimestampUtc).HasColumnType("datetime");

                entity.Property(e => e.LastModifiedUtcDt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.SourceFileId).HasColumnType("bigint(20)");
            });

            modelBuilder.Entity<DatGcSysDaily>(entity =>
            {
                entity.HasKey(e => new { e.DeviceId, e.TimestampUtc })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("dat_gc_sys_daily");

                entity.HasIndex(e => e.LastModifiedUtcDt, "x1_gc_sys_daily");

                entity.Property(e => e.DeviceId).HasColumnType("int(11)");

                entity.Property(e => e.TimestampUtc).HasColumnType("datetime");

                entity.Property(e => e.LastModifiedUtcDt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.RecCount).HasColumnType("int(11)");
            });

            modelBuilder.Entity<DatGcSysHourly>(entity =>
            {
                entity.HasKey(e => new { e.DeviceId, e.TimestampUtc })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("dat_gc_sys_hourly");

                entity.HasIndex(e => e.LastModifiedUtcDt, "x1_gc_sys_hourly");

                entity.Property(e => e.DeviceId).HasColumnType("int(11)");

                entity.Property(e => e.TimestampUtc).HasColumnType("datetime");

                entity.Property(e => e.LastModifiedUtcDt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.RecCount).HasColumnType("int(11)");
            });

            modelBuilder.Entity<DatGcSysMonthly>(entity =>
            {
                entity.HasKey(e => new { e.DeviceId, e.TimestampUtc })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("dat_gc_sys_monthly");

                entity.HasIndex(e => e.LastModifiedUtcDt, "x1_gc_sys_monthly");

                entity.Property(e => e.DeviceId).HasColumnType("int(11)");

                entity.Property(e => e.TimestampUtc).HasColumnType("datetime");

                entity.Property(e => e.LastModifiedUtcDt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.RecCount).HasColumnType("int(11)");
            });

            modelBuilder.Entity<DatGcSysOld>(entity =>
            {
                entity.ToTable("dat_gc_sys_old");

                entity.HasIndex(e => new { e.DeviceId, e.TimestampUtc }, "X1_dat_gc_sys");

                entity.HasIndex(e => e.LastModifiedUtcDt, "x2_gc_sys");

                entity.Property(e => e.Id).HasColumnType("bigint(20)");

                entity.Property(e => e.DeviceId).HasColumnType("int(11)");

                entity.Property(e => e.LastModifiedUtcDt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.SourceFileId).HasColumnType("bigint(20)");

                entity.Property(e => e.TimestampUtc).HasColumnType("datetime");
            });

            modelBuilder.Entity<DatLatestDevice>(entity =>
            {
                entity.HasKey(e => e.DeviceId)
                    .HasName("PRIMARY");

                entity.ToTable("dat_latest_device");

                entity.HasIndex(e => e.DeviceLastTransmittedDt, "x1_latest_device");

                entity.Property(e => e.DeviceId)
                    .HasColumnType("int(11)")
                    .ValueGeneratedNever();

                entity.Property(e => e.DeviceLastTransmittedDt)
                    .HasColumnType("datetime")
                    .HasColumnName("DeviceLastTransmittedDT")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.LastModifiedUtc).HasColumnType("datetime");

                entity.Property(e => e.LatLngModifiedUtc).HasColumnType("datetime");

                entity.Property(e => e.SysModifiedUtc).HasColumnType("datetime");

                entity.Property(e => e.WindModifiedUtc).HasColumnType("datetime");

                entity.Property(e => e.WindUnit).HasMaxLength(50);
            });

            modelBuilder.Entity<DatLatestDeviceGc>(entity =>
            {
                entity.HasKey(e => new { e.DeviceId, e.ParamId })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("dat_latest_device_gc");

                entity.HasIndex(e => e.RollingAvgDt, "x1_latest_device_gc");

                entity.HasIndex(e => e.TimestampUtc, "x2_latest_device_gc");

                entity.Property(e => e.DeviceId).HasColumnType("int(11)");

                entity.Property(e => e.ParamId).HasMaxLength(50);

                entity.Property(e => e.DeviceLastTransmittedDt)
                    .HasColumnType("datetime")
                    .HasColumnName("DeviceLastTransmittedDT")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.RollingAvg1Hrs).HasColumnType("tinyint(4)");

                entity.Property(e => e.RollingAvg2Hrs).HasColumnType("tinyint(4)");

                entity.Property(e => e.RollingAvgDt).HasColumnType("datetime");

                entity.Property(e => e.TimestampUtc).HasColumnType("datetime");

                entity.Property(e => e.Unit).HasMaxLength(50);
            });

            modelBuilder.Entity<DatReading>(entity =>
            {
                entity.HasKey(e => new { e.DeviceId, e.TimestampUtc })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("dat_reading");

                entity.HasIndex(e => e.LastModifiedDt, "X1_dat_reading");

                entity.HasIndex(e => new { e.DeviceId, e.TimestampLocal }, "X2_dat_reading");

                entity.Property(e => e.DeviceId).HasColumnType("int(11)");

                entity.Property(e => e.TimestampUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("TimestampUTC");

                entity.Property(e => e.Co).HasColumnName("co");

                entity.Property(e => e.Co2).HasColumnName("co2");

                entity.Property(e => e.Dewpoint).HasColumnName("dewpoint");

                entity.Property(e => e.H2s).HasColumnName("h2s");

                entity.Property(e => e.Humidity).HasColumnName("humidity");

                entity.Property(e => e.LastModifiedDt)
                    .HasColumnType("datetime")
                    .HasColumnName("LastModifiedDT");

                entity.Property(e => e.No).HasColumnName("no");

                entity.Property(e => e.No2).HasColumnName("no2");

                entity.Property(e => e.Nox).HasColumnName("nox");

                entity.Property(e => e.O3).HasColumnName("o3");

                entity.Property(e => e.Pm1).HasColumnName("pm_1");

                entity.Property(e => e.Pm10).HasColumnName("pm_10");

                entity.Property(e => e.Pm25).HasColumnName("pm_2_5");

                entity.Property(e => e.Pressure).HasColumnName("pressure");

                entity.Property(e => e.So2).HasColumnName("so2");

                entity.Property(e => e.Temp).HasColumnName("temp");

                entity.Property(e => e.TimestampLocal).HasColumnType("datetime");

                entity.Property(e => e.TotalVoc).HasColumnName("total_voc");

                entity.Property(e => e.Wd).HasColumnName("wd");
            });

            modelBuilder.Entity<DatReadingDaily>(entity =>
            {
                entity.HasKey(e => new { e.DeviceId, e.TimestampUtc })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("dat_reading_daily");

                entity.HasIndex(e => e.LastModifiedDt, "modified");

                entity.Property(e => e.DeviceId).HasColumnType("int(11)");

                entity.Property(e => e.TimestampUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("TimestampUTC");

                entity.Property(e => e.Co).HasColumnName("co");

                entity.Property(e => e.Co2).HasColumnName("co2");

                entity.Property(e => e.Co2Delta).HasColumnName("co2_delta");

                entity.Property(e => e.Co2Rollavg).HasColumnName("co2_rollavg");

                entity.Property(e => e.Co2Score)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("co2_score");

                entity.Property(e => e.CoDelta).HasColumnName("co_delta");

                entity.Property(e => e.CoRollavg).HasColumnName("co_rollavg");

                entity.Property(e => e.CoScore)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("co_score");

                entity.Property(e => e.Dewpoint).HasColumnName("dewpoint");

                entity.Property(e => e.DewpointDelta).HasColumnName("dewpoint_delta");

                entity.Property(e => e.DewpointRollavg).HasColumnName("dewpoint_rollavg");

                entity.Property(e => e.DewpointScore)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("dewpoint_score");

                entity.Property(e => e.H2s).HasColumnName("h2s");

                entity.Property(e => e.Humidity).HasColumnName("humidity");

                entity.Property(e => e.HumidityDelta).HasColumnName("humidity_delta");

                entity.Property(e => e.HumidityRollavg).HasColumnName("humidity_rollavg");

                entity.Property(e => e.HumidityScore)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("humidity_score");

                entity.Property(e => e.LastModifiedDt).HasColumnType("datetime");

                entity.Property(e => e.No).HasColumnName("no");

                entity.Property(e => e.No2).HasColumnName("no2");

                entity.Property(e => e.No2Delta).HasColumnName("no2_delta");

                entity.Property(e => e.No2Rollavg).HasColumnName("no2_rollavg");

                entity.Property(e => e.No2Score)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("no2_score");

                entity.Property(e => e.NoDelta).HasColumnName("no_delta");

                entity.Property(e => e.NoRollavg).HasColumnName("no_rollavg");

                entity.Property(e => e.NoScore)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("no_score");

                entity.Property(e => e.Nox).HasColumnName("nox");

                entity.Property(e => e.NoxDelta).HasColumnName("nox_delta");

                entity.Property(e => e.NoxRollavg).HasColumnName("nox_rollavg");

                entity.Property(e => e.NoxScore)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("nox_score");

                entity.Property(e => e.O3).HasColumnName("o3");

                entity.Property(e => e.O3Delta).HasColumnName("o3_delta");

                entity.Property(e => e.O3Rollavg).HasColumnName("o3_rollavg");

                entity.Property(e => e.O3Score)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("o3_score");

                entity.Property(e => e.Pm1).HasColumnName("pm_1");

                entity.Property(e => e.Pm10).HasColumnName("pm_10");

                entity.Property(e => e.Pm10Delta).HasColumnName("pm_10_delta");

                entity.Property(e => e.Pm10Rollavg).HasColumnName("pm_10_rollavg");

                entity.Property(e => e.Pm10Score)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("pm_10_score");

                entity.Property(e => e.Pm1Delta).HasColumnName("pm_1_delta");

                entity.Property(e => e.Pm1Rollavg).HasColumnName("pm_1_rollavg");

                entity.Property(e => e.Pm1Score)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("pm_1_score");

                entity.Property(e => e.Pm25).HasColumnName("pm_2_5");

                entity.Property(e => e.Pm25Delta).HasColumnName("pm_2_5_delta");

                entity.Property(e => e.Pm25Rollavg).HasColumnName("pm_2_5_rollavg");

                entity.Property(e => e.Pm25Score)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("pm_2_5_score");

                entity.Property(e => e.Pressure).HasColumnName("pressure");

                entity.Property(e => e.PressureDelta).HasColumnName("pressure_delta");

                entity.Property(e => e.PressureRollavg).HasColumnName("pressure_rollavg");

                entity.Property(e => e.PressureScore)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("pressure_score");

                entity.Property(e => e.RecCount)
                    .HasColumnType("int(11)")
                    .HasColumnName("rec_count");

                entity.Property(e => e.So2).HasColumnName("so2");

                entity.Property(e => e.So2Delta).HasColumnName("so2_delta");

                entity.Property(e => e.So2Rollavg).HasColumnName("so2_rollavg");

                entity.Property(e => e.So2Score)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("so2_score");

                entity.Property(e => e.Temp).HasColumnName("temp");

                entity.Property(e => e.TempDelta).HasColumnName("temp_delta");

                entity.Property(e => e.TempRollavg).HasColumnName("temp_rollavg");

                entity.Property(e => e.TempScore)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("temp_score");

                entity.Property(e => e.TimestampLocal).HasColumnType("datetime");

                entity.Property(e => e.TotalVoc).HasColumnName("total_voc");

                entity.Property(e => e.TotalVocDelta).HasColumnName("total_voc_delta");

                entity.Property(e => e.TotalVocRollavg).HasColumnName("total_voc_rollavg");

                entity.Property(e => e.TotalVocScore)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("total_voc_score");

                entity.Property(e => e.Wd).HasColumnName("wd");
            });

            modelBuilder.Entity<DatReadingHourly>(entity =>
            {
                entity.HasKey(e => new { e.DeviceId, e.TimestampUtc })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("dat_reading_hourly");

                entity.HasIndex(e => e.LastModifiedDt, "modified");

                entity.Property(e => e.DeviceId).HasColumnType("int(11)");

                entity.Property(e => e.TimestampUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("TimestampUTC");

                entity.Property(e => e.Co).HasColumnName("co");

                entity.Property(e => e.Co2).HasColumnName("co2");

                entity.Property(e => e.Co2Delta).HasColumnName("co2_delta");

                entity.Property(e => e.Co2Rollavg).HasColumnName("co2_rollavg");

                entity.Property(e => e.Co2Score)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("co2_score");

                entity.Property(e => e.CoDelta).HasColumnName("co_delta");

                entity.Property(e => e.CoRollavg).HasColumnName("co_rollavg");

                entity.Property(e => e.CoScore)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("co_score");

                entity.Property(e => e.Dewpoint).HasColumnName("dewpoint");

                entity.Property(e => e.DewpointDelta).HasColumnName("dewpoint_delta");

                entity.Property(e => e.DewpointRollavg).HasColumnName("dewpoint_rollavg");

                entity.Property(e => e.DewpointScore)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("dewpoint_score");

                entity.Property(e => e.H2s).HasColumnName("h2s");

                entity.Property(e => e.Humidity).HasColumnName("humidity");

                entity.Property(e => e.HumidityDelta).HasColumnName("humidity_delta");

                entity.Property(e => e.HumidityRollavg).HasColumnName("humidity_rollavg");

                entity.Property(e => e.HumidityScore)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("humidity_score");

                entity.Property(e => e.LastModifiedDt).HasColumnType("datetime");

                entity.Property(e => e.No).HasColumnName("no");

                entity.Property(e => e.No2).HasColumnName("no2");

                entity.Property(e => e.No2Delta).HasColumnName("no2_delta");

                entity.Property(e => e.No2Rollavg).HasColumnName("no2_rollavg");

                entity.Property(e => e.No2Score)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("no2_score");

                entity.Property(e => e.NoDelta).HasColumnName("no_delta");

                entity.Property(e => e.NoRollavg).HasColumnName("no_rollavg");

                entity.Property(e => e.NoScore)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("no_score");

                entity.Property(e => e.Nox).HasColumnName("nox");

                entity.Property(e => e.NoxDelta).HasColumnName("nox_delta");

                entity.Property(e => e.NoxRollavg).HasColumnName("nox_rollavg");

                entity.Property(e => e.NoxScore)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("nox_score");

                entity.Property(e => e.O3).HasColumnName("o3");

                entity.Property(e => e.O3Delta).HasColumnName("o3_delta");

                entity.Property(e => e.O3Rollavg).HasColumnName("o3_rollavg");

                entity.Property(e => e.O3Score)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("o3_score");

                entity.Property(e => e.Pm1).HasColumnName("pm_1");

                entity.Property(e => e.Pm10).HasColumnName("pm_10");

                entity.Property(e => e.Pm10Delta).HasColumnName("pm_10_delta");

                entity.Property(e => e.Pm10Rollavg).HasColumnName("pm_10_rollavg");

                entity.Property(e => e.Pm10Score)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("pm_10_score");

                entity.Property(e => e.Pm1Delta).HasColumnName("pm_1_delta");

                entity.Property(e => e.Pm1Rollavg).HasColumnName("pm_1_rollavg");

                entity.Property(e => e.Pm1Score)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("pm_1_score");

                entity.Property(e => e.Pm25).HasColumnName("pm_2_5");

                entity.Property(e => e.Pm25Delta).HasColumnName("pm_2_5_delta");

                entity.Property(e => e.Pm25Rollavg).HasColumnName("pm_2_5_rollavg");

                entity.Property(e => e.Pm25Score)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("pm_2_5_score");

                entity.Property(e => e.Pressure).HasColumnName("pressure");

                entity.Property(e => e.PressureDelta).HasColumnName("pressure_delta");

                entity.Property(e => e.PressureRollavg).HasColumnName("pressure_rollavg");

                entity.Property(e => e.PressureScore)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("pressure_score");

                entity.Property(e => e.RecCount)
                    .HasColumnType("int(11)")
                    .HasColumnName("rec_count");

                entity.Property(e => e.RollingAvgDt).HasColumnType("datetime");

                entity.Property(e => e.So2).HasColumnName("so2");

                entity.Property(e => e.So2Delta).HasColumnName("so2_delta");

                entity.Property(e => e.So2Rollavg).HasColumnName("so2_rollavg");

                entity.Property(e => e.So2Score)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("so2_score");

                entity.Property(e => e.Temp).HasColumnName("temp");

                entity.Property(e => e.TempDelta).HasColumnName("temp_delta");

                entity.Property(e => e.TempRollavg).HasColumnName("temp_rollavg");

                entity.Property(e => e.TempScore)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("temp_score");

                entity.Property(e => e.TimestampLocal).HasColumnType("datetime");

                entity.Property(e => e.TotalVoc).HasColumnName("total_voc");

                entity.Property(e => e.TotalVocDelta).HasColumnName("total_voc_delta");

                entity.Property(e => e.TotalVocRollavg).HasColumnName("total_voc_rollavg");

                entity.Property(e => e.TotalVocScore)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("total_voc_score");

                entity.Property(e => e.Wd).HasColumnName("wd");
            });

            modelBuilder.Entity<DatReadingQuartHour>(entity =>
            {
                entity.HasKey(e => new { e.DeviceId, e.TimestampUtc })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("dat_reading_quart_hour");

                entity.HasIndex(e => e.LastModifiedDt, "modified");

                entity.Property(e => e.DeviceId).HasColumnType("int(11)");

                entity.Property(e => e.TimestampUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("TimestampUTC");

                entity.Property(e => e.Co).HasColumnName("co");

                entity.Property(e => e.Co2).HasColumnName("co2");

                entity.Property(e => e.Co2Rollavg).HasColumnName("co2_rollavg");

                entity.Property(e => e.CoMax).HasColumnName("co_max");

                entity.Property(e => e.CoMin).HasColumnName("co_min");

                entity.Property(e => e.CoRollavg).HasColumnName("co_rollavg");

                entity.Property(e => e.Dewpoint).HasColumnName("dewpoint");

                entity.Property(e => e.DewpointMax).HasColumnName("dewpoint_max");

                entity.Property(e => e.DewpointMin).HasColumnName("dewpoint_min");

                entity.Property(e => e.DewpointRollavg).HasColumnName("dewpoint_rollavg");

                entity.Property(e => e.H2s).HasColumnName("h2s");

                entity.Property(e => e.H2sMax).HasColumnName("h2s_max");

                entity.Property(e => e.H2sMin).HasColumnName("h2s_min");

                entity.Property(e => e.H2sRollavg).HasColumnName("h2s_rollavg");

                entity.Property(e => e.Humidity).HasColumnName("humidity");

                entity.Property(e => e.HumidityMax).HasColumnName("humidity_max");

                entity.Property(e => e.HumidityMin).HasColumnName("humidity_min");

                entity.Property(e => e.HumidityRollavg).HasColumnName("humidity_rollavg");

                entity.Property(e => e.LastModifiedDt).HasColumnType("datetime");

                entity.Property(e => e.No).HasColumnName("no");

                entity.Property(e => e.No2).HasColumnName("no2");

                entity.Property(e => e.No2Max).HasColumnName("no2_max");

                entity.Property(e => e.No2Min).HasColumnName("no2_min");

                entity.Property(e => e.No2Rollavg).HasColumnName("no2_rollavg");

                entity.Property(e => e.NoMax).HasColumnName("no_max");

                entity.Property(e => e.NoMin).HasColumnName("no_min");

                entity.Property(e => e.NoRollavg).HasColumnName("no_rollavg");

                entity.Property(e => e.Nox).HasColumnName("nox");

                entity.Property(e => e.NoxMax).HasColumnName("nox_max");

                entity.Property(e => e.NoxMin).HasColumnName("nox_min");

                entity.Property(e => e.NoxRollavg).HasColumnName("nox_rollavg");

                entity.Property(e => e.O3).HasColumnName("o3");

                entity.Property(e => e.O3Max).HasColumnName("o3_max");

                entity.Property(e => e.O3Min).HasColumnName("o3_min");

                entity.Property(e => e.O3Rollavg).HasColumnName("o3_rollavg");

                entity.Property(e => e.Pm1).HasColumnName("pm_1");

                entity.Property(e => e.Pm10).HasColumnName("pm_10");

                entity.Property(e => e.Pm10Max).HasColumnName("pm_10_max");

                entity.Property(e => e.Pm10Min).HasColumnName("pm_10_min");

                entity.Property(e => e.Pm10Rollavg).HasColumnName("pm_10_rollavg");

                entity.Property(e => e.Pm1Max).HasColumnName("pm_1_max");

                entity.Property(e => e.Pm1Min).HasColumnName("pm_1_min");

                entity.Property(e => e.Pm1Rollavg).HasColumnName("pm_1_rollavg");

                entity.Property(e => e.Pm25).HasColumnName("pm_2_5");

                entity.Property(e => e.Pm25Max).HasColumnName("pm_2_5_max");

                entity.Property(e => e.Pm25Min).HasColumnName("pm_2_5_min");

                entity.Property(e => e.Pm25Rollavg).HasColumnName("pm_2_5_rollavg");

                entity.Property(e => e.Pressure).HasColumnName("pressure");

                entity.Property(e => e.PressureMax).HasColumnName("pressure_max");

                entity.Property(e => e.PressureMin).HasColumnName("pressure_min");

                entity.Property(e => e.PressureRollavg).HasColumnName("pressure_rollavg");

                entity.Property(e => e.RecCount)
                    .HasColumnType("int(11)")
                    .HasColumnName("rec_count");

                entity.Property(e => e.So2).HasColumnName("so2");

                entity.Property(e => e.So2Max).HasColumnName("so2_max");

                entity.Property(e => e.So2Min).HasColumnName("so2_min");

                entity.Property(e => e.So2Rollavg).HasColumnName("so2_rollavg");

                entity.Property(e => e.Temp).HasColumnName("temp");

                entity.Property(e => e.TempMax).HasColumnName("temp_max");

                entity.Property(e => e.TempMin).HasColumnName("temp_min");

                entity.Property(e => e.TempRollavg).HasColumnName("temp_rollavg");

                entity.Property(e => e.TimestampLocal).HasColumnType("datetime");

                entity.Property(e => e.TotalVoc).HasColumnName("total_voc");

                entity.Property(e => e.TotalVocMax).HasColumnName("total_voc_max");

                entity.Property(e => e.TotalVocMin).HasColumnName("total_voc_min");

                entity.Property(e => e.TotalVocRollavg).HasColumnName("total_voc_rollavg");

                entity.Property(e => e.Wd).HasColumnName("wd");
            });

            modelBuilder.Entity<DatWind>(entity =>
            {
                entity.HasKey(e => new { e.DeviceId, e.TimestampUtc })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("dat_wind");

                entity.HasIndex(e => e.LastModifiedUtcDt, "X1_dat_wind");

                entity.Property(e => e.DeviceId).HasColumnType("int(11)");

                entity.Property(e => e.TimestampUtc).HasColumnType("datetime");

                entity.Property(e => e.LastModifiedUtcDt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.SourceFileId).HasColumnType("bigint(20)");

                entity.Property(e => e.WindUnit)
                    .HasMaxLength(50)
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");
            });

            modelBuilder.Entity<DatWindDaily>(entity =>
            {
                entity.HasKey(e => new { e.DeviceId, e.TimestampUtc })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("dat_wind_daily");

                entity.HasIndex(e => e.LastModifiedUtcDt, "x1_wind_daily");

                entity.Property(e => e.DeviceId).HasColumnType("int(11)");

                entity.Property(e => e.TimestampUtc).HasColumnType("datetime");

                entity.Property(e => e.LastModifiedUtcDt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.RecCount).HasColumnType("int(11)");

                entity.Property(e => e.WindUnit)
                    .HasMaxLength(50)
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");
            });

            modelBuilder.Entity<DatWindHourly>(entity =>
            {
                entity.HasKey(e => new { e.DeviceId, e.TimestampUtc })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("dat_wind_hourly");

                entity.HasIndex(e => e.LastModifiedUtcDt, "x1_wind_hourly");

                entity.Property(e => e.DeviceId).HasColumnType("int(11)");

                entity.Property(e => e.TimestampUtc).HasColumnType("datetime");

                entity.Property(e => e.LastModifiedUtcDt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.RecCount).HasColumnType("int(11)");

                entity.Property(e => e.WindUnit)
                    .HasMaxLength(50)
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");
            });

            modelBuilder.Entity<DatWindMonthly>(entity =>
            {
                entity.HasKey(e => new { e.DeviceId, e.TimestampUtc })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("dat_wind_monthly");

                entity.HasIndex(e => e.LastModifiedUtcDt, "x1_wind_monthly");

                entity.Property(e => e.DeviceId).HasColumnType("int(11)");

                entity.Property(e => e.TimestampUtc).HasColumnType("datetime");

                entity.Property(e => e.LastModifiedUtcDt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.RecCount).HasColumnType("int(11)");

                entity.Property(e => e.WindUnit)
                    .HasMaxLength(50)
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");
            });

            modelBuilder.Entity<DatWindOld>(entity =>
            {
                entity.ToTable("dat_wind_old");

                entity.HasIndex(e => new { e.DeviceId, e.TimestampUtc }, "X1_dat_wind");

                entity.Property(e => e.Id).HasColumnType("bigint(20)");

                entity.Property(e => e.DeviceId).HasColumnType("int(11)");

                entity.Property(e => e.LastModifiedUtcDt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.SourceFileId).HasColumnType("bigint(20)");

                entity.Property(e => e.TimestampUtc).HasColumnType("datetime");

                entity.Property(e => e.WindUnit)
                    .HasMaxLength(50)
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");
            });

            modelBuilder.Entity<LovMem>(entity =>
            {
                entity.ToTable("lov_mem");

                entity.HasIndex(e => e.LovMemId, "LovMemId_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => new { e.Class, e.Value }, "X1_LovMem");

                entity.HasIndex(e => e.ManufacturerId, "X2_LovMem");

                entity.HasIndex(e => e.SystemParam, "X3_LovMem");

                entity.Property(e => e.LovMemId).HasColumnType("int(11)");

                entity.Property(e => e.Class)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.CreatedDt).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(255);

                entity.Property(e => e.ManufacturerId).HasColumnType("int(11)");

                entity.Property(e => e.Param1).HasMaxLength(50);

                entity.Property(e => e.Param2).HasMaxLength(50);

                entity.Property(e => e.Param3).HasMaxLength(50);

                entity.Property(e => e.Param4).HasMaxLength(50);

                entity.Property(e => e.Param5).HasMaxLength(50);

                entity.Property(e => e.SystemParam)
                    .IsRequired()
                    .HasMaxLength(1);

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<LovSite>(entity =>
            {
                entity.ToTable("lov_sites");

                entity.Property(e => e.LovSiteId).HasColumnType("int(11)");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.CreatedDt).HasColumnType("datetime");

                entity.Property(e => e.SiteId).HasColumnType("int(11)");

                entity.Property(e => e.SiteName)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.TenantId).HasColumnType("int(11)");

                entity.Property(e => e.TenantName)
                    .IsRequired()
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<SysDevice>(entity =>
            {
                entity.HasKey(e => e.DeviceId)
                    .HasName("PRIMARY");

                entity.ToTable("sys_device");

                entity.HasIndex(e => new { e.DeviceSerialNo, e.ManufacturerId }, "X1_sys_device")
                    .IsUnique();

                entity.HasIndex(e => e.ManufacturerId, "X2_sys_device");

                entity.HasIndex(e => e.SensorType, "X3_sys_device");

                entity.Property(e => e.DeviceId).HasColumnType("int(11)");

                entity.Property(e => e.DeviceAddedDt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.DeviceDescription)
                    .HasMaxLength(255)
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.DeviceName)
                    .HasMaxLength(255)
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.DeviceSerialNo)
                    .IsRequired()
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.IsEnabled).HasMaxLength(1);

                entity.Property(e => e.LatLngDeviceId).HasColumnType("int(11)");

                entity.Property(e => e.LatestDataDt).HasColumnType("datetime");

                entity.Property(e => e.ManufacturerId).HasColumnType("int(11)");

                entity.Property(e => e.SensorType)
                    .IsRequired()
                    .HasMaxLength(20)
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.SiteId).HasColumnType("int(11)");
            });

            modelBuilder.Entity<SysDeviceLatlngSubstitute>(entity =>
            {
                entity.HasKey(e => new { e.LatLngDeviceId, e.StartDateTimeUtc })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("sys_device_latlng_substitute");

                entity.HasIndex(e => e.DeviceId, "x1_sys_device_latlng_substitude");

                entity.Property(e => e.LatLngDeviceId).HasColumnType("int(11)");

                entity.Property(e => e.StartDateTimeUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("StartDateTimeUTC");

                entity.Property(e => e.DeviceId).HasColumnType("int(11)");

                entity.Property(e => e.EndDateTimeUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("EndDateTimeUTC");
            });

            modelBuilder.Entity<SysManufacturer>(entity =>
            {
                entity.HasKey(e => e.ManufacturerId)
                    .HasName("PRIMARY");

                entity.ToTable("sys_manufacturer");

                entity.Property(e => e.ManufacturerId).HasColumnType("int(11)");

                entity.Property(e => e.ManufacturerName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");
            });

            modelBuilder.Entity<SysManufacturerParam>(entity =>
            {
                entity.HasKey(e => new { e.ManufacturerId, e.ParameterId })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("sys_manufacturer_param");

                entity.Property(e => e.ManufacturerId).HasColumnType("int(11)");

                entity.Property(e => e.ParameterId).HasColumnType("int(11)");

                entity.Property(e => e.ParameterUnit)
                    .HasMaxLength(255)
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");
            });

            modelBuilder.Entity<SysParameter>(entity =>
            {
                entity.HasKey(e => e.ParameterId)
                    .HasName("PRIMARY");

                entity.ToTable("sys_parameter");

                entity.Property(e => e.ParameterId).HasColumnType("int(11)");

                entity.Property(e => e.ParameterAbbreviation)
                    .IsRequired()
                    .HasMaxLength(10)
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.ParameterDescription)
                    .HasMaxLength(255)
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.ParameterName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.ParameterUnit)
                    .HasMaxLength(255)
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");
            });

            modelBuilder.Entity<SysProcessLog>(entity =>
            {
                entity.HasKey(e => e.RecId)
                    .HasName("PRIMARY");

                entity.ToTable("sys_process_log");

                entity.HasIndex(e => e.Ts, "x2_sys_process_log");

                entity.Property(e => e.RecId)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("rec_id");

                entity.Property(e => e.Message)
                    .HasMaxLength(255)
                    .HasColumnName("message")
                    .UseCollation("utf8_general_ci")
                    .HasCharSet("utf8");

                entity.Property(e => e.RecCount)
                    .HasColumnType("int(11)")
                    .HasColumnName("rec_count");

                entity.Property(e => e.RecErred)
                    .HasColumnType("int(11)")
                    .HasColumnName("rec_erred");

                entity.Property(e => e.RecImported)
                    .HasColumnType("int(11)")
                    .HasColumnName("rec_imported");

                entity.Property(e => e.Ts)
                    .HasColumnType("timestamp")
                    .HasColumnName("ts")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");
            });

            modelBuilder.Entity<SysSetting>(entity =>
            {
                entity.HasKey(e => e.SettingName)
                    .HasName("PRIMARY");

                entity.ToTable("sys_settings");

                entity.Property(e => e.SettingName)
                    .HasMaxLength(50)
                    .HasColumnName("setting_name");

                entity.Property(e => e.SettingValue)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("setting_value");
            });

            modelBuilder.Entity<SysTenantAuth>(entity =>
            {
                entity.HasKey(e => new { e.TenantId, e.ManufacturerId })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("sys_tenant_auth");

                entity.Property(e => e.TenantId).HasColumnType("int(11)");

                entity.Property(e => e.ManufacturerId).HasColumnType("int(11)");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.CreatedDt).HasColumnType("datetime");
            });

            modelBuilder.Entity<TmpGc>(entity =>
            {
                entity.ToTable("tmp_gc");

                entity.Property(e => e.Id).HasColumnType("bigint(20)");

                entity.Property(e => e.DeviceId)
                    .HasColumnType("int(11)")
                    .HasColumnName("deviceId");

                entity.Property(e => e.Param)
                    .HasMaxLength(50)
                    .HasColumnName("param");

                entity.Property(e => e.PeakHeight).HasColumnName("peakHeight");

                entity.Property(e => e.PeakTime).HasColumnName("peakTime");

                entity.Property(e => e.Result).HasColumnName("result");

                entity.Property(e => e.Status)
                    .HasMaxLength(50)
                    .HasColumnName("status");

                entity.Property(e => e.Ts)
                    .HasColumnType("datetime")
                    .HasColumnName("ts");

                entity.Property(e => e.Unit)
                    .HasMaxLength(50)
                    .HasColumnName("unit");
            });

            modelBuilder.Entity<TmpGc2>(entity =>
            {
                entity.ToTable("tmp_gc_2");

                entity.Property(e => e.Id).HasColumnType("bigint(20)");

                entity.Property(e => e.DeviceId)
                    .HasColumnType("int(11)")
                    .HasColumnName("deviceId");

                entity.Property(e => e.Param)
                    .HasMaxLength(50)
                    .HasColumnName("param");

                entity.Property(e => e.PeakHeight).HasColumnName("peakHeight");

                entity.Property(e => e.PeakTime).HasColumnName("peakTime");

                entity.Property(e => e.Result).HasColumnName("result");

                entity.Property(e => e.Status)
                    .HasMaxLength(50)
                    .HasColumnName("status");

                entity.Property(e => e.Ts)
                    .HasColumnType("datetime")
                    .HasColumnName("ts");

                entity.Property(e => e.Unit)
                    .HasMaxLength(50)
                    .HasColumnName("unit");
            });

            modelBuilder.Entity<TmpGcSy>(entity =>
            {
                entity.ToTable("tmp_gc_sys");

                entity.Property(e => e.Id).HasColumnType("bigint(20)");

                entity.Property(e => e.BatteryVoltage).HasColumnName("batteryVoltage");

                entity.Property(e => e.DeviceId)
                    .HasColumnType("int(11)")
                    .HasColumnName("deviceId");

                entity.Property(e => e.EnclosureTempC).HasColumnName("enclosureTempC");

                entity.Property(e => e.MicroTempC).HasColumnName("microTempC");

                entity.Property(e => e.Ts)
                    .HasColumnType("datetime")
                    .HasColumnName("ts");
            });

            modelBuilder.Entity<TmpGcSys2>(entity =>
            {
                entity.ToTable("tmp_gc_sys_2");

                entity.Property(e => e.Id).HasColumnType("bigint(20)");

                entity.Property(e => e.BatteryVoltage).HasColumnName("batteryVoltage");

                entity.Property(e => e.DeviceId)
                    .HasColumnType("int(11)")
                    .HasColumnName("deviceId");

                entity.Property(e => e.EnclosureTempC).HasColumnName("enclosureTempC");

                entity.Property(e => e.MicroTempC).HasColumnName("microTempC");

                entity.Property(e => e.Ts)
                    .HasColumnType("datetime")
                    .HasColumnName("ts");
            });

            modelBuilder.Entity<TmpLatlng>(entity =>
            {
                entity.ToTable("tmp_latlng");

                entity.Property(e => e.Id).HasColumnType("bigint(20)");

                entity.Property(e => e.DeviceId)
                    .HasColumnType("int(11)")
                    .HasColumnName("deviceId");

                entity.Property(e => e.Lat).HasColumnName("lat");

                entity.Property(e => e.Lng).HasColumnName("lng");

                entity.Property(e => e.Ts)
                    .HasColumnType("datetime")
                    .HasColumnName("ts");
            });

            modelBuilder.Entity<TmpLatlng2>(entity =>
            {
                entity.ToTable("tmp_latlng_2");

                entity.Property(e => e.Id).HasColumnType("bigint(20)");

                entity.Property(e => e.DeviceId)
                    .HasColumnType("int(11)")
                    .HasColumnName("deviceId");

                entity.Property(e => e.Lat).HasColumnName("lat");

                entity.Property(e => e.Lng).HasColumnName("lng");

                entity.Property(e => e.Ts)
                    .HasColumnType("datetime")
                    .HasColumnName("ts");
            });

            modelBuilder.Entity<TmpWind>(entity =>
            {
                entity.ToTable("tmp_wind");

                entity.Property(e => e.Id).HasColumnType("bigint(20)");

                entity.Property(e => e.DeviceId)
                    .HasColumnType("int(11)")
                    .HasColumnName("deviceId");

                entity.Property(e => e.Dir).HasColumnName("dir");

                entity.Property(e => e.Gust).HasColumnName("gust");

                entity.Property(e => e.Speed).HasColumnName("speed");

                entity.Property(e => e.Ts)
                    .HasColumnType("datetime")
                    .HasColumnName("ts");

                entity.Property(e => e.Unit)
                    .HasMaxLength(50)
                    .HasColumnName("unit");
            });

            modelBuilder.Entity<TmpWind2>(entity =>
            {
                entity.ToTable("tmp_wind_2");

                entity.Property(e => e.Id).HasColumnType("bigint(20)");

                entity.Property(e => e.DeviceId)
                    .HasColumnType("int(11)")
                    .HasColumnName("deviceId");

                entity.Property(e => e.Dir).HasColumnName("dir");

                entity.Property(e => e.Gust).HasColumnName("gust");

                entity.Property(e => e.Speed).HasColumnName("speed");

                entity.Property(e => e.Ts)
                    .HasColumnType("datetime")
                    .HasColumnName("ts");

                entity.Property(e => e.Unit)
                    .HasMaxLength(50)
                    .HasColumnName("unit");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}

﻿using System;
using System.Collections.Generic;

#nullable disable

namespace gc.DataAccess.DataObjects
{
    public partial class AutEnmet
    {
        public string DateTime { get; set; }
        public string Timestamp { get; set; }
        public string Record { get; set; }
        public string RunType { get; set; }
        public string Vapor { get; set; }
        public string Concentration { get; set; }
        public string PeakHeight { get; set; }
        public string PeakLocation { get; set; }
        public string AlarmLevel { get; set; }
        public string ColTemp { get; set; }
        public string CarrDuty { get; set; }
        public string CalFactor { get; set; }
        public string Baseline { get; set; }
        public string Ver { get; set; }
        public string RunCount { get; set; }
        public string Status { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string WsMs { get; set; }
        public string WsAvg { get; set; }
        public string WindDir { get; set; }
        public string BatteryVoltage { get; set; }
        public string EnclosureTemp { get; set; }
        public string EngineTemp { get; set; }
        public string TimestampLocal { get; set; }
        public string SiteId { get; set; }
        public string DeadCol { get; set; }
        public long Id { get; set; }
        public string DeviceId { get; set; }
        public DateTime? TimestampUtcDt { get; set; }
        public DateTime? TimestampLocalDt { get; set; }
    }
}

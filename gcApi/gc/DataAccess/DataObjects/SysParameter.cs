﻿using System;
using System.Collections.Generic;

#nullable disable

namespace gc.DataAccess.DataObjects
{
    public partial class SysParameter
    {
        public int ParameterId { get; set; }
        public string ParameterName { get; set; }
        public string ParameterAbbreviation { get; set; }
        public string ParameterDescription { get; set; }
        public string ParameterUnit { get; set; }
        public float? AqiMultiplier { get; set; }
        public float? AqiAvgHours { get; set; }
    }
}

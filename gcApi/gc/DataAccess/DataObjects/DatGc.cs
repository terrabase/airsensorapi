﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
#nullable disable

namespace gc.DataAccess.DataObjects
{
    public partial class DatGc
    {
        public int DeviceId { get; set; }
        public DateTime TimestampUtc { get; set; }
        public string ParamId { get; set; }
        public float? Result { get; set; }
        public float? PeakTime { get; set; }
        public float? PeakHeight { get; set; }
        public string Unit { get; set; }
        public long? SourceFileId { get; set; }
        public string Status { get; set; }
        public DateTime? LastModifiedUtcDt { get; set; }
    }
}

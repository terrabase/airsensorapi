﻿using System;
using System.Collections.Generic;

#nullable disable

namespace gc.DataAccess.DataObjects
{
    public partial class TmpWind2
    {
        public long Id { get; set; }
        public int? DeviceId { get; set; }
        public DateTime? Ts { get; set; }
        public float? Speed { get; set; }
        public float? Dir { get; set; }
        public string Unit { get; set; }
        public float? Gust { get; set; }
    }
}

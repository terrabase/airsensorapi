﻿using System;
using System.Collections.Generic;

#nullable disable

namespace gc.DataAccess.DataObjects
{
    public partial class DatGcSysOld
    {
        public long Id { get; set; }
        public int DeviceId { get; set; }
        public DateTime TimestampUtc { get; set; }
        public float? MicroTempC { get; set; }
        public float? BatteryVoltage { get; set; }
        public long? SourceFileId { get; set; }
        public float? EnclosureTemp { get; set; }
        public DateTime? LastModifiedUtcDt { get; set; }
    }
}

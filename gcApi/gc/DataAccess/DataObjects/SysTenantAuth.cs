﻿using System;
using System.Collections.Generic;

#nullable disable

namespace gc.DataAccess.DataObjects
{
    public partial class SysTenantAuth
    {
        public int TenantId { get; set; }
        public int ManufacturerId { get; set; }
        public DateTime CreatedDt { get; set; }
        public string CreatedBy { get; set; }
    }
}

﻿(function () {
    window.addEventListener("load", function () {
        setTimeout(function () {
            // Section 01 - Set url link 
            var logo = document.getElementsByClassName('link');
            logo[0].href = "https://www.terrabase.com/";
            logo[0].target = "_blank";

            // Section 02 - Set logo
            logo[0].children[0].alt = "TerraBase Air Sensor API";
            logo[0].children[0].src = "/swagger-ui/resources/terrabase.jpg";

            // Section 03 - Set 32x32 favicon
            var linkIcon32 = document.createElement('link');
            linkIcon32.type = 'image/png';
            linkIcon32.rel = 'icon';
            linkIcon32.href = '/swagger-ui/resources/favicon-32x32.png';
            linkIcon32.sizes = '32x32';
            document.getElementsByTagName('head')[0].appendChild(linkIcon32);

            // Section 03 - Set 16x16 favicon
            var linkIcon16 = document.createElement('link');
            linkIcon16.type = 'image/png';
            linkIcon16.rel = 'icon';
            linkIcon16.href = '/swagger-ui/resources/favicon-16x16.png';            
            linkIcon16.sizes = '16x16';
            document.getElementsByTagName('head')[0].appendChild(linkIcon16);

            var link = document.querySelector("link[rel*='icon']") || document.createElement('link');;
            document.head.removeChild(link);
            link = document.querySelector("link[rel*='icon']") || document.createElement('link');
            document.head.removeChild(link);
            link = document.createElement('link');
            link.type = 'image/x-icon';
            link.rel = 'shortcut icon';
            link.href = '../swagger-ui/resources/favicon-16x16.png';
            document.getElementsByTagName('head')[0].appendChild(link);

            customizeSwaggerUI();
        });
    });
})();
function customizeSwaggerUI() {
    setTimeout(function () {
        var tag = '<rapi-pdf style="display:none" id="thedoc"> </rapi-pdf>';
        var btn1 = '<button id="btn" style="font-size:16px;padding: 6px 16px;text-align: center;white-space: nowrap;background-color: orangered;color: white;border: 0px solid #333;cursor: pointer;" type="button" onclick="downloadPDF()"><i class="fas fa-download"></i>&nbsp;&nbsp;Download API Document </button>';
        //var btn2 = '<button id="btn" style="font-size:16px;padding: 6px 16px;text-align: center;white-space: nowrap;background-color: grey;color: white;border: 0px solid #333;cursor: pointer;" type="button" onclick="gotoGitHub()"><i class="fab fa-github"></i>&nbsp;&nbsp;See example code on GitHub </button>';
        var oldhtml = document.getElementsByClassName('info')[0].innerHTML;
        document.getElementsByClassName('info')[0].innerHTML = oldhtml + '</br>' + tag + '</br>' + btn1;
        //document.getElementsByClassName('info')[0].innerHTML = oldhtml + '</br>' + tag + '</br>' + btn1 + '&nbsp;&nbsp;&nbsp;&nbsp;' + btn2;
    }, 1200);
}
function gotoGitHub() {
    alert("Not Implemented");
}
function downloadPDF() {
    var client = new XMLHttpRequest();
    client.overrideMimeType("application/json");
    client.open('GET', 'swagger/v1/swagger.json');
    var jsonAPI = "";
    client.onreadystatechange = function () {
        if (client.responseText != 'undefined' && client.responseText != "") {
            jsonAPI = client.responseText;
            if (jsonAPI != "") {
                let docEl = document.getElementById("thedoc");
                var key = jsonAPI.replace('\"Authorization: Bearer {token}\"', "");
                let objSpec = JSON.parse(key);
                docEl.generatePdf(objSpec);
            }
        }
    }
    client.send();
}